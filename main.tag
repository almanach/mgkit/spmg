/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2003, 2004, 2005, 2006, 2007, 2008 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  frenchmg.tag -- Small French XTAG from Meta Grammar
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include 'header.tag'.

:-require 'spgram.tag'.
:-require 'addons.tag'.

:-op(  700, xfx, [?=]). % for default value
:-xcompiler(( X ?= V :- ( \+ var(X) xor X = V))). %% Setting a default value

%?-recorded( L::lctag(_,_,_) ), format('lctag ~w\n',[L]),fail.

:-light_tabular fast_reader/1.

fast_reader(L) :- domain('-fast_reader',L).

?-argv(L),
  \+ fast_reader(L),
  wait((clean_sentence)),
%%  every((lctag_ok(Tree),format('lctag_true ~w\n',[Tree]))),
  wait((tag_filter(_))),
%%  L=[],
  recorded('N'(N)), A=0,
  (
   %% First try to get a full parse for a verbal sentence
   %%      tag_phrase(top='S'{ sat => (+), extraction => extraction[-,wh]} at - 'S',A,N)
   %% short sentence are systematically tried,
   %% otherwise mask good interpretations for sentences such as
   %% 'ce calme' or 'la voile'
   tag_phrase(top='S'{  extraction => extraction[~rel],
%%			mode => mode[~ [(-)]],
			sat => (+)
%%			control => (-)
		     } at - 'S',A,N)
  %% tag_phrase(top='S'{  extraction => extraction[~rel] } at - 'S',A,N)
/*  ;  
   wait( '$answers'('S',A,N) ),
   ( check_parsed_sentence(A,N) ->
     %% If non full verbal parse, then look for a full non-verbal parse
     %%     format('switch to short sentence\n',[]),
     tag_phrase(top='S'{  extraction => extraction[~rel],
			  mode => (-),
			  sat => (+)
		       }
	       at - 'S',A,N)

   ;
     fail
   )
*/
  )
  .

:-std_prolog check_parsed_sentence/2.

check_parsed_sentence(A,N) :-
	item_term( I, 'S'{ mode => mode[~ [(-)]],
			   extraction => extraction[~rel],
			   sat => (+),
			   control => (-)
			 }(A,N) ),
	\+ recorded( I )
	.

?-argv(L),
  \+ fast_reader(L),
  domain('-robust',L),
  recorded('N'(_N)), _A=0,
  wait( tag_phrase( '$answers'('S',_A,_N)) ),
  (   item_term( I, 'S'{}(_A,_N) ),
      \+ recorded( I ) ->
      %% If no full parse, then try partial parses
      format('*** Try robust mode\n',[]),
      record(robust),
      wait(( %%make_intlist(AllPos,0,_N),
	     %%domain(_Left,AllPos),
	     ANA::analyze(_,_,_),
%%	     format('Analyze ~w\n',[ANA]),
	     true
	   )),
      ( extract_best_solution(A,N,U),
%%	format('best sol ~w ~w ~w\n',[A,N,U]),
	true
      ; '$answers'(extract_best_solution(_L1,_R1,_)),
	'$answers'(extract_best_solution(_L2,_R2,_)),
	_L2 < _R1,
	_R1 =< _R2,
	_L1 =< _L2,
	\+ (_L1 == _L2,_R2 == _R1 ),
	%%	U=unknown,
	%%	format('potential ~w ~w (l1=~w r1=~w; l2=~w,r2=~w)\n',[_L2,_R1,_L1,_R1,_L2,_R2]),
	%%	U=unknown,
	%%	extract_analyze(A,N,U),
	%%	N is A+1,
	%% ( N =< _L2, A >= _L1
	%% ; A >= _R1, N =< _R2
	%% ),
	( try_fill_second_best(_L1,_L2,A,N,U)
	;  try_fill_second_best(_R1,_R2,A,N,U)
	),
 	\+ ( '$answers'(extract_best_solution(_L3,_R3,_)),
	     _L3 =< A,
	     N =< _R3,
	     (_R3 =< _L2 ; _L3 >= _R1)
	   ),
%%	format('extra sol ~w ~w ~w\n',[A,N,U]),
	true
      ),
      analyze(A,N,U)
  ;
      fail
  )
  .

:-std_prolog try_fill_second_best/5.

try_fill_second_best(XL,XR,L,R,U) :-
	( extract_second_best_solution(XL,XR,L,R,U),
	  true
	; '$answers'(extract_second_best_solution(XL,XR,_L1,_R1,_)),
	  '$answers'(extract_second_best_solution(XL,XR,_L2,_R2,_)),
	  _L2 < _R1,
	  _R1 =< _R2,
	  _L1 =< _L2,
	  \+ (_L1 == _L2,_R2 == _R1 ),
	  /*
	  extract_analyze(L,R,U),
	  ( R =< _L2, L >= _L1
	  ; L >= _R1, R =< _R2
	  ),
	  */
	  ( try_fill_second_best(_L1,_L2,L,R,U)
	  ;  try_fill_second_best(_R1,_R2,L,R,U)
	  ),
	  \+ ( '$answers'(extract_second_best_solution(XL,XR,_L3,_R3,_)),
	       _L3 =< L,
	       R =< _R3,
	       (_R3 =< _L2 ; _L3 >= _R1)
	     ),
%%	  analyze(L,R,U)
	  true
	;
	  fail
	)
	.
     
analyze(A,N,U) :-
	tag_phrase(top=U::'S'{ extraction => extraction[~rel],
			       control => (-),
			       mode => Mode,
			       xarg => XArg } at - 'S',A,N),
	N > A+1,
	%%	format('Analyze found at ~w ~w ~w\n',[A,N,U]),
	( XArg = (-) xor XArg = xarg{ trace => (-) } ),
	( Mode == imperative ->
	  %% avoid false imperative sentence just at the end of a sentence in robust mode
	  \+ (N is A+2, recorded('N'(N)))
	;
	  true
	),
	%% format('\tAnalyze kept\n',[]),
	true
	.

<<<<<<< .mine
analyze(A,N,U) :- tag_phrase(top=U::comp{} at - comp,A,N), N > A.

=======
%%analyze(A,N,U) :- tag_phrase(top=U::comp{} at - comp,A,N), N > A.

>>>>>>> .r926
analyze(A,N,U) :- tag_phrase(top=U::'PP'{} at - 'PP',A,N), N > A.

<<<<<<< .mine
%%analyze(A,N,U) :- tag_phrase(top=U::'N2'{ sat => (+) } at - 'N2',A,N), N > A.


=======
analyze(A,N,U) :- tag_phrase(top=U::'N2'{ sat => (+) } at - 'N2',A,N), N > A.


>>>>>>> .r926
%%analyze(A,N,U) :- tag_phrase(- <=> poncts, A, N), N > A.
%%analyze(A,N,U) :- tag_phrase(- <=> ponctw, A, N), N > A.

%%analyze(A,N,U) :- tag_phrase(<=> adv,A,N), N > A.

% analyze(A,N,U) :- tag_phrase(- <=> csu, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> pri, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> prel, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> pro, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> adj, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> advneg, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> prep, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> coo, A, N), N > A.
% analyze(A,N,U) :- tag_phrase(- <=> v, A, N), N > A.
analyze(A,N,unknown) :- tag_phrase(- unknown, A, N),N > A.

:-std_prolog extract_analyze/3.

extract_analyze(A,N,U) :-
	item_term(I,analyze(A,N,U)),
	recorded(I,_)
	.

%%:-std_prolog extract_best_solution/3.
:-light_tabular extract_best_solution/3.
:-mode(extract_best_solution/3,+(-,-,-)).

extract_best_solution(L,R,U) :-
	extract_analyze(L,R,U),
	(\+ ( extract_analyze(L2,R2,_),
	      L2 =< L, R =< R2,
	      \+ (L=L2, R=R2)
	    )
	)
	.


:-light_tabular extract_second_best_solution/5.
:-mode(extract_second_best_solution/5,+(+,+,-,-,-)).

extract_second_best_solution(XL,XR,L,R,U) :-
	extract_analyze(L,R,U),
	XL =< L,
	R =< XR,
%%	(XL < L xor R < XR),
	(\+ ( extract_analyze(L2,R2,_),
	      L2 =< L, R =< R2,
	      XL =< L2, R2 =< XR,
	      \+ (L=L2, R=R2)
	    )
	)
	.


%%?-tag_hypertag(Family,HyperTag),tag_family_load(Family,Cat).

%%?-true.

%% Normalization of some cat

cat_normalize(det,det{ wh=> Wh,
		       poss => Pos,
		       numberposs => NPos,
		       dem => Dem
		     }) :-
	Wh ?= (-),
	Pos ?= (-),
	NPos ?= (-),
	Dem ?= (-)
	.

cat_normalize(nc,nc{time=> Time,
		    hum => Hum,
		    def => Def }) :-
	Time ?= (-), Hum ?= (-), Def ?= (+).

cat_normalize(prep,prep{pcas=> Pcas }) :- Pcas ?= (+).
%%cat_normalize(csu, csu{ que => Que }) :- Que ?= (-).

/*
cat_normalize(v,v{mode => Mode, person=> Person, number=> Number }) :-
	(   \+ var(Person) xor Person = 3 ),
	(   \+ var(Number) xor Number = sg )
	.
*/

/*
cat_normalize(v,v{ aux_req => AuxReq }) :-
	AuxReq ?= 'avoir'
	.
*/
cat_normalize(csu,csu{wh=> Wh}) :-
	Wh ?= (+).
cat_normalize(pri,pri{case => Case }) :-
	Case ?= case[~ [nom,acc]].
cat_normalize(prel,prel{case => Case }) :-
	Case ?= case[~ [nom,acc]].
cat_normalize(adv,adv{adv_kind => Kind, chunk_type => Chunk }) :-
	Kind ?= adv_kind[~ [tr�s,intensive,modnc,equalizer,modpro]],
	Chunk ?= (-)
	.
cat_normalize(pres,pres{chunk_type => Chunk }) :-
	Chunk ?= (-)
	.
cat_normalize(adj,adj{ pre_det => X }) :- X ?= (-).

cat_normalize(v,v{ lightverb => X}) :- X ?= (-).

:-light_tabular clean_sentence/0.

delete_address(X) :- '$interface'( 'object_delete'(X:ptr),[return(bool)]).

/*
clean_sentence :-
	'C'(N,lemma{ cat=> v,
		     lemma=>Lemma,
		     top=> v{ mode=>participle,
			      diathesis => passive,
			      gender=> Gender,
			      number=> Number
			    }
		   },M),
	\+ var(Lemma),
	\+ domain(Lemma,[uw,uwSe]),
	every(( recorded(_K::'C'(N,lemma{ cat=>adj,
					  lemma => Lemma,
					  top => adj{gender => Gender,
						     number=>Number} },M),
			 Adr),
%%		format('Remove adj ~w\n',[_K]),
		delete_address(Adr)))
	.
*/

/*
clean_sentence :-
	'C'(N,
	   lemma{ lex => Lex,
		  cat => adj,
		  lemma => Lemma,
		  top => adj{ gender => Gender, number => Number, person => Person }
		},
	    M),
	\+ var(Lemma),
	every(( recorded('C'(N,
			     lemma{ lex => Lex,
				    cat => nc,
				    lemma => Lemma,
				    top => nc{ gender => Gender, number => Number, person => Person }
				  },
			     M),
			 Adr),
		delete_address(Adr)
	      )).
*/

/*
clean_sentence :-
	'C'(N,
	   lemma{ lex => Lex,
		  cat => adj,
		  lemma => Lemma,
		  top => adj{ gender => Gender, number => Number, person => Person }
		},
	    M),
	\+ K::'C'(N,
		  lemma{ lex => Lex,
			 cat => nc,
			 lemma => Lemma,
			 top => nc{ gender => Gender, number => Number, person => Person }
		       },
		  M),
%%	format('Record ~w\n',[K]),
	record( K )
	.
*/

:-require 'format.pl'.

:-finite_set(de,['De',de,'d''']).
:-finite_set(la,['La',la,'l''']).

clean_sentence :-
	%% A simple recongnizer for proper nouns
	'C'(L1,lemma{ cat => np, lex => Lex1, truelex => TrueLex1 },R1),
	\+ 'C'(_,lemma{ cat => np},L1),
	@*{ goal => (
			'C'(_L,lemma{ cat => np, lex => _Lex, truelex => _True_Lex },_R),
			name_builder('~w ~w',[_Lex_In,_Lex],_Lex_Out),
			name_builder('~w ~w',[_True_Lex_In,_True_Lex],_True_Lex_Out)
		    xor	'C'(_L,lemma{ lex => _Part::de[] , truelex => _True_Part},_L1),
			(   'C'(_L1,lemma{ cat => np, lex => _Lex, truelex => _True_Lex},_R),
			    name_builder('~w ~w ~w',[_Lex_In,_Part,_Lex],_Lex_Out),
			    name_builder('~w ~w ~w',[_True_Lex_In,_True_Part,_True_Lex],_True_Lex_Out)
			xor 'C'(_L1,lemma{ lex => _Part2::la[], truelex => _True_Part2 },_L2),
			    'C'(_L2,lemma{ cat => np, lex => _Lex, truelex => _True_Lex },_R),
			    name_builder('~w ~w ~w ~w',[_Lex_In,_Part,_Part2,_Lex],_Lex_Out),
			    name_builder('~w ~w ~w ~w',[_True_Lex_In,_True_Part,_True_Part2,_True_Lex],_True_Lex_Out)
			)
		    ),
	    from => 1,
	    collect_first => [R1,Lex1,TrueLex1],
	    collect_last  => [R,LexLast,TrueLexLast],
	    collect_loop  => [_L,_Lex_In,_True_Lex_In],
	    collect_next  => [_R,_Lex_Out,_True_Lex_Out]
	  },
	\+ 'C'(R,lemma{ cat => np},_),
%%	format('Found NP sequence ~w -> ~w: ~w\n',[L1,R,LexLast])
	record_without_doublon('C'(L1,lemma{lemma => '_Uw', lex=>LexLast,truelex=>TrueLexLast,cat=>np},R))
	.

:-finite_set(open_quote,['�','"','''']).
:-finite_set(close_quote,['�','"','''']).
	
clean_sentence :-
	'C'(A, lemma{ lex => Open:: open_quote[], truelex => Q1 }, B ),
	'C'(B, lemma{ lex => Lex, cat => Cat, lemma => Lemma, top => Top, anchor => Anchor, truelex => TrueLex },C),
	'C'(C, lemma{ lex => Close:: close_quote[], truelex => Q2 }, D ),
%%	format('Found ~w ~w ~w : ~w -> ~w\n',[Open,Lex,Close,A,D]),
	domain(Open:Close,['�':'�','"':'"','''':'''']),
	name_builder('~w ~w ~w',[Open,Lex,Close],QuotedLex),
	name_builder('~w ~w ~w',[Q1,TrueLex,Q2],QuotedTrueLex),
	record_without_doublon( 'C'(A,
				    lemma{ lex => QuotedLex,
					   cat => Cat,
					   lemma => Lemma,
					   top => Top,
					   anchor => Anchor,
					   truelex => QuotedTrueLex
					 },
				    D)
			      )
	.


%% Hack to handle ncpred with lightverbs !
%% Transfer subcat frame from ncpred to v+lightverb

clean_sentence :-
	recorded( KK:: 'C'(C, lemma{ cat => v,
				top => Top::v{ lightverb => _Light },
				lex => Lex,
				truelex => TLex,
				%%				lemma => Lemma,
				anchor => tag_anchor{ name => ht{ diathesis => Dia::active,
								  %% arg1 => arg{ kind => _Arg1Kind },
								  cat => Cat
								} }
			      },
		      D),
		  Addr),
	% be sure we deal with an instantiated entry and not some underspecified one
	\+ var(_Light),
	%%	_Arg1Kind == npredobj,
	erase(KK),
	every(('C'(A,
		   lemma{ top => ncpred{ lightverb => Light },
			  lemma => Lemma,
			  anchor => Anchor::tag_anchor{ name => HT::ht{ diathesis => Dia,
									cat => Cat,
									refl => Refl,
									arg1 => Arg1,
									arg2 => Arg2::arg{ kind => Kind2 },
									arg0 => Arg0 % subject
								      }
						      }
			},
		   B),
	       _Light == Light,
	       %%	format('Delete ~w Before Adding ~w\n',[Addr,K]),
	       %%	every(( delete_address(Addr) )),
	       %% Seems better to move original arg1 to arg2 when possible
	       %% new arg1 would correspond to a kind of object, filled by ncpred
	       ( Kind2 == (-) ->
		   XArg1= Arg2,
		   XArg2= Arg1
	       ;   
		   XArg1= Arg1,
		   XArg2= Arg2
	       ),
	       record( K::'C'(C,
			      lemma{ cat => v,
				     top => Top,
				     lex => Lex,
				     truelex => TLex,
				     lemma => Lemma,
				     anchor => tag_anchor{ name => ht{ diathesis => Dia,
								       cat => Cat,
								       refl => Refl,
								       arg0 => Arg0,
								       arg1 => XArg1,
								       arg2 => XArg2
								     },
							   equations => [],
							   coanchors => []
							 }
				   },
			      D) ),
	       %% format('Adding ~w\n',[K]),
	       true
	      )),
	%% delete_address seems to be buggy, don't know why !
	%% delete_address(Addr),
	%% record_without_doublon( edge_disable(Addr) ),
	true
	.

:-extensional autoload_check_hypertag/3.

autoload_check_hypertag(v,ht{},ht{ arg1 => arg{ kind => npredobj } }).

:-std_prolog anchor_hypertag/2.

anchor_hypertag(ht{ anchor => Anchor }, Anchor ).

:-std_prolog name_builder/3.

name_builder(Format,Args,Name) :-
        string_stream(_,S),
        format(S,Format,Args),
        flush_string_stream(S,Name),
        close(S)
        .

:-std_prolog record_without_doublon/1.

record_without_doublon( A ) :-
        (   recorded( A ) xor record( A ))
        .


:-std_prolog tag_autoload_adj/7.

tag_autoload_adj(Cat,Top,Bot,Left,Right,Left1,Right1) :-
	( Cat = 'Infl' ->
	  ( \+ (\+ Bot = 'Infl'{ mode => adjective }) ->
	    %% format('Search autoload adj cat=~w left=~w left1=~w bot =~w\n',[Cat,Left,Left1,Bot]),
	    'C'(Left,
		Lemma::lemma{ cat => v,
			      anchor => tag_anchor{
						   name => ht{ arg1 => arg{ kind => Kind1 },
							       arg2 => arg{ kind => Kind2} }}
			    },
		_Left1
	       ),
	    %%	    format('Found0 autoload_adj ~w left=~w lemma=~w\n',[Cat,_Left,Lemma]),
	    (Kind1 = acomp xor Kind2 = acomp),
	    Right=Right1,
	    term_range(_Left1,1000,Left1),
	    %%	    format('Found1 autoload_adj ~w left=~w lemma=~w\n',[Cat,_Left,Lemma]),
	    true
	  ;
	    Left=Left1,
	    Right=Right1
	  )
	;
	    Left=Left1,
	    Right=Right1
	)
	.


:-light_tabular special_lctag/2.
:-mode(special_lctag/2,+(+,-)).

/*
special_lctag(Name::follow_coord_aux,Left) :-
	'C'(M,lemma{ cat => Cat, lex => Lex },Left),
	( Cat = coo
	;
	  Lex = (','),
	  K is Left+100,
	  term_range(Left,K,Range),
	  'C'(Range,lemma{ cat => coo },_)
	)
	.


special_lctag('25 empty_spunct shallow_auxiliary',Left) :-
	( recorded( 'N'(Left) )
	xor 'C'(Left,lemma{ lex => ponct['\�','"'] },_)
	)
	.
*/