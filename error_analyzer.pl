#!/usr/bin/perl

use 5.006;
use strict;
use warnings;
use Carp;

use Event qw/loop unloop/;
use IPC::Open2;
use IPC::Open3;
use IPC::Run qw/start/;

use AppConfig qw/:argcount :expand/;

use locale;

#use IO::Handle;

my $config = AppConfig->new(
			    "verbose|v!" => {DEFAULT => 1},
			    "stats|s!"   => {DEFAULT => 0},
			    "errfile=s"  => {DEFAULT => "errors"},
			    "stoplist=s" => {DEFAULT => "stoplist"},
			    "sentences!" => {DEFAULT => 0},
			    "frate=f"    => {DEFAULT => 0.30},
			    "occ=i"      => {DEFAULT => 3},
			    "testfile|t=f"
                           );

$config->args;

my %stoplist = ();

my $testfile = $config->testfile();

if (-r $config->stoplist) {
  open SL,'<',$config->stoplist || die "can't open stoplist: $!";
  while(<SL>) {
    chomp;
    $stoplist{$_} = 1;
  }
  close SL;
}

my ($in,$out,$err);

my $h = start ['easy.pl','--noexpand'], '<', \$in, '>pty>', \$out, '2>', \$err
  or die "can't connect easy.pl: $?";

my %sentences = ();
my %words;
my $sentence;
my $status = 'fail';
my $counter = 0;

my $failed = 0;
my $tried = 0;

if ($testfile) {
  open(TEST,"<$testfile") || die "can't open $testfile\n";
}

while (<>) {
  my $xsentence = $_;
  my $status = 'fail';
  chomp $xsentence;
  next if ($xsentence =~ /^#/);
  next if ($xsentence =~ /^\s*$/);
  next if ($xsentence =~ /^\*{2}/);
  next if ($xsentence =~ /^\s+/);
  $status = 'ok' if ($xsentence =~ s/^ok\s+\d+\s+//og);
  $xsentence =~ s/^fail\s+\d+\s+//og;
  my $sentence = $xsentence;
  if ($testfile) {
    $sentence = <TEST>;
    chomp $sentence;
  }
  $sentence =~ s/^\*\s+//og;
  $sentences{$sentence} ||= { id => ++$counter, status => $status };
  my $sid = $sentences{$sentence}{id};
  ++$tried;
  ++$failed unless ($status eq 'ok');
#  print "SEND $sentence\n";
  $in .= "$sentence\n";
  $h->pump until ($out =~ /SENTENCE\s+DIV/o);
  foreach (split(/\r*\n+/,$out)) {
    last if (/SENTENCE\s+DIV/o);
    next if (/^%%/);
    next if (/^\s*$/);
    my $word = $_;
    chomp $word;
##    print "Handling $word\n";
    next if (exists $stoplist{$word});
    my $entry = $words{$word} ||= { occ => 0, focc => 0, fsent => {} };
    ++$entry->{occ};
    if ($status eq 'fail') {
      $entry->{fsent}{$sid} ||= 1;
      ++$entry->{focc};
    }
  }
  $out = '';
}

if ($testfile) {
  close(TEST);
}

$h->finish;

##my $minfrate = $config->frate;
my $minocc = $config->occ;

my $minfrate = 1.05 * ($failed / $tried);

foreach my $word (keys %words) {
  my $entry = $words{$word};
  my $focc = $entry->{focc};
  my $occ = $entry->{occ};
  my $frate = $focc / $occ;
  unless ($frate > $minfrate && $occ > $minocc) {
    delete $words{$word};
    next;
  }
  $entry->{frate} = $frate;
}

foreach my $word (sort word_sort keys %words) {
  my $entry = $words{$word}; 
  my @sentences = sort keys %{$entry->{fsent}};
  $word =~ s/\s+/_/og;
  printf "%s %.2f %u %u %u\n", $word, $entry->{frate}, scalar(@sentences), $entry->{focc}, $entry->{occ};
  next unless ($config->sentences);
  foreach my $sid (@sentences) {
    print "\t$sentences{$sid}\n";
  }
  print "\n";
}

sub word_sort {
  my $ea = $words{$a};
  my $eb = $words{$b};
  ($eb->{occ} <=> $ea->{occ} || $eb->{frate} <=> $ea->{frate} || $eb->{occ} <=> $ea->{occ} );
}


=head1 NAME

error_analyzer.pl

=head1 DESCRIPTION

to compute error rates for words from parse logs

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2004-2007, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
