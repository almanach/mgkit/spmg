/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2001, 2004, 2008 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  preheader.tag -- Header file for TAG Grammar
 *
 * ----------------------------------------------------------------
 * Description
 * 
 * ----------------------------------------------------------------
 */

:-include_unless_def(defheader).

:-require 'tag_generic.pl'.

:-features( tag_tree, [ family, name, tree ] ).
:-features( tag_anchor, [name, coanchors, equations ] ).

:-tag_mode(_,all,[+|-],+,-).

:-features( lexicon, [lex,cat,ht,top] ).

:-extensional lexicon{}.

%% Need to be on a single line for the extraction to build small_header.tag
%% the extractor should be made more clever in some future
:-finite_set(ponct,['�','�',';','.','!','?','...','?!',':',',','"','(',')','[',']','\�','\�','etc.','!?','-','*','_','+','_SMILEY','_SENT_BOUND','!!!']).

:-subset(initponct,ponct['*','-','_','+']).

%%:-tagfilter(Name^Left^Right^NT^Top^(recorded(robust) xor ok_tree(Name,Left))).

:-tagfilter(Name^Left^Right^NT^Top^(check_ok_tree(Name,Left))).

%%:-tagfilter_cat(Cat^Mode^Left^Right^(check_ok_cat(Cat,Mode,Left))).
:-tagfilter_cat(Cat^Mode^Left^Right^(check_ok_cat(Cat,Mode,Left,Right))).

%% Optimize multiple tig tree adjoining because they don't cover empty spans
:-no_empty_tig_trees.

%% Optimize kleene loops because their body don't cover empty span
:-no_empty_kleene_body.

%% Max boundary on the number of tig adjoinings on each side
:-max_tig_adj(5).



