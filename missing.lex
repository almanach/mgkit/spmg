## My completion file for adding missing features to words
## beware of tabs as separator (not blanks)

## Nothing for Spanish.. yet.
no	100	advneg	[pred="no_____1",cat=advneg]	no_____1	Default	N	%default
toda	100	a	[pre_det=+,cat=a,@Q0FS0]	todo_____1	Default	Q0FS0	%default
todas	100	a	[pre_det=+,cat=a,@Q0FP0]	todo_____1	Default	Q0FP0	%default
todo	100	a	[pre_det=+,cat=a,@Q0MS0]	todo_____1	Default	Q0MS0	%default
todos	100	a	[pre_det=+,cat=a,@Q0MP0]	todo_____1	Default	Q0MP0	%default

##No son solo cliticos sino tambien pronombres
ella	100	p	[pred="p_____1",case=nom,@P3FS000]	p_____1	Default	P3FS000	%default
ellas	100	p	[pred="p_____1",case=nom,@P3FP000]	p_____1	Default	P3FP000	%default
ello	100	p	[pred="p_____1",case=nom,@P3NS000]	p_____1	Default	P3NS000	%default
ellos	100	p	[pred="p_____1",case=nom,@P3MP000]	p_____1	Default	P3MP000	%default
�l	100	p	[pred="p_____1",case=nom,@P3NS000]	p_____1	Default	P3NS000	%default
nosotras	100	p	[pred="p_____1",case=nom,@P1FP000]	p_____1	Default	P1FP000	%default
nosotros	100	p	[pred="p_____1",case=nom,@P1MP000]	p_____1	Default	P1MP000	%default
s�	100	p	[pred="p_____1",case=nom,@P3CNO00]	p_____1	Default	P3CNO00	%default
ti	100	p	[pred="p_____1",case=nom,@P2CSO00]	p_____1	Default	P2CSO00	%default
t�	100	p	[pred="p_____1",case=nom,@P2CSN00]	p_____1	Default	P2CSN00	%default

usted	100	p	[pred="p_____1",case=nom,@P2CS00P]	p_____1	Default	P2CS00P	%default
ustedes	100	p	[pred="p_____1",case=nom,@P2CP00P]	p_____1	Default	P2CP00P	%default

vosotras	100	p	[pred="p_____1",case=nom,@P2FP000]	p_____1	Default	P2FP000	%default
vosotros	100	p	[pred="p_____1",case=nom,@P2MP000]	p_____1	Default	P2MP000	%default
yo	100	p	[pred="p_____1",case=nom,@P1CSN00]	p_____1	Default	P1CSN00	%default
m�	100	p	[pred="p_____1",case=dat,@P1CSO00]	p_____1	Default	P1CSO00	%default


##Este problema se podria solucionar manejando por separado el cl con el prel y de esta forma controlar
##la concordancia con el verbo de la oracion relativa

lo que	100	prel	[pred="lo que_____1",cat=p,@R0CN000]	lo que_____1	Default	R0CN000	%default
la que	100	prel	[pred="la que_____1",cat=p,@R0CN000]	la que_____1	Default	R0CN000	%default
los que	100	prel	[pred="los que_____1",cat=p,@R0CN000]	los que_____1	Default	R0CN000	%default
las que	100	prel	[pred="las que_____1",cat=p,@R0CN000]	las que_____1	Default	R0CN000	%default
lo cual	100	prel	[pred="lo cual_____1",cat=p,@R0CN000]	lo cual_____1	Default	R0CN000	%default
la cual	100	prel	[pred="la cual_____1",cat=p,@R0CN000]	la cual_____1	Default	R0CN000	%default
los cuales	100	prel	[pred="los cuales_____1",cat=p,@R0CN000]	los cuales_____1	Default	R0CN000	%default
las cuales	100	prel	[pred="las cuales_____1",cat=p,@R0CN000]	las cuales_____1	Default	R0CN000	%default

hay	100	v	[pred="haber_____9",@impers,cat=v,@MIP3S0]	haber_____9	ThirdSing	MIP3S0	%actif_impersonnel

@title maestro +s @m
@title maestra +s @f
@title doctor +es @m
@title doctora +s @f
@title profesor +es @m
@title profesora +s @f
@title se�ora +s @f
@title se�orita +s @f
@title se�or +es @m
@title Sr. Sres. @m
@title Sra. Sras. @f
@title Srta. Srtas @f

