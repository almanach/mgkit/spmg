#!/usr/bin/perl -w
use Test::More;
use File::Basename;
use strict;

# compute plan
plan tests => 2;

my $nb_tx = `grep '<tree' frgram.tag.xml  | wc -l`;
chomp $nb_tx;
is($nb_tx, 169, "number of trees in frgram.tag.xml");

my $nb_t = `grep '^tag_tree' frgram.tag  | wc -l`;
chomp $nb_t;
is($nb_t, $nb_tx, "number of trees in frgram.tag");



