<?xml version="1.0"?>
<!-- Author: Eric de la Clergerie "Eric.De_La_Clergerie@inria.fr" -->
<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
	method="html"
	indent="yes"
	encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:param name="simple"/>

<xsl:template match="/tag">
  <xsl:choose>
    <xsl:when test="$simple = 'yes'">
      <xsl:apply-templates select="//tree"/>
    </xsl:when>
    <xsl:otherwise>
      <html>
        <head>
          <link rel="STYLESHEET" type="text/css" href="style.css"/>
	  <script src="tree.js" type="text/javascript"/>
          <title>Browsing Grammar Frenchmg</title>
        </head>
        <body>
          <xsl:apply-templates select="//tree"/>
        </body>
      </html>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="tree">
  <xsl:variable name="size">
          <xsl:value-of select="count(.//node[not(node)])"/>
  </xsl:variable>

  <h1>Tree <xsl:value-of select="@name"/></h1>

  <table>

    <tr style="vertical-align:top">

      <td>
        <span class="tree">
          <xsl:apply-templates select="node|optional"/>
        </span>
      </td>
      
      <td>
        <table>
          <caption>Hypertag</caption>
          <td>
            <xsl:for-each select="description/fs/f[@name='ht']">
              <xsl:apply-templates select="." mode="root"/>
            </xsl:for-each>
          </td>
        </table>
      </td>
            
    </tr>
  </table>

  <div class="alldecorations">
    <table>
      <tr style="vertical-align:top">
	<xsl:apply-templates select="." mode="recurseavm"/>
      </tr>
    </table>
  </div>
  
  <div class="allguards">
    <table>
      <tr style="vertical-align:top">
	<xsl:apply-templates select="." mode="recurseguard"/>
      </tr>
    </table>
  </div>

</xsl:template>

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Main Feature -->
<xsl:template match="fs" mode="root">
  <table class="fs">
    <xsl:apply-templates select="f"/>
  </table>
</xsl:template >
<!-- End Feature -->

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="fs/f">
  <tr class="fval">
    <td class="feature">
      <xsl:value-of select="@name"/>
    </td>
    <td class="fvalue">
      <xsl:apply-templates/>
    </td>
  </tr>
</xsl:template >
<!-- End Feature -->

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="fs/f" mode="onelevel">
  <td class="feature">
    <xsl:value-of select="@name"/>
  </td>
</xsl:template >
<!-- End Feature -->

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="sym|symbol">
  <xsl:value-of select="@value"/>
</xsl:template >
<!-- End Feature -->

<xsl:template match="plus">
  <xsl:text>+</xsl:text>
</xsl:template>

<xsl:template match="minus">
  <xsl:text>-</xsl:text>
</xsl:template>

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<!-- Feature -->
<xsl:template match="fs">
  <table class="fs">
    <xsl:apply-templates select="f"/>
  </table>
</xsl:template >
<!-- End Feature -->


<!-- Variables -->
<xsl:template match="var">
  <table class="var">
    <td class="varname">
      <xsl:value-of select="@name"/>
    </td>
  </table>
</xsl:template>

<xsl:template match="var[./*]">
  <table class="var varwithvalue">
    <td class="varname">
      <xsl:value-of select="@name"/>
    </td>
    <td class="fvalue varvalue">
      <xsl:apply-templates/>
    </td>
  </table>
</xsl:template>


<xsl:template match="vAlt">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())"><span class="vAlt">|</span></xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="not">
  <span class="notop">~</span>
  <xsl:apply-templates/>
</xsl:template>

<!-- Nodes -->

<xsl:template match="optional">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="interleave" mode="nodemark">
  ##
</xsl:template>

<xsl:template match="alternative" mode="nodemark">
  |
</xsl:template>

<xsl:template match="sequence" mode="nodemark">
  .
</xsl:template>

<xsl:template match="interleave|sequence|alternative">
  <table class="node" type="{name(.)}" id="{generate-id(.)}">
    <xsl:variable name="type">
      <xsl:choose>
        <xsl:when test="parent::optional[@n='*']">nodestar</xsl:when>
        <xsl:when test="parent::optional">nodeopt</xsl:when>
        <xsl:otherwise>nodespecial</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <caption class="nodelabel">
      <xsl:if test="node|interleave|sequence|alternative|optional/node|optional/sequence">
	<xsl:attribute name="internal">
	  <xsl:text>yes</xsl:text>
	</xsl:attribute>
      </xsl:if>
      <div class="{$type}">
        <xsl:apply-templates select="." mode="nodemark"/>
        <xsl:if test="@id">
          <sub class="nodeid" id="{generate-id(.)}">
            <xsl:value-of select="@id"/>
          </sub>
        </xsl:if>
    </div>
    </caption>
    <tr class="nodebody">
      <xsl:for-each select="node|interleave|sequence|alternative|optional/node|optional/sequence">
        <td class="nodechild">
          <xsl:apply-templates select="."/>        
        </td>
      </xsl:for-each>
    </tr>
  </table>
</xsl:template>

<xsl:template match="node">
  <table class="node" type="{@type}">
    <caption class="nodelabel">
      <xsl:if test="node|interleave|sequence|alternative|optional/node|optional/sequence">
	<xsl:attribute name="internal">
	  <xsl:text>yes</xsl:text>
	</xsl:attribute>
      </xsl:if>
      <xsl:variable name="type">
        <xsl:choose>
          <xsl:when test="parent::optional[@n='*']">nodestar</xsl:when>
          <xsl:when test="parent::optional">nodeopt</xsl:when>
          <xsl:when test="guards">nodeguard</xsl:when>
          <xsl:otherwise>nodestd</xsl:otherwise>
        </xsl:choose>
      </xsl:variable>
      <div class="{$type}">
        <xsl:choose>
             <xsl:when test='@adj="no" and (node or @type="anchor" or @type="coanchor") '>
	       <xsl:text>-</xsl:text>
	     </xsl:when>
             <xsl:when test='@adj="strict"'>
	       <xsl:text>+</xsl:text>
	     </xsl:when>
        </xsl:choose>
        <xsl:choose>
          <xsl:when test="@type = 'foot'">
            <xsl:text>*</xsl:text>
            <xsl:value-of select="@cat"/>
          </xsl:when>
          <xsl:when test="@type = 'anchor'">
            <xsl:text>&lt;&gt;</xsl:text>
            <xsl:value-of select="@cat"/>
          </xsl:when>
          <xsl:when test="@type = 'coanchor'">
            <xsl:text>&lt;=&gt;</xsl:text>
            <xsl:value-of select="@cat"/>
          </xsl:when>
          <xsl:when test="@type = 'lex'">
            <xsl:value-of select="@lex"/>
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="@cat"/>
          </xsl:otherwise>
        </xsl:choose>
        <xsl:if test="@id">
	  <sub class="nodeid" id="{generate-id(.)}"> 
	    <xsl:value-of select="@id"/>
	  </sub>
        </xsl:if>
      </div>
    </caption>
    <tr class="nodebody">
      <xsl:for-each select="node|interleave|sequence|alternative|optional/node|optional/sequence">
        <td class="nodechild">
          <xsl:apply-templates select="."/>        
        </td>
      </xsl:for-each>
    </tr>
  </table>
</xsl:template>

<xsl:template match="node[@id]" mode="avm">
  <xsl:variable name='identifiant'>
    <xsl:value-of select="@id"/>
  </xsl:variable>
  <xsl:if test="narg">
    <td>
    <table id="decoration{generate-id(.)}" class="narg">
      <caption>
        <xsl:value-of select="$identifiant"/>
      </caption>
      <td>
      <table class="fs">
        <xsl:apply-templates select="narg" mode="narg"/>
      </table>
    </td>
    </table>
    </td>
  </xsl:if>
</xsl:template>

<xsl:template match="tree|node|sequence|interleave|alternative|optional" mode="recurseavm">
  <xsl:apply-templates select="node[@id]" mode="avm"/>
  <xsl:apply-templates select="node|optional|interleave|alternative|sequence" mode="recurseavm"/>
</xsl:template>

<xsl:template match="narg" mode="narg">
  <tr class="fval">
    <td class="feature">
      <xsl:value-of select="@type"/>
    </td>
    <td class="fvalue">
      <xsl:apply-templates/>
    </td>
  </tr>
</xsl:template>

<xsl:template match="tree|node|sequence|alternative|interleave|optional" mode="recurseguard">
  <xsl:apply-templates select="*[@id and guards]" mode="guard"/>
  <xsl:apply-templates select="node|optional|interleave|alternative|sequence" mode="recurseguard"/>
</xsl:template>

<xsl:template match="*[@id and guards]" mode="guard">
    <td>
      <div id="guard{generate-id(.)}">
	<table  class="guards">
	  <caption>
	    <xsl:value-of select="@id"/>
	  </caption>
	  <td>
	    <xsl:apply-templates select="guards" mode="guard"/>
	  </td>
	</table>
      </div>
  </td>
</xsl:template>

<xsl:template match="guards[@rel='+']" mode="guard">
  <table class="pguard">
    <xsl:apply-templates mode="guard"/>
  </table>
</xsl:template>

<xsl:template match="guards[@rel='-']" mode="guard">
  <table class="nguard">
    <xsl:apply-templates mode="guard"/>
  </table>
</xsl:template>

<xsl:template match="guard" mode="guard">
  <tr>
    <td>
      <xsl:apply-templates select="*[1]"/>
    </td>
    <td>
      =
    </td>
    <td>
      <xsl:apply-templates select="*[2]"/>
    </td>
  </tr>
</xsl:template>

<xsl:template match="xguard" mode="guard">
  <tr class="xguard">
    <xsl:for-each select="*">
      <td>
        <xsl:apply-templates select="."/>
      </td>
    </xsl:for-each>    
  </tr>
</xsl:template>


<xsl:template match="and" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="or" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="or[count(*) > 1]" mode="guard">
  <tr>
    <td> OR </td>
    <td colspan="2">
      <table class="orguard">
        <xsl:for-each select="*">
          <tr>
            <td>
              <table class="orblock">
                <xsl:apply-templates select="." mode="guard"/>
              </table>
            </td>
          </tr>
        </xsl:for-each>
      </table>
    </td>
  </tr>
</xsl:template>

</xsl:stylesheet>

