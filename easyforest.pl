/* -*- mode:prolog; -*-
 ******************************************************************
 * $Id$
 * Copyright (C) 2004, 2005, 2006, 2007 by INRIA 
 * Author: Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
 * ----------------------------------------------------------------
 *
 *  easyforest.pl -- Conversion to EASy format
 *
 * ----------------------------------------------------------------
 * Description
 *  Conversion of DyALog dependency forest to expected EASy format
 * ----------------------------------------------------------------
 */

:-import{ module=> xml,
	  file => 'libdyalogxml.pl',
	  preds => [
		    reader/2,
		    stream_reader/3,
		    read_event/3,
		    read_raw_event/2,
		    event_handler/5,
		    event_process/3,
		    event_process/4,
		    event_ctx/2,
		    event_super_handler/2
		   ]
         }.


:-import{ module=> sqlite,
	  file => 'libdyalogsqlite.pl'
	}.

%% To undo potential presence of -parse in compile flags
%% important to be in this mode to parse the list of options to easyforest
:-parse_mode(list).

:-require('format.pl').
:-require('rx.pl').

:-features(ctx,[class,node,constraint,current]).

:-features(cluster, [id,left,right,lex,token] ).
:-features(node, [id,cluster,cat,tree,lemma,form,xcat,deriv,w] ).
:-features(edge, [id,source,target,type,label,deriv] ).
:-features(op,[id,cat,deriv,span,top,bot]).

:-features(f,[id,lex,cid,rank]).
:-features(groupe, [id,type,content,left,right] ).
:-features(relation, [id,type,arg1,arg2,arg3] ).

:-extensional
	cluster{},
	node{},
	edge{},

	f{},
	groupe{},
	op{},
	sentence/1,
	used/1,
	opt/1,
	extra_edge/2,
	deriv2span/2
	.

:-rec_prolog relation{}.

:-finite_set(cat,
	     [-,'CS','N','N2','PP','S','V','VMod',
	      adj,adv,advneg,
	      aux,v,
	      cla,cld,clg,cll,cln,clr,clneg,
	      comp,coo,csu,det,
	      nc,np,number,ncpred,
	      poncts,ponctw,
	      prel,prep,pri,pro,conj]).

:-subset( nominal, cat[nc,cln,pro,np,pri,prel,ncpred] ).
:-subset( xnominal, cat[nc,cln,pro,np,pri,prel,ncpred,adj] ).

:-subset( terminal, cat[~ [-,'CS','N','N2','PP','S','V']] ).

:-finite_set(pos,[left,right,xleft,xright,in,out,xin,xout]).

:-finite_set(const,['GN','GA','GP','NV','GR','PV']).

:-finite_set(rel,[ 'SUJ-V',	% sujet-verbe
		   'AUX-V',	% auxiliaire-verbe
		   'COD-V',	% cod-verbe
		   'CPL-V',	% complement-verbe
		   'MOD-V',	% modifier verbe
		   'COMP',	% complementeur: csu->NV, prep->(GN,NV)
		   'ATB-SO',	% attribut sujet/objet  
		   'MOD-N',	% modifier nom
		   'MOD-A',	% modifier adjectif
		   'MOD-R',	% modifier adverbe
		   'MOD-P',	% modifier preposition
		   'COORD',	% coordination
		   'APPOS',	% apposition
		   'JUXT'	% juxtaposition
		   ]).

:-finite_set(label,[ subject,object,arg,preparg,scomp,comp,xcomp,
		     'Infl','V1','V',v,'vmod',
		     cla,cld,clg,cll,clr,clneg,
		     void,coo,coo2,coord2,coord3,
		     det,det1, incise,en,
		     nc,np,number,ncpred,
		     'CS','Modifier',
		     'N','N2','N2app','Nc2',
		     'PP',prep,csu,
		     prel,pri,pro,'Root','Punct',
		     'S','S2','SRel','SubS',varg,
		     start,wh,starter,advneg,aux,ce
		     ]).

:-finite_set(edge_kind,[adj,subst,lexical,virtual,epsilon]).

:-finite_set(unknown,['uw','_Uw']).
:-finite_set(date,['_DATE_arto','_DATE_artf','_DATE_year']).
:-finite_set(time,[arto,artf,arti]).
	     
:-xcompiler((xml!wrapper(Handler,Ctx,Name,Attr,G) :-
	     event_process(Handler,start_element{ name => Name, attributes => Attr },Ctx),
	     G,
	     event_process(Handler,end_element{ name => Name },Ctx)
	    )).

:-xcompiler((xml!text(Handler,Ctx,Text) :-
	    event_process(Handler,characters{ value => Text }, Ctx ))).


:-extensional agglutinate/3.

agglutinate('�','le__det','au').
agglutinate('�','les__det','aux').
agglutinate('�','le','au').
agglutinate('�','les','aux').
agglutinate('�','lequel','auquel').
agglutinate('�','lesquels','auxquels').
agglutinate('�','lesquelles','auxquelles').
agglutinate('de','le__det','du').
agglutinate('de','le','du').
agglutinate('de','lequel','duquel').
agglutinate('de','lesquels','desquels').
agglutinate('de','lequelles','desquelles').
agglutinate('de','les__det','des').
agglutinate('en','les__det','�s').
agglutinate('de','les','des').
agglutinate('en','les','�s').
agglutinate('�','ledit','audit').
agglutinate('�','lesdits','auxdits').
agglutinate('�','lesdites','audites').
agglutinate('de','ledit','dudit').
agglutinate('de','lesdits','dudits').
agglutinate('de','lesdites','auxdites').

%% Special case of _SENT_BOUND introduced by sxpipe
:-finite_set(sbound,['*','-','_','+']).
agglutinate(X::sbound[],'_SENT_BOUND',X).

:-std_prolog
	init/0,
	reader/0,
	emit/0,
	convert/0
	.

init :-
	true
	.

reader :-
	stream_reader([],Reader,_),
	event_handler(Reader,loop,eof,[],[])
	.
	    
emit :-
	sentence(Sent),
	name_builder('E~w',[Sent],SId),
	recorded(mode(Mode)),
	event_ctx(Ctx,0),
	Handler=default([]),
	event_process(Handler,start_document,Ctx),
%%	event_process(Handler,pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},Ctx),
	event_process(Handler,xmldecl,Ctx),
	xml!wrapper(Handler,
		    Ctx,
		    'DOCUMENT',
		    [id='frmg',
		     xmlns!xlink= 'http://www.w3.org/1999/xlink'],
		    xml!wrapper(Handler,
				Ctx,
				'E', [ id:SId, mode:Mode ],
				%% Constituants and relations
				event_process(Handler,[constituants,relations],Ctx)
			       )
		   ),
	event_process(Handler,end_document,Ctx)
	.

depxml_emit :-
	sentence(Sent),
	name_builder('E~w',[Sent],SId),
	recorded(mode(Mode)),
	event_ctx(Ctx,0),
	Handler=default([]),
	event_process(Handler,start_document,Ctx),
%%	event_process(Handler,pi{name=>xml,value=> [version:'1.0',encoding:'latin1']},Ctx),
	event_process(Handler,xmldecl,Ctx),
	( recorded(mode(R)) ->
	  Attrs = [mode:R]
	;
	  Attrs = []
	),
	xml!wrapper(Handler,
		    Ctx,
		    'dependencies',
		    Attrs,
		    event_process(Handler,[depclusters,depnodes,depedges,depops],Ctx)
		   ),
	event_process(Handler,end_document,Ctx)
	.

:-std_prolog keep_only_best/2.

keep_only_best(CIds,EIds) :-
	every(( _E::edge{ id => _EId },
		\+ domain(_EId,EIds),
		( '$answers'(edge_cost(_EId,_W)) xor _W='none'),
		verbose('Erase w=~w edge ~E\n',[_W,_E]),
		erase( _E )
	      )),
	%% keep nodes related to existing edges or to cluster not covered by the clusters in CIds
	every(( _N::node{ id => _NId,
			  cluster => _C::cluster{ id => _CId, lex => _Lex },
			  deriv => _Derivs },
		( edge{ source => _N} -> true
		; edge{ target => _N } -> true
		; _Lex \== '',
		  domain(__CId,CIds),
		  cluster_overlap(_C,__C::cluster{ id => __CId }),
		  ( _CId \== __CId
		  xor edge{ source => node{ cluster => cluster{ id => _CId } } }
		  xor edge{ target => node{ cluster => cluster{ id =>_CId } } }
		  )
		  ->
		  verbose('Erase node ~E overlap with ~E\n',[_N,__C]),
		  erase( _N )
		; _Lex == '' ->
		  verbose('Erase node ~E emptylex\n',[_N]),
		  erase( _N )
		;
		  %% keep a non empty node
		  %% which is neither an edge source nor target
		  true
		)
	      )),
	every(( _C::cluster{ lex => Lex},
		Lex \== '',
		\+ node{ cluster => _C },
		verbose('Erase cluster ~E\n',[_C]),
		erase(_C)
	      )),
	every((
	       _N::node{ deriv =>NDerivs, id => _NId },
	       keep_derivs(_N)
	      )),
	every(( _O::op{ deriv => ODerivs },
		\+ (( domain(DId,ODerivs),
		      recorded( keep_deriv(DId))
		    )),
		erase(_O))),
	(   \+ opt(verbose)
	xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
	    every(( _N::node{}, format('Node ~E\n',[_N]) )),
	    every(( _E::edge{ id => _EId },
		    '$answers'(edge_cost(_EId,_W)),
		    format('Edge ~w: ~E\n',[_W,_E]) ))
	)
	.

convert :-
	%% Add a virtual root node
	record( CRoot::cluster{ id => root, lex => '', token => '', left => 0, right => 0 } ),
	mutable(MRDerivs,[]),
	every(( N::node{ id => NId, cat => Cat },
		(\+ N = NRoot),
		((   \+ edge{ target => N } ) xor Cat = cat[v] ),
		mutable_read(MRDerivs,_L),
		mutable(MRDerivs,[root(NId)|_L])
	      )),
	mutable_read(MRDerivs,RDerivs),
	record( NRoot::node{ id => root, cluster => CRoot, cat => [], tree => [], deriv => RDerivs }),
	every(( domain(root(NId),RDerivs),
		NN::node{ id => NId },
		verbose('Add root edge ~E\n',[NN]),
		record_without_doublon( edge{ id => root(NId),
					      source => NRoot,
					      target => NN,
					      label => root,
					      type => virtual,
					      deriv => [root(NId)]
					    } )
	      )),
	wait( best_parse(root,dummy,_,_,_,_) ),
	every(( \+ recorded( mode(robust) ),
		recorded( list_best_parse(root,M_LBP) ),
		mutable_read(M_LBP,LBP),
		%%		verbose('LIST ~w\nsearch ~w ~w ~w\n',[LBP,CIds,EIds,W]),
		domain(best_parses(root,W,Span,CIds,EIds),LBP),
		verbose('Best parse from virtual root:\n\tcids=~w\n\teids=~w\nw=~w\n\n',[CIds,EIds,W])
		->
%%		verbose('Found best parse ~w: ~w ~w\n', [W,CIds,EIds]),
		verbose('Found best parse ~w: ~w ~w\n', [W,CIds,EIds]),
		(\+ opt(verbose)
		xor every(( domain(_EId,EIds),
			    _E::edge{ id => _EId },
			    '$answers'(edge_cost(_EId,_W)),
			    format('\t~w: ~E\n',[_W,_E]) ))),
		keep_only_best(CIds,EIds)
	      ;	  find_best_parse_coverage ->
		(   \+ opt(verbose)
		xor every(( _C::cluster{}, format('Cluster ~E\n',[_C]) )),
		    every(( _N::node{}, format('Node ~E\n',[_N]) )),
		    every(( _E::edge{ id => _EId },
			    '$answers'(edge_cost(_EId,_W)),
			    format('Edge ~w: ~E\n',[_W,_E]) ))
		)
	      ;	  
		  verbose('Not found best parse\n', [])
	      )),
	( opt(depxml)
	xor
	every(( word(0) )),
	verbose('Done word\n',[]),
	every(( domain(Type,['NV','GN','GA','GR','PV','GP']),
		verbose('Try build group ~w\n',[Type]),
		build_group(Type),
		verbose('Done build group ~w\n',[Type]),
		true
	      )),
	verbose('Done group\n',[]),
	  %% verbose('Starting coord expand\n',[]),
	  %%	  (coord_expand xor true),
	  %% verbose('Done coord expand\n',[]),
	every(( extract_relation( relation{ type => rel[] } )))
	)
	.


:-light_tabular keep_derivs/1.
:-mode(keep_derivs/1,+(+)).

keep_derivs(N::node{ deriv => NDerivs, id =>NId }) :-
	%% mark all derivs to be kept
	%%   by traversing in a top-down way nodes
	every(( edge{ source => node{ id => NId }, target => N2 },
		keep_derivs(N2) )),
	%% we can now assume that derivations on N's descendants have been marked
	%% we look derivations for N
	%% we keep a deriv D for N if
	%%    - it belongs to the derivs of some node N
	%%    - it belongs to the derivs of all edges starting from N
	%%    - its termop are either terminal or sourceop of an edge reached from N
	( NDerivs == [[]] ->
	  true
	; NId == root ->
	  domain(DId,NDerivs),
	  record_without_doublon( keep_deriv(DId) )
	;
	  domain(DId,NDerivs),
	  verbose('test deriv ~w from node ~w\n',[DId,NId]),
	  \+ ( edge{ id => EId,
		     source => node{ id => NId},
		     target => node{ id => NId2},
		     deriv => EDerivs },
	       ( \+ domain(DId,EDerivs)
	       xor recorded( deriv(DId,EId,_,_,TOP) ),
		 edge{ id => EId2,
		       source => node{ id => NId2 },
		       deriv => EDerivs2
		     },
		 \+ ( domain(DId2,EDerivs2),
		      recorded( deriv(DId2,EId2,_,SOP2,_)),
		      recorded( keep_deriv(DId2) ),
		      TOP=SOP2
		    )
	       )
	     ),
	  verbose('keep deriv ~w\n',[DId]),
	  record_without_doublon( keep_deriv(DId) )
	).

:-std_prolog clusters_coverage/2.

clusters_coverage(CIds::[CId|_],Length) :-
	cluster{ id => CId, left => Left, right => Right },
	bound_max(CIds,Right,XRight),
	Length is XRight - Left
	.


:-std_prolog find_best_parse_coverage/0.

find_best_parse_coverage :-
	verbose('Try find best coverage\n',[]),

/*	every(( N::node{id=>NId},
		\+ recorded( list_best_parse(NId,_) ),
		best_parse(NId,dummy,_,_,_,_)
	      )),
*/
	mutable(M,[]),
	every((
%%	       '$answers'( O::best_parse(NId,CIds,EIds,W) ),
%%	       edge{ type => virtual, target => node{ id => NId } },
	       node{ id => NId },
	       NId \== root,
	       recorded( list_best_parse(NId,M_L_NId) ),
	       mutable_read(M_L_NId,L_NId),
	       domain(best_parses(NId,W,Span,CIds,EIds), L_NId),
	       O = best_parse(NId,Span,CIds,EIds,W),
	       %% no need to add best parse covering empty span !
	       \+ (Span = [_L,_L] ),
	       \+ (Span = [_L,_L,_L,_L] ),
	       clusters_coverage(CIds,Length),
	       mutable_read(M,_L),
	       %%	       Length > 1,
	       length_sorted_add(Length,O,_L,_LL),
	       mutable(M,_LL)
	      )),
	mutable_read(M,L),
	mutable(M,[]),
	verbose('Sorted partial best parses ~w\n',[L]),
	best_parse_traversal(L,CIds,EIds),
	verbose('Second try: found best parse by gluing ~w ~w\n',[CIds,EIds]),
	keep_only_best(CIds,EIds),
	true
	.

:-std_prolog generalized_domain/2.

generalized_domain(C::cluster{ id => CId }, CIds ) :-
	(   domain(CId,CIds)
	xor cluster_overlap(C,C2::cluster{ id => CId2 }),
	    domain(CId2,CIds)
	)
	.
			    
	      
:-std_prolog best_parse_traversal/3.

best_parse_traversal( L,
		      CIds,
		      EIds
		    ) :-
	mutable(MCIds,[]),
	mutable(MEIds,[]),
	every(( domain([Length|KL],L),
		domain([W|WL],KL),
		domain(best_parse(NId1,Span,CIds1,EIds1,_),WL),
		mutable_read(MCIds,_CIds),
		mutable_read(MEIds,_EIds),
		cluster!safe_concat(CIds1,_CIds,NewCIds),
		edge_safe_concat(EIds1,_EIds,NewEIds),
		verbose('Best parse recovery: agglutinate length=~w weight=~w node=~w\n',[Length,W,NId1]),
		mutable(MCIds,NewCIds),
		mutable(MEIds,NewEIds)
	      )),
	mutable_read(MCIds,CIds),
	mutable_read(MEIds,EIds)
	.

:-rec_prolog length_sorted_add/4.

length_sorted_add(Length,O::best_parse(_,_,_,_,W),[],[[Length|WL]]) :-
	weight_sorted_add(W,O,[],WL).
	
length_sorted_add( Length,
		   O::best_parse(_,_,_,_,W),
		   L::[X::[_Length|WL]|L2],
		   XL
		 ) :-
	( Length == _Length ->
	    XL = [[Length|XWL]|L2],
	    weight_sorted_add(W,O,WL,XWL)
	;   Length > _Length ->
	    XL = [[Length|XWL]|L],
	    weight_sorted_add(W,O,[],XWL)
	;   
	    XL = [X|XL2],
	    length_sorted_add(Length,O,L2,XL2)
	)
	.

:-rec_prolog weight_sorted_add/4.

weight_sorted_add(W,O,[],[[W,O]]).
weight_sorted_add(W,O,L::[X::[_W|KL]|L2],XL) :-
	( W == _W ->
	    XL = [[W,O|KL]|L2]
	;   W > _W ->
	    XL = [[W,O]|L]
	;   
	    XL= [X|XL2],
	    weight_sorted_add(W,O,L2,XL2)
	)
	.

:-prolog (dcg parse_options/0, parse_option/0).

parse_options --> parse_option, parse_options ; [].
parse_option -->
        ( ['-e',Id] -> { record( sentence(Id) )}
	;   ['-verbose'] -> { record_without_doublon( opt(verbose) ) }
	;   ['-restrictions',File] -> {
				       verbose('open restriction database ~w\n',[File]),
				       sqlite!open(File,DB),
				       record_without_doublon( opt(restrictions(DB,File)) )
				      }
	;   ['-nodis'] -> { record_without_doublon(opt(nodis)),
			    record_without_doublon(opt(depxml))
			    }
	;   ['-depxml'] -> { record_without_doublon(opt(depxml)) }
	;   ['-xmldep'] -> { record_without_doublon(opt(depxml)) }
	;   ['-weights'] -> { record_without_doublon(opt(weights)) }
	;   { fail }
        )
        .

main :-
	init,
	argv( Argv ),
	phrase( parse_options, Argv, [] ),
	(   sentence(_) xor record( sentence(1) ) ),
	reader,
	(\+ opt(verbose) xor
	    every(( C::cluster{}, format('Cluster ~E\n',[C]) )),
	    every(( N::node{}, format('Node ~E\n',[N]) )),
	    every(( E::edge{}, format('Edge ~E\n',[E]) ))
	),
	(opt(nodis)
	xor wait((convert))
	),
	( opt(depxml) ->
	  depxml_emit
	;
	  emit
	),
	fail
	.

?- main.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Reducing ambiguities

:-light_tabular best_parse/6.
:-mode(best_parse/6,+(+,+,-,-,-,-)).

%% best_parse(NodeId,IncomingEId,ClusterIds,EdgeIds,Weight)

best_parse(NId,IncomingSpan,W,Span,CIds,EIds) :-
	N::node{ id => NId, cluster => C::cluster{ id => CId, lex => Lex } },
	verbose('Try find best parse from ~w entering from ~w\n',[NId,IncomingSpan]),
	best_parse_in(N,IncomingSpan),
	( recorded( list_best_parse(NId,M) )
	xor mutable(M,[]),
	  record(list_best_parse(NId,M))
	),
	every((
	       recorded( _BP::best_parses(NId,_W,_Span,_CIds,_EIds), Addr_BP ),
	       \+ recorded( registered_best_parse(Addr_BP) ),
	       record( registered_best_parse(Addr_BP) ),
	       mutable_read(M,_L),
	       add_best_parse( _BP, _L, _LL),
	       mutable(M, _LL)
	      )),
	mutable_read(M,L),
	domain( best_parses(NId,W,Span,CIds,EIds), L),
	(IncominSpan=Span xor check_span(IncomingSpan,Span)),
	verbose('Found best parse from ~E: weight=~w span=~w cids=~w eids=~w\n',[N,W,Span,CIds,EIds]),
	true
	.


:-rec_prolog add_best_parse/3.

add_best_parse(X,[],[X]).
add_best_parse(X::best_parses(_,WX,_,_,_),L::[Y::best_parses(_,WY,_,_,_)|L1],LL) :-
	( WX >= WY ->
	    LL = [X|L]
	;   add_best_parse(X,L1,LL1),
	    LL=[Y|LL1]
	)
	.

:-std_prolog best_parse_in/2.

best_parse_in( N::node{ id => NId,
			cluster => C::cluster{ id => CId, lex => Lex },
			deriv => All_Derivs
		      },
	       IncomingSpan
	     ) :-
	%% Get left-to-right sorted edges from N
	all_edges(N,All_Edges),
	verbose('All edges from ~E ~w: ~w\n',[N,All_Edges,All_Derivs]),
/*	every(( domain(EId,All_Edges),
		edge{ id =>EId, target => node{ id => NId1} },
		verbose('Try from ~w ~w\n',[EId,NId1]),
		best_parse(NId1,EId,_,_,_,_)
	      )),
*/
	every((
	       ( All_Derivs == [] ->
		 Deriv = dummy
	       ; IncomingSpan=dummy ->
		 domain(Deriv,All_Derivs)
	       ;   
		 domain(Deriv,All_Derivs),
		 verbose('\tTry selecting deriv=~w for span=~w\n',[Deriv,IncomingSpan]),
		 ( Deriv = []
		 xor
		 deriv2span(Deriv,XSpan),
		 verbose('\t\twith span=~w\n',[XSpan]),
		   %%		 XPcheck_span(IncomingSpan,XSpan)
		   IncomingSpan=XSpan
		 ),
		 verbose('\t\t\tselected\n',[]),
		 true
	       ),
	       %% select a derivation and subset of edges comptabilble with it
	       %% return best parses for this derivation
	       edge_select(N,All_Edges,Deriv,_CIds,_EIds,_W_Raw),
	       verbose('Finished edge select ~E deriv=~w\n',[N,Deriv]),
	       verbose('Found pre parse from ~E for deriv=~w: ~w ~w ~w\n',[N,Deriv,_W_Raw,_CIds,_EIds]),
	       
	       mutable(_WM,_W_Raw),
	       every(( node_cost_regional(NId,All_Edges,_CIds,_EIds,_W_Reg),
		       mutable_read(_WM,_W1),
		       _W2 is _W1 + _W_Reg,
		       mutable(_WM,_W2) )),
	       mutable_read(_WM,_W),	   
	       verbose('Found parse from ~E: ~w ~w ~w\n',[NId,_W,_CIds,_EIds]),

	       compute_span(_CIds,_Span),
	       verbose('Computed span span=~w cids=~w\n',[_Span,_CIds]),

	       %% Keep at most 10 best parses for distinct spans
	       %% (and at most 1 parse per span)
	       ( recorded( O::best_parses(NId,_W1,_Span,__CIds,__EIds) ) ->
		 ( _W1 > _W ->
		   verbose('Dont keep: better w=~w span=~w cids=~w eids=~w\n',[_W1,_Span,__CIds,__EIds]),
		   true
		 ;   _W1 < _W,
		   safe_erase(O),
		   verbose('Erase previous best parse w=~w span=~w\n',[_W1,_Span]),
		   verbose('Record best parse ~w ~w ~w\n',[_W,_CIds,_EIds]),
		   record_without_doublon( best_parses(NId,_W,_Span,_CIds,_EIds) )
		 ;   _W1 = _W, 
		   random(K), %% Random choice to keep or replace
		   ( 0 is K mod 2
		   xor
		     verbose('Dont keep: better w=~w span=~w cids=~w eids=~w\n',[_W1,_Span,__CIds,__EIds]),
		     fail
		   ),
		   safe_erase(O),
		   verbose('Erase previous best parse w=~w span=~w\n',[_W1,_Span]),
		   verbose('Record best parse ~w ~w ~w\n',[_W,_CIds,_EIds]),
		   record_without_doublon( best_parses(NId,_W,_Span,_CIds,_EIds) )
		 )
	       ;   value_counter(NId,V), V  < 5 ->
		 verbose('Record best parse ~w ~w ~w\n',[_W,_CIds,_EIds]),
		 record_without_doublon( best_parses(NId,_W,_Span,_CIds,_EIds) ),
		 update_counter(NId,_)
	       ;   %% Max best parses recorded for NId
		 %% Keep current one only if we can remove the worst one wrt Weight
		 recorded( O2::best_parses(NId,_W2,_Span2,_,_)),
		 _W2 < _W,
		 \+ ( recorded( best_parses(NId,_W3,_,_,_)),
		      _W3 < _W2 ) ->
		 safe_erase(O2),
		 verbose('Erase previous best parse w=~w span=~w\n',[_W2,_Span2]),
		 verbose('Record best parse ~w ~w ~w\n',[_W,_CIds,_EIds]),
		 record_without_doublon( best_parses(NId,_W,_Span,_CIds,_EIds) )
	       ;   %% Max best parses already recorded all better than _W
		 %% do nothing
		 recorded( O2::best_parses(NId,_W2,_Span2,__CIds,__EIds)),
		 _W2 > _W,
		 verbose('Dont keep: better w=~w span=~w cids=~w eids=~w\n',[_W2,_Span2,__CIds,__EIds]),
		 true
	       )
	      )
	     )
	.


:-std_prolog compute_span/2.

compute_span([CId1|CIds],X) :-
	cluster{ id => CId1, left => L1, right => R1 },
	( CIds = [] ->
	  X = [L1,R1]
	;
	  compute_span(CIds,[L|Rest]),
	  ( R1 = L ->
	    X = [L1|Rest]
	  ;
	    %% Found a hole
	    %% but at most one hole accepted
	    Rest = [_],
	    X = [L1,R1,L|Rest]
	  )
	)
	.

:-extensional check_span/2.

check_span([L,R],[L,R]).

%% Wrap. adjoing with hole proper in the middle
check_span([L,R,L1,R1],[L,L1,R1,R]).

%% Tig adjoining
check_span([L,L,L,R],[L,R]).
check_span([L,R,R,R],[L,R]).

%% Wrap adjoining, but with hole on left or right
check_span([L,R,LHole,R],[L,LHole]).
check_span([L,R,L,RHole],[RHole,R]).
			      
:-light_tabular all_edges/2.
:-mode(all_edges/2,+(+,-)).

all_edges( N::node{ id => NId, cluster => C::cluster{ id => CId, lex => Lex } },
	   All_Edges
	 ) :-
	%% return left-to-right sorted set of all out edges
	mutable(ME,[]),
	every(( edge{ source => N, id => EId, target => _N::node{ id => _NId} },
		mutable_read(ME,_L),
		edge_sorted_add(EId,_L,_LL),
		mutable(ME,_LL)
	      )),
	mutable_read(ME,All_Edges)
	.
	

:-light_tabular allowed_empty_parse/1.

allowed_empty_parse(node{ cat => start, cluster => cluster{ lex => '' }}).
allowed_empty_parse(node{ cat => end, cluster => cluster{ lex => '' }}).

:-light_tabular wrapper/1.

wrapper(node{ cat => incise }).

:-finite_set(quoted,[double_quoted,chevron_quote,simple_quoted]).

wrapper(node{ tree => Tree }) :- domain(quoted[],Tree).

:-std_prolog edge_safe_concat/3.

edge_safe_concat(L,L2,L4) :-	  
	( L == [] ->
	    L2 = L4
	;   L= [E|L1],
	    edge_safe_concat(L1,L2,L3),
	    edge_safe_add(E,L3,L4)
	)
	.

:-std_prolog edge_safe_add/3.

edge_safe_add(A,L,XL) :-
	( L = [] -> XL = [A]
	;   L=[B|L2],
	    (	A == B -> fail
	    ;	A @< B -> XL = [A|L]
	    ;	XL=[B|XL2],
		edge_safe_add(A,L2,XL2)
	    )
	)
	.

:-rec_prolog edge_sorted_add/3.

edge_sorted_add(EId,[],[EId]).
edge_sorted_add(EId1,L::[EId2|L2],XL) :-
	%% all edges are assumed to share the same source
	EId1 \== EId2,
	edge{ id => EId1, target => node{ cluster => cluster{ left => Left1, right => Right1 }}},
	edge{ id => EId2, target => node{ cluster => cluster{ left => Left2, right => Right2 }}},
	(   ( Left1 < Left2
	    xor (Left1 == Left2,
		    (	Right1 < Right2
		    xor Right1 == Right2, EId1 @< EId2 ))) ->
	    XL = [EId1|L]
	;   
	    edge_sorted_add(EId1,L2,XL2),
	    XL = [EId2|XL2]
	)
	.

:-std_prolog safe_erase/1.

safe_erase(G) :-
        every(( recorded(G,Addr),
                delete_address(Addr) ))
        .

:-std_prolog edge_select/6.

edge_select(N::node{ id => NId,
		     cluster => C::cluster{ id => CId, lex => Lex, left => Left } },
	    Edges,
	    Deriv,
	    CIds,
	    EIds,
	    W
	   ) :-
	verbose('Enter Edge select ~E deriv=~w ~w\n',[N,Deriv,Edges]),
	( Edges = [] ->
	  EIds = [],
	  CIds = [CId],
	  W = 0
	;
	  Edges = [EId1|Edges2],
	  E1::edge{ id=>EId1,
		    target => N1::node{ id => NId1,
					cluster => C1::cluster{ left => Left1, right => Right1 }},
		    source => N,
		    deriv => Derivs1
		  },
	  edge_select(N,Edges2,Deriv,CIds2,EIds2,W2),
	  verbose('Middle Edge select ~E ~w deriv=~w=> ~w ~w\n',[N,Edges,Deriv,CIds2,EIds2]),
	  verbose('Edge examine ~E derivs=~w\n',[E1,Derivs1]),
	  ( domain(Deriv,Derivs1) ->
	    verbose('Edge keep ~E d=~w _d=~w deriv=~w\n',[E1,D,_D,Deriv]),
	    ( Deriv = root(NId1) ->
	      Span1 = [_,_]
	    ;
	      recorded( deriv(Deriv,EId1,Span1,_,_) )
	    ),
	    verbose('Found span ~w for ~w => best_parse for ~w ~w\n',[Span1,EId1,NId1,Span1]),
	    best_parse(NId1,Span1,W1,_Span1,CIds1,_EIds1),
	    verbose('Retrieved best parse ~w deriv=~w: w=~w span=~w cids=~w eids=~w\n',[NId1,Deriv,W1,_Span1,CIds1,_EIds1]),
	    check_span(Span1,_Span1),
	    verbose('Checked span ~w ~w\n',[Span1,_Span1]),
	    edge_cost(EId1,WE), W is WE+W1+W2,
	    verbose('Try safe concat ~w ~w\n',[CIds1,CIds2]),
	    cluster!safe_concat(CIds1,CIds2,CIds),
	    verbose('Success safe concat ~w ~w => ~w\n',[CIds1,CIds2,CIds]),
	    edge_sorted_add(EId1,_EIds1,EIds1),
	    verbose('Try edge safe concat ~w ~w => ~w\n',[EIds1,EIds2,EIds]),
	    edge_safe_concat(EIds1,EIds2,EIds),
	    verbose('Success edge safe concat ~w ~w => ~w\n',[EIds1,EIds2,EIds]),
	    true
	  ;
	    verbose('Edge discard ~E\n',[E1]),
	    W = W2,
	    CIds = CIds2,
	    EIds = EIds2
	  ),
	  true
	),	
	verbose('Return Edge select ~E ~w deriv=~w => ~w ~w ~w\n',[N,Edges,Deriv,W,CIds,EIds]),
	true
	.

%%:-light_tabular cluster_distance/3.
%%:-mode(cluster_distance/3,+(-,-,-)).

:-light_tabular target_pos/3.
:-mode(target_pos/3,+(+,-,-)).

target_pos(Pos,E,D) :-
	E::edge{ target => node{ cluster => C1::cluster{ left => Pos }},
		 source => node{ cluster => C::cluster{}}},
	cluster_distance(C,C1,D)
	.

:-light_tabular edge_rank/3.
:-mode(edge_rank/3,+(+,-,-)).

edge_rank( E::edge{ source => N,
		    target => T::node{ cluster => cluster{ left => Left, right => Right } }
		  },
	   Rank,
	   Dir
	 ) :-
	(   edge_to_right(E) ->
	    Dir = right,
	    (	E1::edge{ source => N,
			  target => T1::node{ cluster => cluster{ left => Left1,
								  right => Right1
								}
					    }},
		E1 \== E,
		edge_to_right(E1),
		Right1 =< Left,
		\+ ( edge{ source => N,
			   target => T2::node{ cluster => cluster{ left => Left2,
								   right => Right2
								 }
					     }},
		       Right2 =< Left,
		       Right1 =< Left2
		   )
	    ->	
		edge_rank(E1,Rank1,right),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	;   
	    Dir = left,
	    (	E1::edge{ source => N,
			  target => T1::node{ cluster => cluster{ left => Left1,
								  right => Right1
								}
					    }},
		E1 \== E,
		\+ edge_to_right(E1),
		Right =< Left1,
		\+ ( edge{ source => N,
			   target => T2::node{ cluster => cluster{ left => Left2,
								   right => Right2
								 }
					     }},
		       Right =< Left2,
		       Right2 =< Left1
		   ) ->  
		edge_rank(E1,Rank1,left),
		Rank is Rank1 + 1
	    ;	
		Rank is 1
	    )
	),
	verbose('Edge rank ~w ~w ~E\n',[Rank,Dir,E]),
	true
	.

:-std_prolog cluster_distance/3.

cluster_distance(C1::cluster{ left => L1, right => R1 }, C2::cluster{ left => L2, right => R2 },D) :-
	( R1 =< L2 ->
	    D is L2 - R1
	;
	    D is L1 - R2
	)
	.

:-rec_prolog simple_contiguous/2.

simple_contiguous([],_).
simple_contiguous(_,[]).
simple_contiguous([CId1|L2],L::[CId|_]) :-
	( L2 = [] ->
	    contiguous(CId1,CId)
	;   L2 = [CId2|L3],
	    contiguous(CId1,CId2) ->
	    simple_contiguous(L2,L)
	;   contiguous(CId1,CId),
	    simple_contiguous(L,L2)
	)
	.

:-light_tabular contiguous/2.

contiguous(CId1,CId2) :-
	cluster{ id => CId1, right => Pos },
	cluster{ id => CId2, left => Pos }
	.

:-std_prolog edge_fall_in/2.

edge_fall_in( E::edge{ target => N::node{ cluster => cluster{ id => CId, lex => Lex } } },
	      CIds
	    ) :-
	verbose('Try edge fall in ~E cids=~w\n',[E,CIds]),
	( domain(CId,CIds) ->
	    true
	;   
	    Lex == '',
	    \+ ( E2::edge{ source => N, target => node{ cluster => cluster{ id => CId2, lex => Lex2 } } },
		   verbose('Here ~E lex2=~w\n',[E2,Lex2]),
%%		   Lex2 \== '',
		   verbose('Here2 ~E\n',[E2]),
		   \+  edge_fall_in(E2,CIds))
	)
	.


:-light_tabular edge_to_right/1.

edge_to_right( edge{ id => EId,
		     source => node{ cluster => cluster{ right => Right } },
		     target => node{ cluster => cluster{ left => Left } }
		   } ) :-
	Right =< Left
	.

:-rec_prolog bound_max/3.

bound_max([],Boundary,Boundary).
bound_max([CId|CIds],Boundary,NewBoundary) :-
	cluster{ id => CId, left => Left, right => Right },
	( Right > Boundary ->
	  bound_max(CIds,Right,NewBoundary)
	;
	  bound_max(CIds,Boundary,NewBoundary)
	)
	.

:-light_tabular cluster_overlap/2.

cluster_overlap( C1::cluster{ lex => Lex1, id => CId1, left => Left1, right => Right1 },
		 C2::cluster{ lex => Lex2, id => CId2, left => Left2, right => Right2 }
	       ) :-
	%% overlap should now work with empty clusters
%%	verbose('Checking overlap ~E ~E\n',[C1,C2]),
	C1, Lex1 \== '',
	C2, Lex2 \== '',
	(   CId1 == CId2
	xor Left1 =< Left2, Left2 < Right1
	xor Left2 =< Left1, Left1 < Right2
	xor Left1 == Left2, Right1 == Right2
	),
	verbose('Overlap ~E ~E\n',[C1,C2]),
	true
	.

:-light_tabular edge_potential_optional/1.

edge_potential_optional(EId) :-
	edge{ id => EId, target => node{ cluster => C::cluster{ id => CId }}},
	edge{ id => _EId, target => node{ cluster => _C::cluster{ id => _CId }}},
	_EId \== EId,
	cluster_overlap(C,_C)
	.

edge_potential_optional(EId) :-
	edge{ id => EId,
	      target => N::node{ cluster => cluster{ lex => '', left => Left }}
	    },
	edge{ id => EId1,
	      source => N,
	      target => node{ cluster => C::cluster{ left => Left }}
	    },
	edge_potential_optional(EId1)
	.

edge_potential_optional(root(_)). %% Virtual nodes

:-std_prolog cluster!safe_concat/3.

cluster!safe_concat(L,L2,L4) :-	  
	( L == [] ->
	    L2 = L4
	;   L= [A|L1],
	    cluster!safe_concat(L1,L2,L3),
	    C::cluster{ id => A },
	    cluster!safe_concat_aux(C,L3,L4)
	)
	.

:-std_prolog cluster!safe_concat_aux/3.

cluster!safe_concat_aux( CA::cluster{ id => A, left => LeftA, right => RightA },
			 L1,
			 XL
		       ) :-
	( L1 == [] ->
	    XL = [A] 
	;   L1 = [B|L2],
	    CB::cluster{ id => B, left => LeftB, right => RightB },
	    ( cluster_overlap(CA,CB) -> fail
	    ;	RightA =< LeftB ->
		XL = [A|L1]
	    ;	cluster!safe_concat_aux(CA,L2,XL2),
		XL = [B|XL2]
	    )
	)
	.


:-light_tabular edge_cost/2.
:-mode(edge_cost/2,+(+,-)).

edge_cost(EId,W) :-
	Edge::edge{ id => EId, source => node{}, target => node{} },
	mutable(WM,0),
	verbose('Computing edge cost ~E\n',[Edge]),
	every(( edge_cost_elem(Name,Edge,_W),
		verbose('Edge cost ~w ~w ~E\n',[Name,_W,Edge]),
		mutable_read(WM,_W1),
		_W2 is _W1 + _W,
		mutable(WM,_W2)
	      )),
	mutable_read(WM,W),
	verbose('TOTAL Edge cost ~w ~E\n',[W,Edge]),
	true
	.

:-rec_prolog edge_cost_elem/3.

%% Penalty for adj dependencies
%% edge_cost_elem( edge{ type => adj }, -1 ).

%% Favor subst and lexical dependencies
edge_cost_elem( '+SUBST', edge{ type => subst , source => node{ cluster => cluster{ lex => Lex }}}, 10 ) :-
	Lex \== ''.
edge_cost_elem( '+LEXICAL', edge{ type => lexical , source => node{ cluster => cluster{ lex => Lex }}}, 20 ) :-
	Lex \== ''.

%% Favor ncpred
edge_cost_elem( '+NCPRED', edge{ type => lexical, source => node{ cat => v }, target => node{ cat => ncpred } }, 800).

%% Favor ncpred mod
edge_cost_elem( '+NCPREDMOD',
		edge{ type => adj,
		      label => ncpred,
		      source => node{ cat => v },
		      target => node{ cat => adv}
		    }, 30).


%% Favor verbal argument, except if date
edge_cost_elem( '+ARG', edge{ label => label[object,preparg,comp,xcomp],
			      target => node{ lemma => Lemma, deriv => Derivs }}, 1000 ) :-
	\+ domain(Lemma,date[]),
	\+ ( domain(D,Derivs),
	     recorded(deriv(D,EId,_,OId,_)),
	     check_op_top_feature(OId,time,time[])
	   )
	.


%% Favor preparg->de over object
edge_cost_elem( '+ARG_prepobj_de',
		edge{ label => label[preparg],
		      target => node{ form => de }},
		1000
	      ).


%% Penalty on comp when N2, (confusion with inverted subject or object or even past participle)
edge_cost_elem( '-N2asComp',
		edge{ label => label[comp],
		      source => node{ cluster => CV::cluster{ right => RV } },
		      target => N::node{ cat => comp }
		    },
		-600
	      ) :-
	edge{ source => N,
	      target => node{ cat => cat[nc,np], cluster => C::cluster{ left => LN }},
	      label => 'N2',
	      type => subst
	    },
	( edge{ label => label[subject,object],
		target => node{ cluster => C }
	      }
	;
	  edge{ source => node{ cluster => C },
		label => 'Infl',
		target => node{ cat => aux }
	      }
	)
	.

%% penalize date as args
edge_cost_elem( '-date_as_ARG',
		edge{ label => label[object,preparg,comp,xcomp],
		      target => node{ lemma => Lemma, deriv => Derivs }},
		-1000 ) :-
	( domain(Lemma,date[])
	xor 
	domain(D,Derivs),
	  recorded(deriv(D,EId,_,OId,_)),
	  check_op_top_feature(OId,time,time[])
	)
	.

%% Favor subject, except for gerundive and particiales
edge_cost_elem( '+SUBJ', edge{ id => EId, label => label[subject], deriv => Derivs }, 1100 ) :-
	\+ ( domain(D,Derivs),
	     recorded(deriv(D,EId,_,OId,_)),
	     check_op_top_feature(OId,mode,gerundive),
%%	     format('found gerundive ~w\n',[Op]),
	     true
	   )
	.

%% Favor qui as subject when possible
edge_cost_elem( '+qui_as_subj',
		edge{ label => subject, target => node{ form => qui, cat => cat[pri,prel] }},
		200
	      ).

%% Penalize subject for gerundive and particiales
edge_cost_elem( '-SUBJ', edge{ id => EId, label => label[subject], deriv => Derivs }, -300 ) :-
	domain(D,Derivs),
	recorded(deriv(D,EId,_,OId,_)),
	check_op_top_feature(OId,mode,gerundive)
	.


%% Penalize sentence subject
edge_cost_elem( '-SasSubj',
		edge{ id => EId,
		      label => label[subject],
		      target => node{ cat => v }
		    },
		-600 ).
		
:-std_prolog check_op_top_feature/3.

check_op_top_feature(OId,F,V) :-
	op{ id  => OId, top => fs(L) },
	domain(F:Vals,L),
	domain(val(V),Vals)
	.

%% Favor xcomp over alternate construction with adjoining
edge_cost_elem( '+ARG_XCOMP',
		edge{ label => label[xcomp],
		      source => V1::node{ cat => v },
		      target => V2::node{ cat => v}
		    }, 800 )
%:-
%	edge{ source => V2, target => V1, type => adj, label  => 'S' }
	.

%% Favor cleft constructions
edge_cost_elem( '+CLEFT', edge{ label => 'CleftQue' }, 800 ).

%% Penalty for long distance dependencies, except for root node
edge_cost_elem( '-LONG',
		Edge::edge{ source => node{ id => IdA, cluster => cluster{ left => LA, right => RA }},
			    target => node{ cluster => cluster{ left => LB, right => RB }},
			    type => edge_kind[~virtual]
			  },
		D
	      ) :-
	verbose('Try length penalty on ~E\n',[Edge]),
	( RA =< LB -> 
	    D1 is  (LB - RA)
	;   
	    D1 is (LA - RB)
	),
	D1 > 3,
	D2 is max(D1-3,0),
	D3 is max(D1-6,0),
	D4 is max(D1-9,0),
%%	D is -1 * (1 << D2)
	D is ( -10 * D2 ) + ( -100 * D3 ) + ( - 200 * D4)
	.

%% Favour adj attribute rather than noun
edge_cost_elem( '+ATTR', edge{ label => comp, target => node{ cat => adj } }, 100 ).

%% Favour det rather than adj or noun or verb
%% edge_cost_elem( edge{ target => node{ cat => det } }, 40 ).


%% Favour noun over verb in first position, overcoming penalty for virtual edges
%% SHOULD ADD SOMETHING SIMILAR FOR 'start'
edge_cost_elem( '+NOUN/VERB',
		E::edge{ target => node{ cat => cat[nc,np],
					 cluster => C::cluster{ left => 0 } } }, 1700 ) :-
%%	format('Try Activation ~w\n',[E]),
	edge{ target => node{ cat => v, cluster => C } },
%%	format('Activation\n',[]),
	true
	.

%% Penalty for raw pronon 'ce'
edge_cost_elem( '-CE',
		edge{ target => N::node{ cluster => cluster{ lex => Lex }, cat => pro } },
		-50
	      ) :-
	\+ edge{ source => N },
	label2lex(Lex,[Ce],_),
	domain(Ce,[ce,'Ce'])
	.

%% Favour coordinations
edge_cost_elem( '+COORD', edge{ label => label[coo,coo2,coord2,coord3] }, 20).

%% Penaltie on transcategorization from adj to nc
edge_cost_elem( '-ADJ', edge{ label => 'N2', target => node{ cat => adj } }, -100 ).

%% Favour det preceding numbers
edge_cost_elem( '+NUM.DET', edge{ source => node{ cat => number }, target => node{ cat => det } }, 20).

%% Favour aux-v rather rather than v-acomp
edge_cost_elem( '+AUX-V', edge{ source => node{ cat => v }, target => node{ cat => aux } }, 50 ).

%% Penalty on person constructions (vs e.g participiales): Jean, viens manger !
edge_cost_elem( '-PERS', edge{ target => node{ cat => 'S', tree => Tree } }, -20 ) :-
	domain(person_on_s,Tree)
	.

%% favour genitive over locative
edge_cost_elem( '+CLG', edge{ target => node{ cat => clg }, label => clg }, 50 ).

%% Favour auxiliary over verbs
edge_cost_elem( '+AUX/V', edge{ target => node{ cat => aux }}, 2000 ).

%% Favour 'est' as verb
%% edge_cost_elem( edge{ target => node{ cluster => cluster{ }}, 1000 ).

%% Penalties on filler in robust parsing
edge_cost_elem( '-UNK', edge{ target => node{ cat => unknown } }, -1000).

%% Penalties on virtual edges
edge_cost_elem( '-VIRTUAL', edge{ type => virtual }, -1000).

%% But less penalties on virtual edges leading to verbs
edge_cost_elem( '+VIR->V', edge{ type => virtual, target => node{ cat => v } }, 500 ).

%% Penalties on virtual edges leading to empty short sentence
edge_cost_elem( '-SHORTVIRT', edge{ type => virtual,
		      target => node{ cat => 'S',
				      cluster => cluster{ lex => '' }}
		    },
		-500).

%% Favour closed categories interpretations over others
edge_cost_elem( '+CLOSED',
		edge{ target => node{ lemma => L,
				      cat => Cat::cat[csu,prep,det,pri,pro,prel,conj,coo,cln,cla,clr,clg,cll,cld],
				      form => F,
				      cluster => C } }, W ) :-
	L \== unknown[],
	( Cat = det -> W = 1700
	; Cat = cat[cln,cla,clr,clg,cll,cld], (\+ domain(F,[en])) -> W = 1500
	; W = 600
	)
	.
%%	edge{ target => node{ cat => cat[nc,np,adj,adv,v], cluster => C } }.


%% Strongly penalize det as noun or adj

%% Strongly penalize que or qu' as pri after verb
edge_cost_elem( '-queAsObject',
		edge{ target => node{ lemma => Lemma,
				      cat => pri,
				      cluster => cluster{ left => L} },
		      source => node{ cluster => cluster{ right => R }},
		      type => subst,
		      label => object
		    },
		-10000
	      ) :-
	      R =< L,
	      domain(Lemma,['queComp?','que?'])
	      .

edge_cost_elem( '-queAsComp',
		edge{ source => N::node{ cat => comp},
		      target =>  node{ lemma => Lemma,
				       cat => pri,
				       cluster => cluster{ left => L} },
		      type => subst,
		      label => 'N2'
		    },
		-20000
	      ) :-
	edge{ target => N,
	      source => node{ cluster => cluster{ right => R }},
	      label => comp,
	      type => subst
	    },
	R =< L,
	domain(Lemma,['queComp?','que?'])
	.


%% Penalties on _uw and _Uw words
edge_cost_elem( '-UW1',
		edge{ target => node{ lemma => 'uw' } },
		-1100
	      )
	      .

edge_cost_elem( '+UWasNc',
		edge{ target => node{ cat => nc, lemma => 'uw' } },
		100
	      )
	      .

edge_cost_elem( '+UWasAdjorAdv',
		edge{ target => node{ cat => cat[adj,adv], lemma => 'uw' } },
		50
	      )
	      .

edge_cost_elem( '-UW2',
		edge{ target => node{ lemma => '_Uw' } },
		-1000
	      )
	      .

%% Penalize unknown words as verbs at beginning of sentence
edge_cost_elem( '-uw_as_v_on_start',
		edge{ target => node{ lemma => 'uw',
				      cat => cat[v,aux],
				      cluster => cluster{ left => 0 }
				    } },
		-1000 ).


%% Penalize capitalization when there is a non-capitalized word
edge_cost_elem( '-Capitalize',
		edge{ target => node{ cat => np, lemma => L, cluster => C } },
		-600
	      ) :-
	L \== unknown[],
	node{ cat => cat[nc,adj], cluster => C }
	.

%% Penalties on sequence of Nc
edge_cost_elem( '-NcSeq',
		Edge::edge{ source => node{ cat => nc },
		      target => node{ cat => nc, lemma => Lemma },
		      label => 'Nc2' },
		-1000 ) :-
	Lemma \== '_NUMBER',
	verbose('I am here ~E\n',[Edge])
	.


%% Penalties on use of comma as sentence separator
edge_cost_elem( '-COMASEP',
		Edge::edge{ type => adj, target => node{ cat => 'S', tree => Tree }}, - 2000) :-
	domain(comma_sep,Tree)
	.

%% Penalties on use of comma in enumerations
%% depends on presence of ending ... and number of ','
edge_cost_elem( '-COMAENUM',
		Edge::edge{ type => adj, target => N::node{ tree => Tree }},
		K
	      ) :-
	domain('N2_enum',Tree),
	mutable(M,300),
	every(( edge{ source => N,
		      type => lexical,
		      target => node{ cluster => cluster{ lex => Lex }}},
		label2lex(Lex,[X],_),
		mutable_read(M,_K),
		( X == (',') ->
		    New_K is 2*_K
		;   X == '...' ->
		    New_K is 3*_K
		;
		    New_K is _K
		),
		mutable(M,New_K)
	      )),
	mutable_read(M,_K)
	.

%% Favour adj->adv edges
edge_cost_elem( '+ADVMODADJ',
		edge{ source => node{ cat => cat[adj] },
		      target => node{ cat => adv },
		      type => adj
		    },
		100 ).

%% Favour adv->adv edges
edge_cost_elem( '+ADVMODADV',
		edge{ source => node{ cat => cat[adv,advneg] },
		      target => node{ cat => adv },
		      type => adj
		    },
		150 ).


%% Penalize mutiple adv edges on adj
edge_cost_elem( '-multipleAdvOnAdj',
		edge{ source => N::node{ cat => cat[adj] },
		      target => node{ cat => adv, cluster => cluster{ right => R }},
		      type => adj
		    },
		-1000
	      ) :-
	edge{ source => N,
	      target => node{ cat => adv, cluster => cluster{ left => L } },
	      type => adj
	    },
	R =< L
	.
	

%% Penalties on edge coming from empty cluster
edge_cost_elem( '-EMPTY',
		Edge::edge{ source => node{ cluster => cluster{ lex => '' }}}, -100) :-
	verbose('Empty cluster penalty ~E\n',[Edge])
	.


%% Favour long clusters, especially for closed cats
edge_cost_elem( '+LONGLEX',
		Edge::edge{ target => node{ cat => Cat,
					    lemma => Lemma,
					    cluster => cluster{ lex => Lex,
								left => Left,
								right => Right }}},
		K
	      ) :-
	Lex \== '',
	Right > Left + 1,
	\+ ( Cat = np,
	     node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	     LX >= Left,
	     RX =< Right,
%%	     ( LX \== Left xor RX \== Right),
	     \+ ( node{ cat => np,
			form=> Lemma,
			cluster => cluster{ left => LX, right => RX } },
		  Lemma \== '_Uw'
		),
	     true
	   ),
	( Cat = cat[det,pri,prel,prep,adv,advneg,prep,csu] -> Factor = 500
	; Factor = 100
	),
	K is (Right - Left - 1) * Factor
	.

%% Favour long clusters case 2
edge_cost_elem( '+LONGLEX2',
		Edge::edge{ target => node{ cat => Cat,
					    cluster => cluster{ lex => Lex,
								left => Left,
								right => Right
							      }}},
		K
	      ) :-
	Lex \== '',
	label2lex(Lex,L::[_,_|_],_),
	\+ ( Cat = np,
	     node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	     LX >= Left,
	     RX =< Right,
	     \+ node{ cat => np, cluster => cluster{ left => LX, right => RX } }
	   ),
	length(L,N),
	( Cat = cat[det,pri,prel,prep,adv,advneg,prep,csu] -> Factor = 500
	; Factor = 100
	),
	K is (N - 1) * Factor,
	K > 0
	.


%% Penalize long lex when NP and reading with some non np components
edge_cost_elem( '-NP_as_LONGLEX',
		Edge::edge{ target => node{ cat => np,
					    cluster => cluster{ lex => Lex, left => Left, right => Right }}},
		-20 ) :-
	Right > Left + 1,
	node{ cat => X::cat[adj,nc,prep,det], cluster => cluster{ left => LX, right => RX } },
	LX >= Left,
	RX =< Right,
	\+ ( node{ cat => np, lemma => Lemma, cluster => cluster{ left => LX, right => RX } },
	     Lemma \== '_Uw'
	   ),
	true
	.

%% Favour prep->X edges
edge_cost_elem( '+PREP->X',
		Edge::edge{ source => node{ cat => prep }, type => edge_kind[subst,lexical] }, 400).

%% Favour prep->nc|np edges
edge_cost_elem( '+PREP->n',
		Edge::edge{ source => node{ cat => prep },
			    type => edge_kind[subst,lexical],
			    target => node{ cat => cat[nc,np] }
			  },
		200).

%% Favour lexical v->prep edges
edge_cost_elem( '+V->PREP',
		Edge::edge{ source => node{ cat => v },
			    target => node{ cat => prep },
			    type => edge_kind[lexical] },
		800
	      ).

%% Favour v->VMOd->prep edges for participials
edge_cost_elem( '+V->VMOD->PREP',
		Edge::edge{ source => V::node{ cat => v },
			    target => VMod::node{ cat => 'VMod' },
			    type => adj
			  },
		400
	      ) :-
	edge{ source => VMod,
	      target => node{ cat => prep },
	      type => subst,
	      label => 'PP'
	    },
	edge{ source => node{ cat => 'N2' },
	      target => V,
	      type => subst,
	      label => 'SubS'
	    }
	.

%% Penalty on v->prep[de,d',des] edges going to verbs
edge_cost_elem( '-V->de',
		Edge::edge{ source => node{ cat => cat['S','VMod','N2'] },
			    target => N::node{ cat => prep,
					       lemma => 'de'
					     }
			  },
		- 100 ) :-
	\+ edge{ source => N, target => node{ cat => v } }
	.

%% Favor [adj,nc,np]->de edges instead of 'de X' as GN
edge_cost_elem( '+->de',
		Edge::edge{ source => node{ cat => cat[nc,np,adj] },
			    target => node{ cat => prep, lemma => 'de' } },
		+ 1000
	      ).


%% Penalty based on rank
edge_cost_elem( '-RANK',
		Edge::edge{}, D ) :-
	edge_rank(Edge,Rank,_),
	D is -5 * Rank.


%% Favor 'est' as verb or aux
edge_cost_elem( '+est/v_or_aux',
		edge{ target => node{ cat => cat[v,aux],
				      form => 'est'
				      }
		    },
		1000 )
	.

%% Use of restriction database: PP attachment on verbs
edge_cost_elem( '+RESTR_PP_V',
		edge{ source => node{ lemma => Source, cat => v },
		      target => VMod::node{ cat => 'VMod' },
		      label => vmod,
		      type => adj
		    },
		300) :-
	recorded(opt(restrictions(DB,_))),
	edge{ source => VMod,
	      target => N::node{ cat => prep, lemma => Prep }
	    },
	edge{ source => N,
	      type => subst,
	      target => node{ lemma => Target } },
	check_restriction(Source,Target,Prep)
	.

%% Favor 'en' for gerundives (participiale)
edge_cost_elem( '+en_as_GERUND',
		edge{ source => N::node{},
		      target => N2::node{ lemma => 'en', form => 'en'},
		      type => lexical,
		      deriv => Derivs
		    },
		2000		% need to compensate penalty for empty source
	      ) :-
	edge{ source => N,
	      target => node{ cat => v },
	      type => subst,
	      deriv => Derivs2,
	      id =>EId
	    },

	domain(D,Derivs),
	domain(D,Derivs2),
	recorded(deriv(D,EId,_,_,OId)),
	verbose('Try gerundive ~w\n',[OId]),
	check_op_top_feature(OId,mode,gerundive),
	verbose('Found gerundive ~E\n',[N2]),
	true
	.

%% Use of restriction database: PP attachment on nouns
edge_cost_elem( '+RESTR_PP_N',
		edge{ source => node{ lemma => Source, cat => cat[nc,adj] },
		      target => N::node{ cat => prep, lemma => Prep },
		      label => 'N',
		      type => adj
		    },
		300) :-
	recorded(opt(restrictions(DB,_))),
	edge{ source => N,
	      type => subst,
	      target => node{ lemma => Target }
	    },
	check_restriction(Source,Target,Prep)
	.

%% Penalize PP attachments on adjP
edge_cost_elem( '-PP_ON_ADJP',
		edge{ source => node{ cat => adj },
		      target => node{ cat => prep },
		      type => adj,
		      label => 'adjP'
		    },
		-30
	      )
.

%% Penalize PP built upon an adj
edge_cost_elem( '-PP_UPON_ADJ' ,
		edge{ source => node{ cat =>  prep },
		      target => node{ cat =>  adj }
		    },
		-500
	      ).

%% Strongly penalize nc/adj/... over det (for words such as un, le, la, une , ...)
edge_cost_elem( '-N2_VS_DET',
		edge{ target => node{ cat => cat[nc,pro,cla,adj], cluster => C } },
		-2000
	      ) :-
	edge{ target => node{ cat => cat[det], cluster => C } }
	.

%% Strongly penalize adv/... over prep (for words such as avec, ...)
edge_cost_elem( '-ADV_VS_PREP',
		edge{ target => node{ cat => cat[adv], cluster => C } },
		-3000
	      ) :-
	edge{ target => node{ cat => cat[prep], cluster => C }}
	.

%% Strongly penalize adv/... over csu (for words such as si, ...)
edge_cost_elem( '-ADV_VS_CSU',
		edge{ target => node{ cat => cat[adv], cluster => C } },
		-500
	      ) :-
	edge{ target => node{ cat => cat[csu], cluster => C }}
	.


%% Favor adv on v after rather than on preceding aux,v when in the middle (but for negation)
edge_cost_elem( '-ADV_ON_PREC_V',
		edge{ source => node{ cat => v,
				      cluster => cluster{ right => R1 }
				    },
		      target => node{ cat => adv,
				      cluster => cluster{ left => L, right => R }
				    }
		    },
		+200 ) :-
	R =< R1
	.


%% But favor advneg over adv (and also over comp) when possible
edge_cost_elem( '+AdvNeg',
		edge{ target => node{ cat => advneg } },
		+2000
	      )
.



%% Penalize inverted subjects (especially for robust mode)
edge_cost_elem( '-INVERTED_SUBJ',
		edge{ label => subject,
		      source => node{ cluster => cluster{ right => R } },
		      target => node{ cluster => cluster{ left => L } }
		    },
		-2000
	      ) :-
	R =< L
	.


%% Strongly penalize que? in N2
edge_cost_elem( '-que?inN2',
		edge{ target => node{ lemma => 'que?', xcat => 'N2' } },
		-10000
	      ).

%% Penalize wh in N2 when in concurrence whith non subject wh-pronoun
edge_cost_elem( '-whinN2Subj',
		edge{ label=> subject,
		      target => node{ cat => pri, xcat => 'N2' } },
		-2000
	      ).

%% favor adjective reading for attributes over noun reading
edge_cost_elem( '+adjAsComp',
		edge{ label => comp,
		      target => node{ cat => adj },
		      type => subst
		    },
		400
	      ).

%% favor Adj on Nouns rather than comp
edge_cost_elem( '+adjOnNoun',
		edge{ source => node{ cat => nominal[] },
		      target => node{ cat => adj, cluster => cluster{left => L, right => R}},
		      type => adj,
		      label => 'N2'
		    },
		800
	      ).

/*
	edge{ label => comp,
	      target => node{ cat => adj,
			      cluster => cluster{left => L, right => R} },
	      type => subst
	    }.
*/

%% Use preferences
edge_cost_elem( '+CatPref',
		edge{ target => node{ form => F,
				      cat => C,
				      lemma => L
				    }},
		W
	      ) :- catpref(F,L,C,W).


%% Favor dependencies whose source and targets are in the same EASy F
edge_cost_elem( '+Dep_In_Same_F',
		edge{ source => node{ cluster => cluster{ lex => Lex }},
		      target => node{ cluster => cluster{ lex => Lex }}
		    },
		1000
	      ) :-
	label2lex(Lex,_,[_|_])
	.
	
:-light_tabular prepare_restriction/1.

prepare_restriction(PStmt) :-
	recorded( opt(restrictions(DB,_)) ),
	sqlite!prepare(DB,'select tag from restrictions where source=? and target=? and dir=? and rel=?',PStmt)
	.

:-std_prolog check_restriction/3.

check_restriction(Source,Target,Rel) :-
	prepare_restriction(PStmt),
	(   Dir = up, L = [Source,Target,Dir,Rel]
	;   Dir = down, L = [Target,Source,Dir,Rel]
	),
	sqlite!reset(PStmt),
%%	format('Bind ~w\n',[L]),
	sqlite!bind(PStmt,L),
	(   sqlite!tuple(PStmt,[Tag]) xor fail),
	verbose('Restriction found for source=~w target=~w rel=~w dir=~w => ~w\n',[Source,Target,Rel,Dir,Tag])
	.
	
:-rec_prolog node_cost_regional/5.

/*
%% strong penalty for discontiguous parses associated to removed edges
node_cost_regional(NId,All_Edges,CIds,EIds,-1000) :-
	\+ contiguous(CIds),
	verbose('NOT CONTIGUOUS ~w\n',[CIds]),
	true
	.
*/

/*
node_cost_regional(NId,All_Edges,CIds,EIds,-100) :-
	domain(EId1,All_Edges),
	\+ domain(EId1,EIds),
	edge{ id => EId1,
	      target => node{ cluster => cluster{ left => CId1_Left,
						  right => CId1_Right,
						  lex => CId1_Lex }}
	    },

	CId1_Lex \== '',
	(   domain( CLeft, CIds ),
	    cluster{ id => CLeft, left => CLeft_Left },
	    CLeft_Left < CId1_Left,
	    
	    domain( CRight, CIds ),
	    cluster{ id => CRight, right => CRight_Right },
	    CRight_Right > CId1_Right
	->
%%	    verbose('DISCONTIGUOUS SEGMENT ~w [~w]: ~w\n',[NId,EId1:NId1,CIds]),
	    true
	;   
	    fail
	)
	.
*/

/* No need when using derivations
%% Error for loosing a det
node_cost_regional(NId,All_Edges,CIds,EIds,-1000) :-
%%	verbose('Try regional cost ~w\n',[NId]),
%%	edge{ source => NId, id => EId },
	edge{ source => node{ id => NId, cat => cat[nc,adj,np] },
	      target=> node{ cat => cat[number,det,prep] },
	      type => subst,
	      id => EId
	    },
%%	verbose('Try regional cost det ~w\n',[NId]),
	\+ domain(EId,EIds),
%%	verbose('Found lost det: ~w => ~w ~w\n',[NId,CIds,EIds]),
	true
	.
*/

/* No need with derivations
%% Strong penalty for verb with two same kind of args (except for inverted clitic subject)
node_cost_regional(NId,All_Edges,CIds,EIds,-2000) :-
	edge{ id => EId1,
	      source => node{ id => NId, cat => v },
	      label => L::label[subject,object,comp,preparg],
	      target => N1::node{ cat => Cat1 }
	    },
	edge{ id => EId2,
	      source => node{ id => NId, cat => v },
	      label => L,
	      target => N2::node{ cat => Cat2 }
	    },
	EId1 \== EId2,
	domain(EId1,EIds),
	domain(EId2,EIds),
	( L == subject ->
	    \+ domain(cln,[Cat1,Cat2])
	;   L == preparg ->
	    edge{ source => N1, target => XN1::node{ cat => XCat1 }, type => subst },
	    edge{ source => N2, target => XN2::node{ cat => XCat2 }, type => subst },
	    (	XCat1 = XCat2, XCat1 = cat[v,adj] )
	;   
	    true
	)
	.
*/

%% Strong penalty for verbs with subject after cod (when cod after verb)
node_cost_regional(NId,All_Edges,CIds,EIds,-5000) :-
	E1::edge{ id => EId1,
	      source => N::node{ id => NId, cat => v, cluster => cluster{ right => Pos} },
	      target => node{ cluster => cluster{ left => Left } },
	      label => subject
	    },
	E2::edge{ id => EId2,
		  source => N::node{ id => NId, cat => v },
		  target => node{ cluster => cluster{ right => Right } },
		  label => label[object,xcomp]
		},
	domain(EId1,EIds),
	domain(EId2,EIds),
	Pos =< Right,
	Right =< Left,
	verbose('Regional penalty cod before subj ~w ~w\n',[E1,E2]),
	true
	.

node_cost_regional(root,All_Edges,CIds,EIds,-50000) :-
	\+ recorded( mode(robust) ),
	verbose('Try Regional penalty on incomplete root node ~w\n',[CIds]),
	_C::cluster{ id => _CId, lex => _Lex },
	_Lex \== '',
	\+ ( domain(_CId,CIds)
	   xor
	   domain(__CId,CIds),
	     cluster_overlap(_C,__C::cluster{ id => __CId }),
	     verbose('Overlap ~E ~E\n',[_C,__C])
	   ),
	verbose('Pb with ~E\n',[_C]),
	verbose('Found Regional penalty on incomplete root node ~w\n',[CIds]),
	true
	.

node_cost_regional(NId,All_Edges,CIds,EIds,WFreq) :-
	opt( weights ),
	node{ id =>NId, w => Ws },
	domain(lemmaFreq:W,Ws),
	WFreq is W*10,
	true
	.

node_cost_regional(NId,All_Edges,CIds,EIds,WTag) :-
	opt( weights ),
	node{ id =>NId, w => Ws },
	domain(matchTagger:1,Ws),
	WTag is 1000,
	true
	.

%% Favor coordination with same type of coords
node_cost_regional(NId,All_Edges,CIds,EIds,200) :-
	edge{ source => node{ cat => Cat },
	      target => COO::node{ id => NId, cat => coo }
	    },
	\+ ( domain(EId,EIds),
	     edge{ source => COO,
		   id => EId,
		   label => label[coord2,coord3],
		   target => node{ cat => _Cat }
		 },
	     _Cat \== Cat
	   )
	.


%% Strong penalties on sequence of relatives on same N2
node_code_regional(NId,All_Edges,CIds,EIds,-10000) :-
	edge{ source => N::node{ id => NId, cat => 'N2' },
	      target => N1,
	      type => adj,
	      id =>EId1
	    },
	domain(EId1,EIds),
	edge{ source => N1,
	      type => subst,
	      label => 'SRel'
	    },
	edge{ source => N::node{ id => NId, cat => 'N2' },
	      target => N2,
	      type => adj,
	      id =>EId2
	    },
	domain(EId2,EIds),
	EId1 \== EId2,
	edge{ source => N2,
	      type => subst,
	      label => 'SRel'
	    }
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Expanding coordinations

:-light_tabular coord_expand/0.

coord_expand :-
%%	format('Start coord expand\n',[]),
	every((
	       edge{ source => N1::node{ cluster => cluster{ left => LN1, right => RN1 }},
		     target => N2::node{ cat => coo,
					 cluster => cluster{ left => LN2, right => RN2 }
				       },
		     type => adj,
		     deriv => Derivs1
		   },
	       (
		%% incoming edges on first coordonate
		%% are rerooted to all coordinates
		E3::edge{ source => N3,
			  target => N1,
			  label => L3,
			  type => T3,
			  deriv => Derivs3
			},
%%		record( toerase(E3) ),
		%% Reroot entering dependencies to N2 (new EASy rules)
		\+ edge{ source => N3, target => N2, type => T3, label => L3 },
		gensym(EId),
		%%	       format('EXPAND ~w => ~w ~w\n',[EId,N3,N4] ),
		record_without_doublon( extra_edge(
						   edge{ id => EId,
							 source => N3,
							 target => N2,
							 type => T3,
							 label => L3,
							 deriv => Derivs3
						       },
						   E3
						  )
				      ),
		%%	       format('COORD ADDED ~E\n',[E]),
		true
	       ;
		%% All outgoing edge from first coordinate
		%% are duplicated on all other coordinates
		%% if before first coordinate and after last one
		%% Subst edges are not duplicated if equivalent already present
		E4::edge{ source => N1,
			  target => N4::node{ cluster => cluster{ left => LN4, right => RN4 }},
			  label => L4,
			  type => T4,
			  deriv => Derivs4
			},
		(RN4 =< LN1 xor RN2 < LN4),
		(T4 = adj
		xor
		L4 = label[subject,object,arg,preparg,scomp,comp,comp],
		\+ ( edge{ source => N2,
			   target => _N1,
			   type => subst
			 },
		     edge{ source => _N1,
			   type => T4,
			   label => L4
			 }
		   )
		),
%%		record( toerase(E4) ),
		\+ edge{ source => N2, target => N4, type => T4, label => L4 },
		gensym(EId),
		%% format('EXPAND ~w => ~w ~w\n',[EId,N3,N4] ),
		record_without_doublon( extra_edge(
						   edge{ id => EId,
							 source => N2,
							 target => N4,
							 type => T4,
							 label => L4,
							 deriv => Derivs4
						       },
						   E4
						  )
				      ),
		%%	       format('COORD ADDED ~E\n',[E]),
		true
	       )
	      )),
%%	every(( recorded(toerase(_E)),
%%		erase(_E) )),
%%	format('End coord expand\n',[]),
	true
	.

coord_expand :-
%%	format('Start coord expand\n',[]),
	every((
	       edge{ source => N1,
		     target => N2::node{ tree => Tree },
		     type => adj
		   },
	       domain('N2_enum',Tree),
	       %%	       format('Found Coord ~w from ~w\n',[N2,N1]),
	       edge{ source => N3,
		     target => N1,
		     label => L3,
		     type => T3 },
	       domain(L,[coord]),
	       edge{ source => N2,
		     target => N4,
		     label => L,
		     type => subst },
	       \+ edge{ source => N3, target => N4, type => T3, label => L3 },
	       gensym(EId),
	       %%	       format('EXPAND ~w => ~w ~w\n',[EId,N3,N4] ),
	       record_without_doublon( E::edge{ id => EId,
						source => N3,
						target => N4,
						type => T3,
						label => L3 } ),
%%	       format('COORD ADDED ~E\n',[E]),
	       true
	      )),
%%	format('End coord expand\n',[]),
	true
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building words

:-std_prolog word/1.

word(_Left) :-
	verbose( 'Word at left=~w\n',[_Left]),
	%% find shortest (non empty non redirected) cluster closest from _Left
	(   C::cluster{ id=> Id, left => Left, right => Right, lex => Lex, token => Token },
	    _Left =< Left,
	    verbose('Found potential cluster ~E after ~w\n',[C,_Left]),
	    Lex \== '',
	    \+ ( cluster{ id => _Id, left => __Left, right => __Right, lex => __Lex },
		   \+ recorded( redirect(_Id,_) ),
		   __Lex \== '',
		   (   (__Left == Left, __Right < Right)
		   xor ( _Left =< __Left, __Left < Left)
		   )
	       ),
	    verbose('Found cluster ~E after ~w\n',[C,_Left]),
	    \+ recorded( redirect(Id,_) )
	->
	    every((
		   C2::cluster{ id => Id2, left => Right, right => Right2, lex => Lex, token => Token2 },
		   agglutinate(Token,Token2,AggLex),
		   verbose('Register redirected ~w -> ~w [~w]\n',[Id2,Id,AggLex]),
		   record_without_doublon( redirect(Id2,Id))
		  )),
	    rx!tokenize(Lex,' ',Words),
	    word_aux(Words,Left,C)
	;   
	    true
	)
	.

:-rec_prolog word_aux/3.

word_aux([],Left,Cluster::cluster{ left => Left , right => Right }) :-
	verbose('Restart word ~w\n',[Left]),
	word(Right)
	.

word_aux([Lex|Words],Left,Cluster::cluster{ id => CId, token => Token }) :-
	verbose( 'Word ~w in cluster ~w at left=~w\n',[Lex,CId,Left]),
	sentence(Sent),
	(   rx!tokenize(Lex,'|',[Label,_Lex1|_R]),
	    Label \== ''
	->  
	    %% Pb when second '|' arise after first '|'
	    ( _R = [] -> Lex1 = _Lex1 ; Lex1 = Lex ),
%%	    name_builder('E~w~w',[Sent,Label1],Label),
	    Label = Label1,
	    ( fail, Lex1 == '*' ->
	      recorded(f{id=> Label, cid => CId0}),
	      record_without_doublon( redirect(CId,CId0) )
	    ;	recorded(f{id=> Label, cid => CId0}) ->
	      verbose('Redirect f label=~w lex=~w cluster=~E cid0=~w\n',[Label,Lex1,Cluster,CId0]),
	      record_without_doublon( redirect(CId,CId0) )
	    ;
	      update_counter(fids,Rank),
	      verbose('Register f rank=~w label=~w lex=~w cluster=~E\n',[Rank,Label,Lex1,Cluster]),
	      record(f{id=>Label, lex=>Lex1, cid => CId, rank => Rank})
	    )
	; Token = '_SENT_BOUND' ->
	    verbose('Ignore sentence boundary cluster=~E\n',[Cluster]),
	    true
	;
	    Lex1 = Lex,
	    update_counter(fids,Rank),
	    name_builder('E~wF~w',[Sent,Rank],Label),
	    verbose('XRegister f rank=~w label=~w lex=~w cluster=~E\n',[Rank,Label,Lex1,Cluster]),
	    record(f{id=>Label, lex=>Lex1, cid => CId, rank => Rank})
	),
	word_aux(Words,Left,Cluster),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Building constitutants

:-light_tabular constituant/3.

constituant(Left,Right,Const) :-
	'$answers'( Const::group(Type,Left,Right,N,Content) ),
	\+ used(Const)
	.

constituant(Left,Right,Const) :-
	Const::cluster{ id => Label, left => Left, right => Right, lex => Lex },
	Lex \== '',
	\+ used(Label)
	.

:-std_prolog x_command/2.

x_command(N1,N2) :-
	(   edge{ source => N1, target => N2 }
	; edge{ source => N3, target => N1, deriv => Derivs1 },
	    edge{ source => N3, target => N2, deriv => Derivs2 },
	    domain(D,Derivs1),
	    domain(D,Derivs2)
	)
	.

:-light_tabular build_group/1.

build_group(Type) :-
%%	format('Try build group ~w\n',[Type]),
	group(Type,N::node{ cluster => Cluster::cluster{ id => WId }},Nodes),
	verbose('Potential group ~w ~w ~w\n',[Type,N,Nodes]),
	\+ used(WId),
	\+  ( recorded( redirect( WId,WId1) ), % Needed to avoid void group
	      ( used(WId1)
	      xor recorded( redirect( _WId,WId1) ), used(_WId)
	      )
	    ),
	verbose('TEST group ~w ~w ~w\n',[Type,N,Nodes]),
	( Nodes = group(_,_Left1,Right,_,Content1) ->
	  Cluster = cluster{ id => Id, left => Left, right => Left1 },
	  _Cluster = cluster{ id => _Id, right => _Left1 },
	  ( recorded( redirect( _Id, Id) ) xor Id=_Id ),
	  ( domain(WId,Content1) ->
	    Content = Content1
	  ;
%%	    verbose('Try Left cluster filler ~E ~w ~w => ~w\n',[Cluster,_Left1,Content1,Content]),
	    cluster!add_left_fillers(Content1,Content,_Left1,Cluster),
	    verbose('Left cluster filler ~E ~w ~w => ~w\n',[Cluster,_Left1,Content1,Content])
%%	    Content = [WId|Content1]
	  ),
	  mark_as_used(Content),
%%	  mark_as_used([Nodes]),
	  true
	;   
	    node!add(N,Nodes,Nodes2),
	    node!add_fillers(Nodes2,Nodes3),
	    node!group(Nodes3,Content,Left,Right)
	),
	verbose('TEST2 group ~w ~w ~w\n',[Type,N,Nodes]),
	Left \== Right,
	verbose('TEST3 group ~w ~w ~w\n',[Type,N,Nodes]),
	group(Type,Left,Right,N,Content)
	.

:-std_prolog cluster!add_left_fillers/4.

cluster!add_left_fillers(Content,XContent,
			Right,
			LeftCluster::cluster{ id => WId, right => R} ):-
	(R=Right ->
	 XContent = [WId|Content]
	; cluster{ id => _WId, left => _Left, right => Right },
	 _Left < Right,
	 R =< _Left,
%%	 verbose('Try left filler Left=~w newright=~w Right=~w\n',[R,_Left,Right]),
	 cluster!add_left_fillers([_WId|Content],XContent,_Left,LeftCluster)
	)
	.
	

:-light_tabular group/5.
:-mode(group/5,+(+,+,+,+,+)).

group(Type,Left,Right,N,Content) :-
	verbose('GROUP ~w ~w ~w ~E ~w\n',[Type,Left,Right,N,Content]),
	mark_as_used(Content)
	.

:-rec_prolog group/3.

group('NV',N,Nodes) :-
	N::node{ cat => Cat::cat[v,aux], cluster => cluster{ left => LeftN, right => RightN }},
	\+ single_past_participiale(N),
	node!first_v_ancestor(N,P1),
	node!collect( (N1::node{ cat => Cat1,
				 lemma => Lemma1,
				 cluster => cluster{ left=> Left1, right => Right1}}) ^
		    (
			node!terminal(N,cat[v,aux],['Infl','V']),
			node!terminal(P1,cat[v],['Infl','V']),
			%% N is first aux or v
			%% get subject of older ancestor P or from N (post subject clitic)
			node!older_ancestor(N,cat[v,aux],P,['Infl','V']),
			record_without_doublon( terminal_v(P,N) ), %% for the SUJ-V relation
%%			format('OLD ANCESTOR ~w => ~w\n',[N,P]),
			( Cat1 = cln
			;
			  Cat1 = pro,
			  domain( Lemma1, ['�a','ceci','cela','ce'] ),
			  Right1 =< LeftN,
			  \+ ( edge{ source => N1,
				     target => N1_Dep::node{ cluster => cluster{ left => N1_Dep_Left}}
				   },
			       N1_Dep_Left >= Right1
			     ),
			  true
			),
			(   node!dependency(out,P,N1,_)
			;   N \== P, node!dependency(out,N,N1,_)
			)
		    ;	
			node!terminal(N,cat[aux],['Infl','V']),
			%% N is first aux or v in a local chain
			%% (may be preceded by a modal verb)
			%% get all clitics from first v ancestor
			(   Cat1 = cat[clg,cla,cld,cll,clr,clneg,advneg,adv],
			    node!dependency(out,P1,N2::node{ cat => Cat1 },[E])
			;   
			    N \== P1,
			    Cat1 = cat[adv,advneg],
			    node!dependency(out,N,N2,[E])
			),
			%% only keep adv or advneg if left of N and find all dep. from advneg
			%% and only if attached to v (not to S) or some advneg
			%% and preceded by a clneg !
			( Cat1 = cat[adv,advneg] ->
%%			    format('ADV* ~w ~w\n',[N2,E]),
			    E = edge{ label => cat[v,advneg] },
			    edge{ source => N, target => node{ cat => clneg } },
			    node!neighbour(xleft,N,N2),
			    (	N1 = N2 ;
				node!safe_dependency(xout,N2,N1),
%%				format('NV XOUT cat=~w N=~w P1=~w N2=~w -> N1=~w\n',[Cat1,N,P1,N2,N1]),
				true
			    )
			;
			    N1 = N2
			)
		    ),
		      Nodes )
	.

group('GN',N,Nodes) :-
	N::node{ cat => Cat::cat[adj,nc,pro,np,pri,prel,ncpred],
		 lemma => L
	       },
	( Cat = adj ->
	    edge{ source => N, type => subst, label => det }
	;   Cat = cat[pri] ->
	  ( \+ edge{ source => node{ cat => 'S'}, target => N }
	  xor pri(L,'GN')),
	  \+ edge{ target => N, label => 'CleftQue' }
	;   Cat = cat[prel] ->
	  \+ edge{ source => node{ cat => 'S'}, target => N },
	  \+ edge{ target => N, label => 'CleftQue' }
	;
	    true
	),
%%	\+ node!neighbour(right,N,node{cat=>cat[nc,np]}),
	node!collect( N1^(
			  edge{ source => N, target => _N1, label => label[~ ['N2']] },
			  ( N1=_N1 ; node!safe_dependency(xout,_N1,N1) ),
			  %%			   format('GN XOUT ~w -> ~w\n\tL=~L\n',[N,N1,['~E',' '],[]]),
			  node!neighbour(xleft,N,N1)
			 )
		    , _Nodes ),
	left_reduce(_Nodes,Nodes)
	.

%% Remove skip node on the left
:-rec_prolog left_reduce/2.
left_reduce([],[]).
left_reduce([N|L],XL) :-
	( edge{ target => N, label => skip, type => epsilon } ->
	  left_reduce(L,XL)
	;
	  XL=[N|L]
	)
	.

group('GN',N,[]) :-
	N::node{ lemma => '_META_TEXTUAL_GN' }
	.

group('GN',N,[]) :-
	N::node{ lemma => '_NUMBER', cat => adj }
	.

group('GP',N,Nodes) :-
	edge{ source => S::node{ cat => 'S'},
	      target => N::node{ cat => Cat::cat[prel], cluster => cluster{ left => L} }},
	node!collect( N1^( node!safe_dependency(xout,N,N1),
			   node!neighbour(xleft,N,N1) )
		    , __Nodes ),
	( edge{ source => S,
		target => Prep::node{ cat => prep, cluster => cluster{ right => R}},
		label => prep
	      },
	  R =< L ->
	  _Nodes = [Prep|__Nodes]
	;
	  _Nodes = __Nodes
	),
	left_reduce(_Nodes,Nodes)
	.

group('GP',N,Nodes) :-
	edge{ source => S::node{ cat => 'S'},
	      target => N::node{ cat => Cat::cat[pri], lemma => L,
				 cluster => cluster{ left => Left }}},
	verbose('TRY GP ~E ~w\n',[N,L]),
	( edge{ source => S,
		target => Prep::node{ cat => prep, cluster => cluster{ right => Right }},
		label => prep
	      },
	  Right =< Left ->
	  Nodes = [Prep]
	;
	  pri(L,'GP'),
	  Nodes = []
	)
	.

group('GP',N,Group1) :-
	'$answers'(Group1::group(const['GN','GR','GP','GA'],Left1,_,Head1,_)),
	node!neighbour(left,Left1,_N::node{ cat => prep }),
	verbose('TRY GP ~E ~w ~E\n',[_N,Left1,Head1]),
	x_command(_N,Head1),
	easy_compound_prep(_N,N::node{ lemma => Lemma }),
	\+ not_a_prep(Lemma),
	true
	.

group('PV',N,Group1) :-
	'$answers'(Group1::group('NV',Left1,_,Head1,_)),
	node!neighbour(left,Left1,_N),
	recorded( terminal_v(Head2,Head1) ),
	verbose( 'PV: left=~E head1=~E head2=~E\n',[_N,Head1,Head2]),
	(   _N = node{ cat => prep },
	    easy_compound_prep(_N,N)
	;
	    N = _N,
	    _N = node{ %% cat => '_',
		       lemma => en }
	),
	x_command(_N,Head2),
	verbose( 'Found PV: left=~E head1=~E head2=~E\n',[N,Head1,Head2]),
	true
	.

group('PV',N,Group1) :-
	'$answers'(Group1::group('NV',Left1,_,Head1,_)),
	node!neighbour(left,Left1,N),
	recorded( terminal_v(Head2,Head1) ),
	edge{ source => Head2, target => M::node{ cat => v }, type => adj, label => 'S' },
	edge{ source => M, target => N::node{ cat => prep }, type => lexical }
	.

group('GR',N,[]) :-
	N::node{ cat => cat[adv,advneg] }
	.

group('GR',N,[]) :-
	edge{ target => N::node{ cat => cat[csu] },
	      label => advneg,
	      source => node{ cat => cat[v,aux]}
	    }
	.

group('GR',N::node{ cat => pri, lemma => L },[]) :-
	edge{ source => node{ cat => 'S'}, target => N },
	%% by default, pri which are not arguments are GR unless
	%% otherwise explicetely mentionned
	pri(L,const[~ ['GP','GN']])
	.

group('GR',N,[]) :-
	edge{ target => N::node{ cat => que_restr },
	      label => advneg,
	      type => lexical
	    }
	.

group('GA',N,[]) :-
	N::node{ cat => adj, lemma => Lemma },
	Lemma \== '_NUMBER'
	.


group('GA',N,[]) :-
	%% Participiales on nouns, non gerundive (ie. past participle)
	single_past_participiale(N)	
	.

:-light_tabular single_past_participiale/1.

single_past_participiale(N::node{}) :-
	edge{ source => node{ cat => 'N2' },
	      target => N::node{ cat => v, cluster => cluster{ lex => Lex } },
	      type => subst,
	      label => 'SubS'
	    },
	\+ edge{ source => N, label => label[object,arg,preparg,scomp,xcomp] },
	\+ edge{ source => N, target => node{ cat => aux } },
	\+ ( edge{ source => N, target => VMod::node{ cat => 'VMod' },
		   type => adj, label => vmod },
	     edge{ source => VMod, target => node{ cat => prep }, label => 'PP'}
	   ),
%%	format('Try1 participale2adj on ~w ~w\n',[Lex,TLex]),
	(label2lex(Lex,[TLex],_) xor TLex = Lex),
	verbose('Try2 participale2adj on ~w ~w\n',[Lex,TLex]),
	(   rx!match{ string => TLex, rx => rx{ pattern => 'ant$'} } ->
	    verbose('Match succeeded\n',[]),
	    fail
	;
	    verbose('Match failed\n',[]),
	    true
	)
	.

:-light_tabular easy_compound_prep/2.
:-mode(easy_compound_prep/2,+(+,-)).

%% Climb along a compound preposition like "� partir de"
%% when they form a single EASy cluster
easy_compound_prep( N1::node{ cat => prep, cluster => cluster{ id => Id1 }},
		    N2::node{ cat => prep, cluster => cluster{ id => Id2 }}
		  ) :-
	( edge{ source => M::node{ cluster => cluster{ id => IdM}},
		target => N1,
		type => adj
	      },
	  verbose('TRY EASY COMPOUND ~w ===> ~w\n',[N1,M]),
	 edge{ source => N3::node{ cat => prep, cluster => cluster{ id => Id3}},
	       target => M,
	       type => subst
	     },
	  verbose('TRY2 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N3]),
	  easy_compound_prep(N3,N2),
	  verbose('TRY3 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N2]),
	  recorded( redirect(Id1, Id2) ),
	  verbose('TRY4 EASY COMPOUND ~w ===> ~w ~w\n',[N1,M,N2]),
	  true
	xor N1=N2
	),
	verbose('EASY COMPOUND ~w ===> ~w\n',[N1,N2]),
	true
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Extracting dependencies

:-std_prolog extract_relation/1.

extract_relation( R ) :-
	R::relation{ type => Type, arg1 => Arg1, arg2 => Arg2, arg3 => Arg3 },
	coord_reroot(R,XR::relation{ type => Type, arg1 => XArg1, arg2 => XArg2, arg3 => XArg3 }),
	arg2id(XArg1,Id1),
	arg2id(XArg2,Id2),
	arg2id(XArg3,Id3),
	( Id1 \== Id2 ),	% To avoid relations between same tokens
	relation( relation{ type => Type, arg1 => Id1, arg2 => Id2, arg3 => Id3 } )
	.

:-std_prolog arg2id/2.

arg2id(Arg,Id) :-
	( Arg = [] -> Id = []
	;   Arg = node{ cluster => C::cluster{ id => CId , left => Left , right => Right}} ->
	    ( recorded( redirect(CId,_CId) ) ->
		_Left is Left - 1
	    ;	
		_Left = Left,
		_CId = CId
	    ), 
	    (	f{ cid => _CId, id => Id } 
	    xor	cluster_overlap(C,C2::cluster{ id => CId2 }),
		f{ cid => CId2, id => Id }
	    )
	;   Id = Arg
	)
	.


:-std_prolog coord_reroot/2.

coord_reroot( R::relation{ type => Type,
			    arg1 => Arg1,
			    arg2 => Arg2,
			    arg3 => Arg3
			  },
	      XR::relation{ type => Type,
			    arg1 => XArg1,
			    arg2 => XArg2,
			    arg3 => XArg3
			  }
	    ) :-
	( Type = 'COORD' ->
	  R = XR
	; coord_relarg_reroot(Type,1,Arg1,XArg1,R),
	  coord_relarg_reroot(Type,2,Arg2,XArg2,R),
	  coord_relarg_reroot(Type,3,Arg3,XArg3,R)
	).

:-rec_prolog coord_relarg_reroot/5.

coord_relarg_reroot(Type,Pos,Arg,XArg,
		    R::relation{ arg1 => Arg1,
				 arg2 => Arg2
			       }
		   ) :-
	( \+ var(Arg),
	  Arg = node{ cluster => cluster{ left => L1, right => R1 } },
%%	  format('Try Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  ( _Arg = Arg
	  ; edge{ source => _Arg,
		  target => Arg,
		  type => edge_kind[subst,lexical],
		  label => label['N2']
		}
	  ),
	  E::edge{ source => _Arg,
		   target => COO::node{ cat => coo,
					cluster => cluster{ left => CL,
							    right => CR }
				      },
		   type => adj
		 },
	  \+ duplicate_in_coord(E,Type,Pos),
%%	  format('Try2 Coord reroot relarg ~w arg~w from ~E\n',[Type,Pos,Arg]),
	  \+ ( ( Pos=1 -> OtherArg=Arg2
	       ; Pos=2 -> OtherArg=Arg1
	       ; fail
	       ),
	       OtherArg = node{ cluster => cluster{ left => OL,
						    right => OR }},
%%	       format('Other arg is ~E\n',[OtherArg]),
	       R1 =< OL,
	       OR =< CL
	     ),

	  allow_reroot(Type,Pos,Arg,XArg,R)
	->
	  XArg = COO,
%%	  verbose('Coord reroot relarg ~w arg~w from ~E to ~E\n',[Type,Pos,Arg,XArg]),
%%	  format('Coord reroot relarg ~w arg~w from ~E to ~E\n',[Type,Pos,Arg,XArg]),
	  true
	;
	  XArg = Arg
	)
	.

:-rec_prolog allow_reroot/5.

allow_reroot(rel[],_,_,_,_).

:-rec_prolog duplicate_in_coord/3.

%% This predicate is to avoid coord rerooting for cases like
%%   il parle et elle part
%% where the same role is duplicated in each coordinated
duplicate_in_coord( edge{ target => COO::node{} },
		    Type,
		    Pos ):-
	edge{ source => COO,
	      target => N,
	      label => coord3
	    },
	( Pos=1 -> Arg1 = N
	; Pos=2 -> Arg2 = N
	; fail
	),
	relation{ type => Type,
		  arg1 => Arg1,
		  arg2 => Arg2
		}
	.

:-light_tabular relation/1.

relation(relation{ id => Label }) :- rel_gensym(Label).

relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N3, target => N1, label => subject },
	(   recorded( terminal_v(N3,N2) )
	xor Cat1 = cln,
	    N2 = N3,
	    Left1 = Right2
	)
	.


relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ source => N3, target => N1, label => subject },
	edge{ source => N3, target => N4, label => label['S',vmod], type => adj },
	node!empty(N4),
	edge{ source => N4, target => N5::node{ cat => v }, type => subst },
	(   recorded( terminal_v(N5,N2) ) xor N2 = N5 )
	.

%/*
%% A quoi sert la clause suivante ? pour les modaux
relation{ type => 'SUJ-V',
	  arg1 => N1::node{ cat => Cat1, cluster => cluster{ left => Left1 }},
	  arg2 => N2::node{ cluster => cluster{ right => Right2 }}
	} :-
	edge{ source => N2, target => N1, label => subject },
	\+ edge{ source => N2, target => node{ cat => aux }, label => 'Infl', type => adj }
	.
%*/

relation{ type => 'SUJ-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{}
	} :-
	edge{ source => N3, target => N2, label => xcomp },
	\+ edge{ source => N2, label => subject },
	verbose('Trying ctrl verb: ~E ~E\n',[N3,N2]),
	(   edge{ source => N3, target => N1, label => object }
	xor edge{ source => N3, target => N1, label => preparg }
	xor edge{ source => N3, target => N1, label => subject }
	)
	.

relation{ type => 'SUJ-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v }
	} :-
	E::edge{ source => N2,
		 target => N3::node{ cat => 'S', tree => Tree },
		 type => adj },
	domain('person_on_s',Tree),
	edge{ source => N3, target => N1, type => subst, label => 'N2' }
	.

relation{ type => 'AUX-V',
	  arg1 => N1::node{ cat => aux},
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1, label => 'Infl' }
	.

relation{ type => 'COD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1,
	      label => label[object,ncpred,clg],
	      type => edge_kind[subst,lexical]
	    }
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{},
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N1, label => label[scomp,xcomp] },
	\+ edge{ source => N2, label => object },
	\+ edge{ source => N2, label => clr }
	.

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => v},
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3, label => 'preparg' },
	edge{ source => N3, target => N1, label => 'S' },
	\+ edge{ source => N2, label => object }
	.

/*
relation{ type => 'COD-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v}
	} :-
	edge{ source => N2, target => N3::node{ cat => prep, lemma => de}, label => preparg },
	edge{ source => N3, target => N1 },
	\+ edge{ source => N2, label => object }
	.
*/

relation{ type => 'COD-V',
	  arg1 => N1::node{ cat => cat[v,aux] },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => _N1, target => N2, label => 'V' },
	\+ edge{ source => N2, label => object },
	( edge{ source => _N1, target => N1, label => 'Infl', type => adj },
	  N1 = node{ cat => aux }
	xor _N1=N1
	)
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3, lemma => Lemma3 },
	      label => label[preparg]
	    },
	( Cat3 = prep ->
	  /*
	    (	Lemma3 \== de 	% not true for all de-obj verb
	    xor edge{ source => N2, label => object }
	    ),
	  */
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;   
	    N3 = N1
	)
	.

relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat },
	      label => label['S',vmod],
	      type => adj
	    },
	( node!empty(N3) ->
	    edge{ source => N3, target => N4::node{ cat => prep } }
	;   Cat = prep,
	    N3 = N4
	),
	(   edge{ source => N4, target => N1, type => edge_kind[subst,lexical] }
	;
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] },
	    N1 = node{ cat => prel }
	),
	\+ node!empty(N1)
	.


relation{ type => 'CPL-V',	% participiales
	  arg1 => N1::node{ cat => cat[v] },
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{},
	      label => label['S',vmod],
	      type => adj
	    },
	node!empty(N3),
	edge{ source => N3, target => N1, type => subst, label => 'SubS' }
	.


relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2, target => N1::node{ cat => cll }, label => cll }
	.


relation{ type => 'CPL-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','vmod'],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N4::node{ cat => prep},
		  label => label['PP',wh], type => subst
		},
	E3::edge{ source => N4,
		  target => N1,
		  type => edge_kind[subst,lexical]
		},
	verbose('CPL-V ~E ~E ~E\n',[E1,E2,E3]),
	true
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[adv,advneg,nc] },
	  arg2 => N2
	} :-
	edge{ source => N2,
	      target => N1,
	      label => label['V','V1','S','v','S2','vmod',advneg], type => edge_kind[adj,lexical] }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => v }
	} :-
	edge{ source => N2,
	      target => N1,
	      label => advneg,
	      type => lexical
	    }
	.

relation{ type => 'MOD-V',
	  arg1 => N1::node{ cat => cat[pri,prel] },
	  arg2 => N2::node{ cat => v }
	} :-
	E1::edge{ source => N2,
		  target => N3::node{ cat => 'S' },
		  label => label['S','vmod'],
		  type => adj },
	node!empty(N3),
	E2::edge{ source => N3,
		  target => N1,
%%		  label => pri,
		  type => edge_kind[subst,lexical]
		},
	\+ edge{ source => N3,
		 target => node{ cat => prep }
	       }
	.

relation{ type => 'MOD-V',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => cat[csu] },
	      label => label['S','vmod'],
	      type => adj },
	edge{ source => N3, target => N1, label => label['SubS'], type => subst }
	.

relation{ type => 'COMP',
	  arg1 => N1,
	  arg2 => N2
	} :-
	edge{ source => N3::node{ cat => cat[v]},
	      target => N2,
	      label => label[scomp,xcomp],
	      type => subst
	    },
	edge{ source => N3, target => N1, label => label[csu], type => lexical }
	.

%% COMP pour csu sans que: quand il vient, il mange

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => csu },
	  arg2 => N2::node{ cat => cat[v,aux] }
	} :-
	edge{ source => N1,
	      target => _N2,
	      label => label['SubS'],
	      type => subst
	    },
	recorded( terminal_v(_N2,N2) )
	.

%% COMP entre Prep et (GN GA ou NV) quand discontinus

relation{ type => 'COMP',
	  arg1 => N1::node{ cat => prep, cluster => cluster{ right => Right1} },
	  arg2 => N2::node{ cat => cat[nc,cln,pro,np,pri,prel,v,adj],
			    cluster => cluster{ id => CId2, left => Left2 }}
	} :-
	x_command(N1,_N2::node{ cat => cat[nc,cln,pro,np,pri,prel,v,adj] }),
	verbose('Found pre potential COMP ~E ~E\n',[N1,_N2]),
	( _N2 = N2,
	  \+ '$answers'( group(const['GP','PV'],_,_,N1,_) )
	; edge{ source => _N2, target => COO::node{ cat => coo }, type => adj },
	  edge{ source => COO, target => N2, label => label[coord2, coord3] }
	),
	Right1 =< Left2,
	verbose('Found potential COMP ~E ~E\n',[N1,N2]),
	'$answers'( group(const['GN','NV','GA'],_,_,N2,_) ),
	\+ ( '$answers'( group(const['GP','PV'],_,_,N1,Content) ),
	     verbose('Content ~E ~L\n',[N1,['~E ', ' '],Content]),
	     domain(CId2,Content)
	   )
	.

relation{ type => 'ATB-SO',
	  arg1 => N1,
	  arg2 => N2::node{ cat => v},
	  arg3 => SO
	} :-
	edge{ source => N2, target => _N1::node{ cat => _Cat1}, label => comp },
	( _Cat1 = comp ->
	  edge{ source => _N1, target => N1, type => subst }
	;
	  N1 = _N1
	),
	( edge{ source => N2, label => object } -> SO = objet ; SO = sujet )
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => xnominal[], xcat => 'N2' },
	  arg3 => faux
	} :-
	edge{ source => N2, target => N3::node{ cat => Cat3::cat[~ coo] },
%%	      type => edge_kind[adj,lexical]
	      type => edge_kind[adj]
	    },
	( node!empty(N3) ->
	    %% Relatives, csu, participiales, ... 
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;   
	    N3 = N1
	)
	.

relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[adj,adv] },
	  arg2 => N2::node{ cat => ncpred },
	  arg3 => faux
	} :-
	edge{ source => N3::node{ cat => v},
	      target => N1,
	      type => adj,
	      label => ncpred
	    },
	edge{ source => N3,
	      target => N2,
	      type => lexical,
	      label => ncpred
	    }
	.


relation{ type => 'MOD-N',
	  arg1 => N1::node{ cat => cat[det,number], lemma => '_NUMBER' },
	  arg2 => N2::node{ cat => xnominal[] , xcat => 'N2' },
	  arg3 => faux
	} :-
	edge{ source => N2,
	      target => N1,
	      type => subst,
	      label => det
	    }
	.

relation{ type => 'MOD-N',
	  arg1 => N1,
	  arg2 => N1,
	  arg3 => vrai
	} :-
	N1::node{ cat => np, cluster => cluster{ left => Left, right => Right} },
	Left + 1 < Right
	.

relation{ type => 'MOD-N',
	  arg1 => N2::node{ cat => terminal[] },
	  arg2 => N1::node{ cat => nominal[] }
	} :-
	edge{ source => N1, target => N2::node{ cat => np }, label => 'np' },
	\+ edge{ source => N1, label => 'MLex', type => lexical }
	.


relation{ type => 'MOD-A',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adj] , xcat => cat[~ ['N2']] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj
	    },
	( node!empty(N3) ->
	    %% not sure it may arise for adjectives !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{ type => 'MOD-R',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[adv,advneg] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj
	    },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for adverbs !
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{ type => 'MOD-P',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => cat[prep] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => Cat3::cat[~coo] },
	      type => adj },
%%	format('HERE ~w ~w\n',[N2,N3]),
	( node!empty(N3) ->
	    %% not sure it may arise for prepositions !
	    edge{ source => N3, target => N1, type => subst },
	    N1 = node{ cat => v }
	;   Cat3 = prep ->
	    %% not sure it may arise for prepositions !
	    edge{ source => N3, target => N1, type => edge_kind[subst,lexical] }
	;
	    N3 = N1
	)
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => vide,
	  arg3 => N3::node{}
	} :-
	edge{ source => Start::node{ cat => 'S' },
	      target => Coord::node{ cat => cat[coo] },
	      type => lexical,
	      label => starter
	    },
	edge{ source => N3,
	      target => Start,
	      type => adj
	    }
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => N2::node{},
	  arg3 => N3::node{}
	} :-
	edge{ source => Start::node{},
	      target => LastCoord::node{ cat => cat[coo] },
	      type => adj
	    },
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	( node!empty(_N2) ->
	  edge{ source => _N2,
		target => N2,
		type => edge_kind[subst,lexical]
	      },
	  N2=node{ cat => cat[v,prep,nc,np] }
	;   
	    N2 = _N2
	),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	),
	true
	.

relation{ type => 'COORD',
	  arg1 => Coord::node{},
	  arg2 => N2::node{},
	  arg3 => N3::node{}
	} :-
	edge{ source => Start::node{},
	      target => Enum::node{ tree => Tree },
	      type => adj
	    },
	domain('N2_enum',Tree),
	LastCoord = Enum,
	(   _N2 = Start
	;   edge{ source => LastCoord, target => _N2, label => coord2 }
	),
	coord_next(LastCoord,_N2,Coord),
	coord_next(LastCoord,Coord,_N3),
	( node!empty(_N2) ->
	  edge{ source => _N2,
		target => N2,
		type => edge_kind[subst,lexical]
	      },
	  N2=node{ cat => cat[v,prep,nc,np] }
	;   
	    N2 = _N2
	),
	( node!empty(_N3) ->
	  edge{ source => _N3,
		target => N3,
		type => edge_kind[subst,lexical]
	      },
	  N3=node{ cat => cat[v,prep,nc,np] }
	;   
	  N3 = _N3
	)
	.


:-std_prolog coord_next/3.

coord_next( LastCoord::node{},
	    Start::node{ cluster => cluster{ right => StartRight }},
	    Next::node{ cluster => cluster{ left => NextLeft }}
	  ) :-
%%	format('Search coord next: ~w\n',[Start]),
	(   Next = LastCoord
	;   edge{ source => LastCoord,
		  target => Next,
		  label => label[void,coord2,coord3]
		}
	),
	NextLeft >= StartRight,
	\+ ( ( edge{ source => LastCoord,
		     target => Next2::node{ cluster => cluster{ left => NextLeft2 }},
		     label => label[void,coord2,coord3] }
	     ;	 Next2 = LastCoord
	     ),
	     NextLeft2 >= StartRight,
	     NextLeft2 < NextLeft
	   ),
%%	format('\n\t => ~w\n',[Next]),
	true
	. 


relation{ type => 'APPOS',
	  arg1 => N2::node{ cat => terminal[] },
	  arg2 => N1::node{ cat => nominal[] }
	} :-
	edge{ source => N2, target => N3::node{ cat => 'N2' }, type => adj },
%%	format('HERE ~w ~w\n',[N2,N3]),
	node!empty(N3),
	edge{ source => N3, target => N1, type => subst, label => 'N2app' }
	.

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1, target => N2::node{ cat => np }, label => 'np' },
	edge{ source => N1, label => 'MLex', type => lexical }
	.

relation{ type => 'APPOS',
	  arg1 => N1::node{ cat => terminal[] },
	  arg2 => N2::node{ cat => nominal[] }
	} :-
	edge{ source => N1, target => N2::node{ cat => nominal[] }, label => 'Nc2', type => lexical }
	.

%% N_1->S_2 S_2 S_2->+N_2
%% or S_1 ->+ N_1 S_1 -> S_2 S_2->+N_2
relation{ type => 'JUXT',
	  arg1 => N1::node{ },
	  arg2 => N2::node{ }
	} :-
	edge{ source => S1::node{ cat => CatS1 },
	      target => S2::node{ cat => cat['S'] },
	      label => label['S'],
	      type => adj
	    },
	node!empty(S2),
	edge{ source => S2, target => _N2::node{ cat => _Cat2 }, type => subst, label => label['S'] },
	edge{ source => S2,
	      target => N4::node{ cluster => cluster{ token => Token }},
	      type => lexical },
	domain(Token,[',',';','.',':']),
	( _Cat2 = cat[v] -> recorded(terminal_v(_N2,N2 ))
	; _Cat2 = cat['S'],
	  edge{ source => _N2, target => Comp2, type => subst, label => comp },
	  edge{ source => Comp2, target => N2, type => subst, label => label[comp,'N2'] }
	),
	( CatS1 = cat[v] -> recorded(terminal_v(S1,N1 ))
	; CatS1 = cat['S'],
	  edge{ source => S1, target => Comp1, type => subst, label => comp },
	  edge{ source => Comp1, target => N1, type => subst, label => label[comp,'N2'] }
	)
	.

relation{ type => 'JUXT',
	  arg1 => N1::node{ cat => xnominal[] },
	  arg2 => N2::node{ cat => cat[v] }
	} :-
	edge{ source => N2,
	      target => N3::node{ cat => 'S' },
	      type => adj,
	      label => 'S'
	    },
	edge{ source => N3,
	      target => N1,
	      type => subst,
	      label => person_mod
	    }
	.
	
%% JUXT: to be done

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Decoding relations

:-extensional xrelation/2.

xrelation( relation{ type => Type::'SUJ-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(sujet,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'AUX-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(auxiliaire,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(cod,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'CPL-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(complement,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-V', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COMP', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arghead(complementeur,Id1), arg(verbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'ATB-SO', arg1 => Id1, arg2 => Id2, arg3 => SO, id => Id },
	   xrelation(Type,Id,[ arg(attribut,Id1), arg(verbe,Id2), so(SO) ] )
	 ).

xrelation( relation{ type => Type::'MOD-N', arg1 => Id1, arg2 => Id2, arg3 => Prop, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(nom,Id2), 'a-propager'(Prop) ] )
	 ).

xrelation( relation{ type => Type::'MOD-A', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(adjectif,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-R', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(adverbe,Id2) ] )
	 ).

xrelation( relation{ type => Type::'MOD-P', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(modifieur,Id1), arghead(preposition,Id2) ] )
	 ).

xrelation( relation{ type => Type::'COORD', arg1 => Id1, arg2 => Id2, arg3 => Id3, id => Id },
	   xrelation(Type,Id,[ arg(coordonnant,Id1),arg('coord-g',Id2), arg('coord-d',Id3) ] )
	 ).

xrelation( relation{ type => Type::'APPOS', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(appose,Id2) ] )
	 ).

xrelation( relation{ type => Type::'JUXT', arg1 => Id1, arg2 => Id2, id => Id },
	   xrelation(Type,Id,[ arg(premier,Id1), arg(suivant,Id2) ] )
	 ).

:-rec_prolog f2groups/3.

f2groups([],[],_).

f2groups([Arg|Args],[XArg|XArgs],All) :-
	f2groups(Args,XArgs,All),
	(   Arg = arg(Type,FId),
	    f2group(FId,GId),
	    \+ ( f2group(FId2,GId),
		 FId2 \== FId,
		 domain( arg(_,FId2), All) ) ->
	    XArg = arg(Type,GId)
	; Arg = arghead(Type,FId) ->
	    XArg = arg(Type,FId)
	;   
	    XArg = Arg
	)
	.

/*
f2groups([Arg|Args],[XArg|XArgs]) :-
	f2groups(Args,XArgs),
	( Arg = arghead(Type,FId) ->
	  XArg = arg(Type,FId)
	;   
	  XArg = Arg
	)
	.
*/

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing DEP XML objects

event_process( H::default(Stream), depclusters, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1),
	every(( C::cluster{},
		event_process(depxml(Stream), C, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       cluster{ id => Id,
			left => Left,
			right => Right,
			token => Token,
			lex => Lex
		      },
	       Ctx1,
	       Ctx2
	     ) :-
	(Id = root ->
	 Ctx1 = Ctx2
	;
	 event_process(default(Stream),
		       element{ name => cluster,
				attributes => [ id: Id,
						left: Left,
						right: Right,
						token: Token,
						lex: Lex
					      ]
			      },
		       Ctx1,
		       Ctx2
		      )
	).
	
event_process( H::default(Stream), depnodes, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1),
	every(( N::node{}, event_process(depxml(Stream), N, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       node{ id => Id,
		     cat => Cat,
		     xcat => XCat,
		     tree => Tree,
		     lemma => Lemma,
		     deriv => Derivs,
		     cluster => cluster{ id =>CId },
		     form => Form,
		     w => W
		   },
	       Ctx1,
	       Ctx2
	     ) :-
	( Id = root ->
	  Ctx1 = Ctx2
	;
	  name_builder('~L',[['~w',' '],Tree],L_Tree),
	  Attrs =  [ id: Id,
		     cat: Cat,
		     (tree): L_Tree,
		     lemma: Lemma,
		     cluster: CId,
		     form: Form
		   ],
	  ( var(XCat) -> Attrs1=Attrs ; Attrs1 = [xcat:XCat|Attrs]),
	  ( Derivs = [[]] ->
	    Attrs2 = Attrs1
	  ;
	    mutable(M,[]),
	    every((
		   domain(DId,Derivs),
		   %%	       format('check deriv=~w for op=~w\n',[DId,Id]),
		   ( recorded(keep_deriv(DId)) ->
		     mutable_read(M,L),
		     mutable(M,[DId|L])
		   ;
		     fail
		   )
		  )),
	    mutable_read(M,Derivs2::[_|_]),
	    name_builder('~L',[['~w',' '],Derivs2],L_Derivs),
	    Attrs2 = [deriv:L_Derivs|Attrs1]
	  ),
	    ( W=[] ->
		Attrs3 = Attrs2
	    ;	
		name_builder('~U',[['~w:~w',' '],W],L_W),
		%%		format('Emitting attr ~w => ~w\n',[W,L_W]),
		Attrs3=[w:L_W|Attrs2]
	    ),
	  event_process(default(Stream),
			element{ name => node,
				 attributes => Attrs3
			       },
			Ctx1,
			Ctx2
		       )
	).

event_process( H::default(Stream), depedges, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1),
	every(( E::edge{}, event_process(depxml(Stream), E, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       edge{ id => Id,
		     source => node{ id => SId },
		     target => node{ id => TId },
		     type => Type,
		     label => Label,
		     deriv => Derivs
		   },
	       Ctx1,
	       Ctx2
	     ) :-
	( Id = root(_) ->
	  Ctx1 = Ctx2
	;
	  Attrs =  [ id: Id,
		     source: SId,
		     target: TId,
		     type: Type,
		     label: Label
		   ],
	  event_process(default(Stream),
			start_element{ name => edge,
				       attributes => Attrs
				     },
			Ctx1,
			Ctx3
		       ),
	  mutable(Ctx,Ctx3),
	  every(( domain(Deriv,Derivs),
		  event_process(H,deriv(Deriv,Id),Ctx) )),
	  mutable_read(Ctx,Ctx4),
	  event_process(default(Stream), end_element{ name => edge },
			Ctx4,
			Ctx2)
	).

event_process( H::depxml(Stream), deriv(DId,EId), Ctx1, Ctx2 ) :-
	recorded( deriv(DId,EId,XSpan,SOP,TOP) ),
	recorded( keep_deriv(DId) ),
%%	op{ id => TOP },
	name_builder('~L',[['~w',' '],XSpan],Span),
	event_process(default(Stream),
		      element{ name => deriv,
			       attributes => [ name: DId,
					       source_op: SOP,
					       target_op: TOP,
					       span: Span
					     ]
			     },
		      Ctx1,
		      Ctx2
		     )
	.

event_process( H::default(Stream), depops, Ctx1, Ctx2 ) :-
	mutable(Ctx,Ctx1),
	every(( O::op{}, event_process(depxml(Stream), O, Ctx ))),
	mutable_read(Ctx,Ctx2)
	.

event_process( H::depxml(Stream),
	       op{ id => Id,
		   span => XSpan,
		   cat => Cat,
		   deriv => Derivs,
		   top => Top,
		   bot => Bot
		 },
	       Ctx1,
	       Ctx2
	     ) :-
	mutable(M,[]),
	every((
	       domain(DId,Derivs),
%%	       format('check deriv=~w for op=~w\n',[DId,Id]),
	       ( recorded(keep_deriv(DId)) ->
	         mutable_read(M,L),
		 mutable(M,[DId|L])
	       ;
		 fail
	       )
	      )),
	mutable_read(M,Derivs2::[_|_]),
	name_builder('~L',[['~w',' '],Derivs2],L_Derivs),
	( var(Top) -> Content1 = []
	; Content1 = [narg(top,Top)]
	),
	( var(Bot) -> Content = Content1
	; Content = [narg(bot,Bot)|Content1]
	),
	name_builder('~L',[['~w',' '],XSpan],Span),
	(Content = []	->
	  event_process(default(Stream),
			element{ name => op,
				 attributes => [ id:Id,
						 cat: Cat,
						 span: Span,
						 deriv: L_Derivs
					       ]
			       },
			Ctx1,
			Ctx2
		       )
	;
	 mutable(Ctx,Ctx1),
	 verbose('FS content: ~w\n',[Content]),
	 event_process(default(Stream),
		       [ start_element{ name => op,
					attributes => [ id:Id,
							cat: Cat,
							span: Span,
							deriv: L_Derivs
						      ]
				      },
			 Content,
			 end_element{ name => op }
		       ],
		       Ctx
		      ),
	 mutable_read(Ctx,Ctx2)
	)
	.


event_process( H::default(Stream),narg(Kind,FS), Ctx1,Ctx2 ) :-
	event_process(H,
		      start_element{ name => narg,
				     attributes => [type:Kind]
				   },
		      Ctx1,
		      Ctx3),
	event_process(fsxml(Stream), FS, Ctx3,Ctx4),
	event_process(H, end_element{ name => narg }, Ctx4,Ctx2 )
	.

event_process( H::fsxml(Stream),fs([]),Ctx1,Ctx2) :-
	event_process(default(Stream),element{ name => fs, attributes => [] },Ctx1,Ctx2).

event_process( H::fsxml(Stream),fs(FS::[_|_]),Ctx1,Ctx2) :-
	mutable(Ctx,Ctx1),
	event_process(default(Stream),
		      start_element{ name => fs, attributes => [] },Ctx),
	every(( domain(F:V,FS),
		event_process( fsxml(Stream), feature(F,V), Ctx )
	      )),
	event_process(default(Stream),
		      end_element{ name => fs }, Ctx ),
	mutable_read(Ctx,Ctx2)
	.

event_process( fsxml(Stream), feature(Name,Values),Ctx1,Ctx2) :-
	mutable(Ctx,Ctx1),
	( Values = var(Var,Values2) ->
	  Attrs = [id:Var]
	;
	  Attrs = [],
	  Values2 = Values
	),
	event_process(default(Stream),
		      start_element{ name => f,
				     attributes => [name:Name|Attrs]
				   },
		      Ctx
		     ),
	every(( domain(Value,Values2),
		event_process(fsxml(Stream),Value,Ctx)
	      )),
	event_process(default(Stream),end_element{ name=> f },Ctx),
	mutable_read(Ctx,Ctx2)
	.

event_process( fsxml(Stream), -, Ctx1,Ctx2 ) :-
	event_process(default(Stream), element{ name => minus },Ctx1,Ctx2).

event_process( fsxml(Stream), +, Ctx1,Ctx2 ) :-
	event_process(default(Stream), element{ name => minus },Ctx1,Ctx2).

event_process( fsxml(Stream), val(V), Ctx1,Ctx2 ) :-
	mutable(Ctx,Ctx1),
	event_process(default(Stream),
		      [ start_element{ name => val },
			characters{ value => V },
			end_element{ name => val }
		      ],
		      Ctx
		     ),
	mutable_read(Ctx,Ctx2)
	.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Process for printing XML objects
%%   extended for EASy Objects

%% event_super_handler(default(Stream),default(Stream)).

event_process( H::default(Stream), xmldecl, Ctx, Ctx ) :-
	format(Stream,'<?~w~L?>',
	       [xml,
		[' ~A',' '],
		[ version:'1.0', %'
		  encoding:'ISO-8859-1']]
	      )
	.
	
event_process( H::default(_), C::cluster{ id => Id, left => Left },Ctx1,Ctx2 ) :-
	event_process(H,iterate_in_cluster(Id,Left),Ctx1,Ctx2).

/*
event_process( H::default(_), iterate_in_cluster(Id,Left),Ctx1,Ctx3 ) :-
	( f{ cid => Id, left => Left, right => Right } ->
	    mutable(M,[]),
	    every(( F::f{ cid => _Id, left => Left, right => Right },
		    mutable_read(M,_L),
		    f_sorted_add(F,_L,_LL),
		    mutable(M,_LL)
		  )),
	    mutable_read(M,L),
	    event_process( H, fs(L), Ctx1,Ctx2 ),
	    event_process( H, iterate_in_cluster(Id,Right), Ctx2, Ctx3)
	;
	    Ctx1=Ctx3
	)
	.
*/

event_process( H::default(_), iterate_in_cluster(_CId,Left),Ctx1,Ctx2 ) :-
	mutable(M,[]),
	every(( ( CId=_CId
		; recorded( redirect(_CId,CId) )),
	        F::f{ cid => CId },
		mutable_read(M,_L),
		f_sorted_add(F,_L,_LL),
		mutable(M,_LL)
	      )),
	mutable_read(M,L),
%%	format('Emit Cluster: ~w => ~w\n',[CId,L]),
	event_process( H, fs(L), Ctx1,Ctx2 )
	.

:-rec_prolog f_sorted_add/3.

f_sorted_add(F,[],[F]).
f_sorted_add(F1::f{ rank => R1 }, L::[F2::f{ rank => R2 }|L2],XL) :-
	(  R1 =< R2 ->
	    XL = [F1|L]
	;
	    XL = [F2|XL2],
	    f_sorted_add(F1,L2,XL2)
	)
	.
	    
event_process( H::default(_),fs([]),Ctx,Ctx).
event_process( H::default(_),fs([F|L]),Ctx1,Ctx3) :-
	event_process(H,F,Ctx1,Ctx2),
	event_process(H,fs(L),Ctx2,Ctx3)
	.

event_process( H::default(_),
	       f{ id => Id, lex => Lex, cid => CId, rank => Rank },
	       Ctx1,
	       Ctx4
	     ) :-
	verbose('Handling f id=~w lex=~w ctx=~w\n',[Id,Lex,Ctx1]),
	(   recorded( emitted(Id) ),
	    Ctx4 = Ctx1
	xor
	    \+ domain(Ctx1,[open(_,_,_),middle(_,_,_),close(_,_,_)]),
	    \+ used( CId ),
	    recorded( redirect(_CId,CId) ),
	    used(_CId),
	    Ctx4 = Ctx1,
	    verbose('To be emitted later in a group for cid=~w: f id=~w lex=~w ctx=~w\n',[_CId,Id,Lex,Ctx1]),
	    true
	xor  
	( domain(Lex,['�','"','''','�','&quot;']),
	  Ctx1 = open(_Ctx5,GId,Type) ->
	  Ctx4 = open(_Ctx4,GId,Type)
	; domain(Lex,['�','"','''','.','...','&quot;']),
	  Ctx1 = middle(_Ctx1,GId,Type),
	  \+ ( %% check Lex is indeed the last F of group GId
	       ( f{ id => _FId, cid => CId, rank => _Rank },
		 Rank < _Rank
	       ;
		 cluster{ id => CId, right => _Left },
		 cluster{ id => _CId, left => _Left, right => _Right },
		 _Left < _Right,
		 f{ id => _FId, cid => _CId }
	       ),
	       recorded( f2group(_FId,GId) )
	     )
	->
	  event_process( H, end_element{ name => 'Groupe' }, _Ctx1, _Ctx5 ),
	  Ctx4=close(_Ctx4,GId,Type)
	; Ctx1 = open(_Ctx1,GId,Type) ->
	  event_process( H, start_element{ name => 'Groupe',
					   attributes => [id:GId,type:Type] },
			 _Ctx1,
			 _Ctx5 ),
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = middle(_Ctx5,GId,Type) ->
	  Ctx4 = middle(_Ctx4,GId,Type)
	; Ctx1 = close(_Ctx5,GId,Type) ->
	  Ctx4 = close(_Ctx4,GId,Type)
	; Ctx4 = _Ctx4,
	  Ctx1 = _Ctx5
	),
	    mutable(MCat,[]),
	    every((( recorded( redirect(_CId,CId) ),
		     node{ cluster => cluster{ id => _CId }, cat => _Cat }
		   ; node{ cluster => cluster{ id => CId }, cat => _Cat }
		   ),
		   mutable_read(MCat,_LCat),
		   mutable(MCat,[_Cat|_LCat])
		  )),
	    mutable_read(MCat,LCat),
	    ( LCat = [] -> Cat = ''
	    ; LCat = [Cat] -> true
	    ; name_builder('~L',[['~w',' '],LCat],Cat)
	    ),
	    event_process( H, start_element{ name => 'F', attributes => [id:Id,cat:Cat] }, _Ctx5, _Ctx2 ),
	    event_process( H, characters{ value => Lex }, _Ctx2, _Ctx3 ),
	    event_process( H, end_element{ name => 'F' }, _Ctx3, _Ctx4 ),
	    record_without_doublon( emitted(Id) )
	)
	.

:-extensional f2group/2.

event_process( H::default(_),group(Type,_,_,_,Content),Ctx1,Ctx4) :-
	group_gensym(Id),
	verbose('Handling group ~w ~w ~w\n',[Id,Type,Content]),
	every(( domain(_CId,Content),
		( CId = _CId
		;  recorded( redirect( _CId, CId) )
		),
		f{ cid => CId, id => FId },
		verbose('Handling cid=~w fid=>~w\n',[CId,FId]),
		record_without_doublon( f2group(FId,Id) ))),
%%	event_process( H, start_element{ name => 'Groupe', attributes => [id:Id,type:Type] }, Ctx1, Ctx2 ),
	Ctx1=Ctx2,
	event_process( H,
		       flist(Content),
		       open(Ctx2,Id,Type),
		       close(Ctx3,Id,Type)
		     ),
	Ctx3=Ctx4,
%%	event_process( H, end_element{ name => 'Groupe' }, Ctx3, Ctx4 ),
	true
	.

event_process( H::default(_), flist([]),
	       open(Ctx,GId,Type),
	       close(Ctx,GId,Type)
	     ) :-
	verbose('*** Void group: ~w ~w\n',[GId,Type])
	.

event_process( H::default(_), flist([]),
	       close(Ctx,GId,Type),
	       close(Ctx,GId,Type)
	     ) :-
	verbose('*** Group already closed: ~w ~w\n',[GId,Type])
	.

event_process( H::default(_), flist([]),
	       middle(Ctx1,GId,Type),
	       close(Ctx2,GId,Type)
	     ) :-
	event_process( H, end_element{ name => 'Groupe' }, Ctx1, Ctx2 )
	.
	
event_process( H::default(_), flist([Id|L]), Ctx1, Ctx3 ) :-
	C::cluster{id => Id },
	event_process(H,C,Ctx1,Ctx2),
	event_process(H,flist(L),Ctx2,Ctx3)
	.

event_process( H::default(_), constituants, Ctx1, Ctx2) :-
%%	event_process( H, start_element{ name => 'constituants', attributes => [] }, Ctx1, Ctx2 ),
	event_process( H, iterate_constituant(0), Ctx1, Ctx2 )
	.

event_process( H::default(_), iterate_constituant(N), Ctx1, Ctx3 ) :-
%%	format('ITERATE CONSTITUANTS ~w',[N]),
	(   constituant(Left,Right,Const),
	    Left >= N,
	    \+ ( constituant(_Left,_,_), _Left >= N, Left > _Left )
	->
%%	    format('CONST ~w\n',[Const]),
	    event_process(H,Const,Ctx1,Ctx2),
	    event_process(H,iterate_constituant(Right),Ctx2,Ctx3)
	;   
%%	    event_process(H,end_element{ name => 'constituants' }, Ctx1,Ctx3)
	    Ctx1=Ctx3
	)
	.

event_process( H::default(_), relations, Ctx1, Ctx4 ) :-
	event_process(H,start_element{ name => relations, attributes => []}, Ctx1, Ctx2 ),
	mutable(Ctx,Ctx2),
	every(( domain(Type,rel[]),
		'$answers'( relation(R::relation{ type => Type }) ),
		xrelation(R,XR::xrelation(Type,Id,Content)),
		(   f2groups(Content,Content1,Content)		   
		xor Content = Content1 ),
		event_process(H,xrelation(Type,Id,Content1),Ctx)
	      )),
	mutable_read(Ctx,Ctx3),
	event_process(H,end_element{ name => relations }, Ctx3, Ctx4 )
	.

event_process( H::default(_), xrelation(Type,Label,Content), Ctx1, Ctx4) :-
	event_process(H,
		      start_element{ name => relation,
				     attributes => [ xlink!type: extended,
						     type: Type,
						     id: Label ] },
		      Ctx1,
		      Ctx2 ),
	mutable(Ctx,Ctx2),
	event_process(relarg(H),Content,Ctx),
	mutable_read(Ctx,Ctx3),
	event_process(H, end_element{ name => relation }, Ctx3, Ctx4 )
	.

event_process( relarg(H), arg(Type,Label), Ctx1, Ctx2 ) :-
	event_process(H,
		      element{ name => Type,
			       attributes => [ xlink!type: locator,
					       xlink!href: Label
					     ]
			     },
		      Ctx1, Ctx2
		     )
	.

event_process( relarg(H), so(SO), Ctx1, Ctx2 ) :-
	event_process(H,
		      element{ name => 's-o',
			       attributes => [valeur: SO]
			     },
		      Ctx1, Ctx2
		     ).

event_process( relarg(H), 'a-propager'(Prop), Ctx1, Ctx2 ) :-
	(Prop = faux xor true),
	event_process(H,
		      element{ name => 'a-propager',
			       attributes=> [booleen: Prop]
			     },
		      Ctx1, Ctx2
		     )
	.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Event Handler

event_handler( Reader,
	       OE::start_element{ name => E::dependencies,
				  attributes => Attrs },
	       OE,
	       [],
	       []
	     ) :-
	attrs([mode:Mode], Attrs),
	(   Mode = 'full' xor true ),
	record( mode(Mode) )
	.

event_handler( Reader,
	       OE::start_element{ name => cluster,
				  attributes => Attrs,
				  empty => true
				},
	       OE,
	       [],
	       []
	     ) :-
	attrs([id:Id,left:XLeft,right:XRight,lex:Lex,token:Token], Attrs),
	utf8_to_xmllatin1(Lex,Latin_Lex),
	(Token = Lex xor true),
	utf8_to_xmllatin1(Token,Latin_Token),
	atom_number(XLeft,Left),
	atom_number(XRight,Right),
	/*
	label2lex(Latin_Lex,_,FIds),
	format('label2lex ~w => ~w\n',[Latin_Lex,FIds]),
	(   FIds = [] ->
	    TLeft = Left,
	    TRight = Right
	;
	    FIds = [F1|_],
	    format('try conversion ~w\n',[F1]),
	    atom_number(F1,TLeft),
	    format('=> TLeft = ~w\n',[TLeft]),
	    length(FIds,N),
	    TRight is TLeft+N,
	    format('=> TRight = ~w\n',[TRight]),
	    true
	),
        */
	Cluster = cluster{ id => Id,
			   left => Left,
			   right => Right,
			   lex => Latin_Lex,
			   token => Latin_Token
			 },
	( Right > Left + 1, Latin_Lex == '_' ->
	    %% bug in frmg => temporary special handling
	    record( tmp(Cluster) )
	;   
	    record( Cluster )
	),
%%	format('~w lex=~w latin=~w\n',[Cluster,Lex,Latin_Lex]),
	true
	.



:-light_tabular fix_clusters/0.

fix_clusters :-
	%% post reading correction on some clusters
	%% because of bug in frmg
	every(( recorded( tmp(cluster{ id => Id,
				       left => Left,
				       right => Right
				     }) ),
		glue_clusters(Left,Right,Lex),
		verbose('clue cluster ~w ~w => ~w\n',[Left,Right,Lex]),
		record( cluster{ id => Id,
				 left => Left,
				 right => Right,
				 lex => Lex
			       } )))
	.

:-std_prolog glue_clusters/3.

glue_clusters(Left,Right,Lex) :-
	( Left == Right ->
	    Lex = ''
	;   
	    recorded( cluster{ left => Left,
			       right => Middle,
			       lex => Lex1 } ),
	    Lex1 \== '',
	    Middle > Left,
	    glue_clusters(Middle,Right,Lex2),
	    ( Lex2 == '' ->
		Lex = Lex1
	    ;	
		name_builder('~w ~w',[Lex1,Lex2],Lex)
	    )
	)
	.

:-std_prolog attr2weights/2.

attr2weights(XW,W) :-
	( XW=[] ->
	    W = []
	;   XW = [X|XW2] ->
	    attr2weights(XW2,W2),
	    rx!tokenize(X,':',[F,V]),
	    atom_number(V,VV),
	    W = [F:VV|W2]
	)
	.

event_handler( Reader,
	       OE::start_element{ name => node,
				  attributes => Attrs,
				  empty => true
				},
	       OE,
	       [],
	       []
	     ) :-
	fix_clusters,
	attrs([id:Id, cluster:CId, (tree):Tree, cat:Cat, lemma: Lemma, xcat: XCat, deriv: Derivs,w:Weights, form:Form], Attrs),
	utf8_to_xmllatin1(Lemma,Latin_Lemma),
	recorded(Cluster::cluster{ id => CId, token => Token }),
	( Form = Token, Latin_Form=Token xor
	utf8_to_xmllatin1(Form,Latin_Form)
	),
	rx!tokenize(Tree,' ',XTree),
	(Derivs = [] xor true),
	rx!tokenize(Derivs,' ',XDerivs),
	(   Weights = [] ->
	    W=[]
	;
%%	    format('Processing weights ~w\n',[Weights]),
	    rx!tokenize(Weights,' ',XWeights),
	    attr2weights(XWeights,W)
	),
	Node = node{ id => Id,
		     cluster => Cluster,
		     tree => XTree,
		     cat => Cat,
		     lemma => Latin_Lemma,
		     xcat => XCat,
		     deriv => XDerivs,
		     w => W,
		     form => Latin_Form
		   },
	record( Node ),
%%	format('~w\n',[Node]),
	true
	.

event_handler( Reader,
	       start_element{ name => E::edge,
			      attributes => Attrs
				},
	       OE::end_element{ name => E },
	       [],
	       []
	     ) :-
	attrs([id:Id,source:SourceId,target:TargetId,type:Type,label:Label], Attrs),
	( recorded( Source::node{ id => SourceId } )
	xor
	verbose('*** Missing source node id=~w in edge ~w\n',[SourceId,Id]),
	  fail
	),
	( recorded( Target::node{ id => TargetId } )
	xor
	verbose('*** Missing target node id=~w in edge ~w\n',[SourceId,Id]),
	  fail
	),
	Edge = edge{ id => Id,
		     source => Source,
		     target => Target,
		     type => Type,
		     label => Label,
		     deriv => Deriv
		   },
	event_handler( Reader, loop, OE, Edge * [], Edge * Deriv),
%%	format('~w\n',[Edge]),
	record( Edge ),
	true
	.

:-std_prolog span2xspan/2.

span2xspan(Span,XSpan) :-
	rx!tokenize(Span,' ', _XSpan),
	(_XSpan = [_L,_R] ->
	 atom_number(_L,L),
	 atom_number(_R,R),
	 XSpan = [L,R]
	; _XSpan = [_L,_L1,_R1,_R] ->
	 atom_number(_L,L),
	 atom_number(_R,R),
	 atom_number(_L1,L1),
	 atom_number(_R1,R1),
	 XSpan = [L,L1,R1,R]
	;
	 fail
	)
	.

event_handler( Reader,
	       OE::start_element{ name => deriv,
				  attributes => Attrs,
				  empty => true
				},
	       OE,
	       (Edge :: edge{ id => EId }) * Deriv,
	       Edge * [Name|Deriv]
	     ) :-
	attrs([name:Name,span:Span,source_op:SOP,target_op:TOP],Attrs),
	span2xspan(Span,XSpan),
	record_without_doublon(deriv(Name,EId,XSpan,SOP,TOP))
	.


event_handler( Reader,
	       OE1::start_element{ name => E::op,
				   attributes => Attrs,
				   empty => Empty
				 },
	       OE2,
	       [],
	       []
	     ) :-
	attrs([id:Id,cat:Cat,span:Span,deriv:Derivs],Attrs),
	rx!tokenize(Derivs,' ',XDerivs),
	span2xspan(Span,XSpan),
	Op = op{ id => Id,
		 cat => Cat,
		 deriv => XDerivs,
		 span => XSpan,
		 top => Top,
		 bot => Bot
	       },
	every(( domain(D,XDerivs),
		record_without_doublon( deriv2span(D,XSpan) )
	      )),
	( Empty == true ->
	  OE2=OE1
	;
	  OE2=end_element{ name => E },
	  event_handler( Reader, loop, OE2, Op, Op )
	),
	record(Op),
	true
	.

event_handler( Reader,
	       start_element{ name => E::narg,
			      attributes => Attrs
			    },
	       OE::end_element{ name => E },
	       Op::op{ top => Top, bot => Bot },
	       Op
	     ) :-
	attrs([type:Type],Attrs),
	( Type = top -> Arg = Top ; Arg = Bot ),
	event_handler( Reader, loop, OE, [], Arg )
	.

event_handler( Reader,
	       OE1::start_element{ name => E::fs,
				   empty => Empty },
	       OE2,
	       [],
	       fs(Arg)
	     ) :-
	( Empty == true ->
	  OE2 = OE1,
	  Arg = []
	;
	  OE2=end_element{ name => E },
	  event_handler(Reader,loop,OE2,[],Arg)
	)
	.

event_handler( Reader,
	       OE1::start_element{ name => E::f,
				   attributes => Attrs,
				   empty => Empty
				 },
	       OE2,
	       L,
	       [Name:V|L]
	     ) :-
	attrs([name:Name,id:Var],Attrs),
%%	format('Reading ~w ~w\n',[Name,L]),
	( Empty == true ->
	  OE2=OE1,
	  V1=[]
	;
	  OE2=end_element{ name => E },
	  event_handler(Reader,loop,OE2,[],V1)
	),
	( var(Var) -> V = V1 ; V = var(Var,V1) )
	.

event_handler( Reader,
	       OE::start_element{ name => E::minus, empty => true },
	       OE,
	       V,
	       [(-)|V]
	     ).

event_handler( Reader,
	       OE::start_element{ name => E::plus, empty => true },
	       OE,
	       V,
	       [(+)|V]
	     ).

event_handler( Reader,
	       start_element{ name => E::val },
	       OE::end_element{ name => E},
	       V,
	       VV
	     ) :- event_handler(Reader,characters,OE,V,VV).

event_handler( Reader,
	       OE::characters{ value => V1 },
	       OE,
	       V,
	       [val(V1)|V]
	     ).

:-rec_prolog attrs/2.

attrs([],_).
attrs([A:V|Attrs],All_Attrs) :-
	(   domain(A:V,All_Attrs) xor true ),
	attrs(Attrs,All_Attrs)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Tools

:-std_prolog utf8_to_xmllatin1/2.

utf8_to_xmllatin1(UTF8,Latin1) :-
	%%	format('Call with ~w\n',[UTF8]),
	%% Conversion to latin1 + replacement of &<> by entities
	'$interface'( 'UTF8_to_XMLLatin1'(UTF8:string), [ return(Latin1:string) ] )
	.

:-std_prolog name_builder/3.

name_builder(Format,Args,Name) :-
        string_stream(_,S),
        format(S,Format,Args),
        flush_string_stream(S,Name),
        close(S)
        .

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Primitives to navigates within dependencies


:-light_tabular node!neighbour/3.
:-mode(node!neighbour/3,+(+,+,-)).

node!neighbour(left, X, N::node{} ) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => P },
	_N::node{ cluster => ClusterB, form => _Form },
	( _Form = '_EPSILON' ->
	  node!neighbour(left,_N,N)
	;
	  N=_N
	)
	.

node!neighbour(right, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P} } xor X = P ),
	ClusterB::cluster{ left => P },
	N::node{ cluster => ClusterB }
	.

node!neighbour(xleft, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ left => P} } xor X = P ),
	ClusterB::cluster{ right => Q }, Q =< P,
	N::node{ cluster => ClusterB }
	.

node!neighbour(xright, X, N::node{}) :-
	(   X = node{ cluster => ClusterA::cluster{ right => P } } xor X = P ),
	ClusterB::cluster{ left => Q }, P =< Q,
	N::node{ cluster => ClusterB }
	.

:-std_prolog node!empty/1.

node!empty( node{ cluster => cluster{ lex => ''} } ).


:-light_tabular node!dependency/4.
:-mode(node!dependency/4,+(+,+,-,-)).

node!dependency(out,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ source => N1, target => N2 }
	.

node!dependency(in,N1::node{},N2::node{},[Edge]) :-
	Edge::edge{ target => N1, source => N2 }
	.

/*
node!dependency(xout,N1::node{},N2::node{},L) :-
	@*{ goal => ( _E::edge{ source => _N1, target => _N2 },
			\+ domain(edge{ source => _N2 },_L)
		    ),
	    from => 1,
	    collect_first => [N1,[]],
	    collect_last  => [N2,L],
	    collect_loop => [ _N1::node{}, _L],
	    collect_next => [ _N2::node{}, [_E|_L] ]
	  }
	.
*/

node!dependency(xout,N1::node{},N2::node{},L) :-
	node!dependency(out,N1,N3,[E]),
	N1 \== N3,
	(   N2 = N3,
	    L = [E]
	;   
	    node!dependency(xout,N3,N2,L1),
	    (\+ domain( edge{ target => N1 }, L1 )),
	    L = [E|L1]
	)
	.

node!dependency(xin,N1::node{},N2::node{},L) :-
	@*{ goal => ( _E::edge{ target => _N1, source => _N2 },
			\+ domain( edge{ target => _N2 },L),
			_N2 \== N1 % to avoid loop
		    ),
	    from => 1,
	    collect_first => [N1,[]],
	    collect_last  => [N2,L],
	    collect_loop => [ _N1::node{}, _L],
	    collect_next => [ _N2::node{}, [_E|_L] ]
	  }
	.

:-light_tabular node!safe_dependency/3.
:-mode(node!safe_dependency/4,+(+,+,-)).

node!safe_dependency(xout,N1::node{},N2::node{}) :-
	node!dependency(out,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xout,N3,N2)
	),
	N1 \== N2
	.

node!safe_dependency(xin,N1::node{},N2::node{}) :-
	node!dependency(in,N1,N3,_),
	(   N2 = N3
	;   
	    node!safe_dependency(xin,N3,N2)
	),
	N1 \== N2
	.

:-xcompiler(( node!collect(X^G,L) :-
	    mutable(M,[]),
	      every((G,
		     mutable_read(M,L1),
		     (	 \+ domain(X,L1)),
		     X = node{ cluster => cluster{ lex => LexX}},
		     LexX \== '',		     
		     node!add(X,L1,L2),
		     mutable(M,L2))),
	      mutable_read(M,L))).


:-rec_prolog node!add/3.

node!add( N::node{},[],[N]).
node!add( N::node{ cluster => Cluster::cluster{ left => Left}},
	  L1::[N1::node{ cluster => Cluster1::cluster{ left => Left1}}|XL1],
	  L2
	) :-
	( Cluster == Cluster1 ->
	    L2 = L1
	;   
	    Left < Left1 ->
	    L2 = [N|L1]
	;   
	    node!add(N,XL1,XL2),
	    L2 = [N1|XL2]
	),
%%	format('NODE ADD ~w\n',[L2]),
	true
	.

:-rec_prolog node!add_fillers/2.

node!add_fillers( L::[N::node{}], L ).
node!add_fillers( L::[N1::node{ cluster => cluster{ left => Left1, right => Right1 }},
		      N2::node{ cluster => cluster{ left => Left2, right => Right2 }} | LL ],
		  XL::[N1|XL2]
		 ) :-
	(   N::node{ cluster => cluster{ lex => _Lex, left => Right1, right => _Left } },
	    _Lex \== '',
	    _Left =< Left2 ->
	    node!add_fillers([N,N2|LL],XL2)
	;
	    node!add_fillers([N2|LL],XL2)
	)
	.

:-std_prolog node!terminal/3.

node!terminal(N,Cat,Path) :-
	\+ ( node!dependency(out,N, node{ cat => Cat },[edge{ label => Label} ]),
	       domain(Label,Path)
	   )
	.

:-std_prolog node!parent/4.

node!parent(N,Cat,P,Path) :-
	node!dependency(in,N,P::node{ cat => XCat },[edge{ label => Label }]),
	domain(Label,Path),
	domain(XCat,Cat)
	.

:-std_prolog node!older_ancestor/4.

node!older_ancestor(N,Cat,P,Path) :-
	(   node!parent(N,Cat,P1,Path),
	    node!older_ancestor(P1,Cat,P,Path)
	xor P = N
	).

:-std_prolog node!first_v_ancestor/2.

node!first_v_ancestor(N::node{cat => Cat::cat[aux,v], tree => Tree}, N1) :-
	( Cat == v ->
	    N1 = N
	;   Cat == aux, domain(cleft_verb,Tree) ->
	    N1 = N
	;   
	    node!parent(N,cat[aux,v],N2,['Infl','V']),
	    node!first_v_ancestor(N2,N1)
	)
	.

%% BUG SOMEWHERE IN DYALOG on @*

:-light_tabular node!group/4.
:-mode(node!group/4,+(+,-,-,-)).

node!group(Nodes,Words,Left,Right) :-
	%%	format('Start ~w\n',[Nodes]),
	@*{ goal => ( _Nodes1 = [ N::node{ cluster => cluster{ id => Id,
							       left => CLeft,
							       right => CRight }}
				| _Nodes],
			( domain(Id,Words) ->
			    _Words1 = _Words
			;   
			    _Words1 = [Id|_Words]
			),
%%			format('HERE ~w ~w ~w\n',[_Nodes,_Left,_Right]),
			( CLeft < _Left -> NLeft = CLeft ; NLeft = _Left ),
			( CRight > _Right -> NRight = CRight ; NRight = _Right ),
			true
		    ),
	    %%	    from => 0,
	    %%	    vars => [Words],
	    collect_first => [Nodes,Words,1000,0],
	    collect_last => [[],[],Left,Right],
	    collect_loop => [_Nodes1,_Words1,_Left,_Right],
	    collect_next => [_Nodes,_Words,NLeft,NRight]
	  },
	%% format('End ~w ~w ~w\n',[Words,Left,Right]),
	true
	.


:-light_tabular label2lex/3.
:-mode(label2lex/3,+(+,-,-)).

label2lex(Label,Lex,FIds) :-
	rx!tokenize(Label,' ',Labels),
	label2lex_aux(Labels,Lex,FIds).

:-std_prolog label2lex_aux/3.

label2lex_aux(Labels,Lex,FIds) :-
	(   Labels = [] -> Lex=[], FIds = []
	;   Labels = [X|Labels2],
	    (	rx!tokenize(X,'|',[FX,LX]) xor LX=X, FX='' ),
	    Lex = [LX|Lex2],
%%	    format('Try match ~w\n',[FX]),
	    rx!match{ string => FX,
		      rx => rx{ pattern => '\\([0-9][0-9]*\\)$' },
		      substrings => [_FX],
		      subs_flag => 1
		    },
%%	    format('=> ~w\n',[_FX]),
	    FIds = [_FX|FIds2],
	    label2lex_aux(Labels2,Lex2,FIds2)
	)
	.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%:-std_prolog verbose/2.

:-xcompiler(( verbose(Msg,Args) :- (	\+ opt(verbose) xor format(Msg,Args)))).
%%:-xcompiler((verbose(Msg,Args) :- true )).


:-std_prolog reverse/3.

reverse(L,LL,Acc) :-
        ( L=[A|B] -> reverse(B,[A|LL],Acc) ; Acc=LL)
        .

:-std_prolog record_without_doublon/1.

record_without_doublon( A ) :-
        (recorded( A ) xor record( A ))
        .

:-std_prolog update_counter/2.

update_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_inc(M,V)
        ;   V=1,
            mutable(M,2),
            record( counter(Name,M) )
        )
        .

:-std_prolog value_counter/2.

value_counter(Name,V) :-
        ( recorded( counter(Name,M) ) ->
            mutable_read(M,V)
        ;
            V = 1
        )
        .

:-std_prolog group_gensym/1.

group_gensym(L) :-
	sentence(Sent),
	update_counter(group,X),
	name_builder('E~wG~w',[Sent,X],L)
	.

:-std_prolog rel_gensym/1.

rel_gensym(L) :-
	sentence(Sent),
	update_counter(relation,X),
	name_builder('E~wR~w',[Sent,X],L)
	.

:-std_prolog mark_as_used/1.

mark_as_used(L) :-
	every(( domain( Label, L),
		record_without_doublon(used(Label)),
% 		( recorded( redirect(Label,CId1) ) ->
% 		  record_without_doublon(used(CId1))
% 		;
% 		  true
% 		),
		true
	      )).

format_hook(0'E,Stream,[edge{ id => Id, source => N1, target => N2, label => L}|R],R) :-
        format(Stream,'~w:~E-~w->~E',[Id,N1,L,N2])
        .

format_hook(0'E,Stream,[node{ id => Id,cat => Cat,cluster => C, lemma => L, form=> F }|R],R) :-
	( L == '' ->
	  format(Stream,'~w/~w/~E',[Id,Cat,C])
	;
	  format(Stream,'~w/~w:~w__~w/~E',[Id,F,L,Cat,C])
	)
        .

format_hook(0'E,Stream,[cluster{ id => Id, lex => Lex, token => Token}|R],R) :-
	(Token == Lex ->
	 format(Stream,'~w{~w}',[Id,Lex])
	;
	 format(Stream,'~w{~w:~w}',[Id,Lex,Token])
	)
	.

format_hook(0'e,Stream,[Id|R],R) :-
	E::edge{ id => Id },
	format(Stream,'~E',[E])
	.

format_hook(0'L,Stream,[[Format,Sep],AA|R],R) :-
        mutable(M,0,true),
        every((   domain(A,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A]) ))
        .

format_hook(0'U,Stream,[[Format,Sep],AA|R],R) :-
        mutable(M,0,true),
        every((   domain(A:B,AA),
                  mutable_inc(M,V),
                  (   V == 0 xor write(Stream,Sep) ),
                  format(Stream,Format,[A,B]) ))
        .


:-std_prolog random/1.

random(X) :- '$interface'( random, [return(X:int)] ).
			 
:-extensional pri/2.

pri('combien?','GR').
pri('quand?','GR').
pri('o�?','GP').
pri('pourquoi?','GR').
pri('comment?','GR').
pri('commentComp?',GR).

:-extensional prel/2.

prel('dont','GP').

%% Prep:

%% v
%% pendant

%% nc

%% adj

%% favor adv with following verb rather than with aux

%% verify MOD-N entre dans 'quel X' 'chaque X'

%% verifier status de combien

%% Verifier MOD-N sur Revues dans am:33

%% Etrange SUJ-V sur am:43

%% Mauvais COMP dans am:54 + beaucoup d'autres phenomenes etranges

%% Verifier entre COD-V ou CPL-V pour "de Sinf"

%% Eviter interpretations de "prep que' comme GP et preferer 'csu' donc COMP

%% Renforcer encore "GN de GN" plutot que argument de verbe (COD-V)

%% am:243 traitement coordonant ternaire

%% am:244 coord sur ADV

%% am:246 coord sur ADJ

:-extensional not_a_prep/1.

not_a_prep('il y a').

:-extensional catpref/4.

catpref(que,_,csu,1000).
catpref(mais,mais,coo,1000).
catpref(donc,donc,coo,1000).
catpref(plus,plus,adv,1000).
catpref(aussi,aussi,adv,1000).
catpref(pour,pour,prep,1000).
catpref(alors,alors,adv,1000).
catpref(avoir,avoir,v,1000).
catpref(avoir,avoir,aux,1000).
catpref(concernant,concerner,v,1000).
catpref(la,_,nc,-1000).
catpref('par example',_,adv,2000).
catpref('eh bien',_,pres,2000).
catpref(avec,avec,prep,1000).
catpref(cent,_,det,1000).
catpref('au moins',_,adv,2000).
catpref('en particulier',_,adv,2000).
catpref('alors',_,adv,100).
catpref('m�me',_,adv,100).
catpref('d'' abord',_,adv,100).
catpref('en outre',_,adv,2000).
catpref('en effet',_,adv,2000).
catpref('� peine',_,adv,2000).
catpref('surtout',_,adv,2000).
catpref('maintenant',maintenant,adv,1000).
catpref('trop',trop,adv,1000).
catpref('sans doute',_,adv,1000).
catpref('en m�me temps',_,adv,1000).
catpref('sans cesse',_,adv,1000).
catpref('de m�me',_,adv,2000).
catpref('au mieux',_,adv,2000).
catpref('tout � l'' heure',_,adv,2000).
catpref('tout � fait',_,adv,2000).
catpref('en vigueur',_,adv,3000).
catpref(_,'afin de',prep,4000).
catpref(_,'� partir de',prep,1000).
catpref(_,'autour de',prep,1000).
catpref(_,'au dehors',adv,1000).
catpref(_,'au dehors de',prep,1000).
catpref('candidat','candidat',nc,1000).
catpref(_,'de plus en plus',adv,5000).
catpref(_,'de moins en moins',adv,5000).
catpref(_,'mort',adj,1000).
catpref(_,'mort',adj,1000).
catpref(_,'quelque chose',pro,3000).
catpref(_,'quelqu''un',pro,3000).
catpref(_,'fille',nc,500).
catpref(_,'dernier',adj,100).
catpref(_,'prochain',adj,200).
catpref(_,'europ�en',adj,1000).
catpref('il y a','il y a',prep,-2000).
catpref('en vain','en vain',adv,3000).
catpref(_,'autre',adj,500).
catpref(_,'en revanche',adv,2000).
catpref(_,'conform�ment �',prep,-2000).
catpref(_,'par ailleurs',adv,2000).
catpref('tout d''abord',_,adv,3000).
catpref(_,'en moyenne',adv,2000).
catpref(_,'d''ailleurs',adv,3000).

%% � m�me de: prep

%% ?? 'quelque chose de p�trifi�' construction specifique sur 'quelque chose de'

%% run27 lemonde:63 etrange !
%% run27 lemonde:78
%%       lemonde:189
%%       lemonde:205 double sujet dont un sur aux

