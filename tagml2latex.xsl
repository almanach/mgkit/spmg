<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output
	method="text"
	indent="no"
	encoding="ISO-8859-1"/>

<xsl:strip-space elements="*"/>

<xsl:template match="/tag">
	<xsl:text>
\documentclass[dvips]{article}

\usepackage{times}
\usepackage{pslatex}

\usepackage[ps2pdf]{hyperref}

\usepackage[T1]{fontenc}
\usepackage[latin1]{inputenc}

\usepackage{latexsym}
\usepackage[leqno,centertags]{amsmath}

\usepackage{a4wide}

\usepackage{avm}

\usepackage{pstcol}
\usepackage{pstricks}
\usepackage{pst-node}
\usepackage{pst-tree}

\avmvskip{.2ex}
%%\avmoptions{labeled}

%\psset{arrows=->,unit=.75cm,treesep=1cm,levelsep=1.5cm,nodesep=2pt,treefit=loose}
\psset{arrows=->,unit=.3cm,treesep=.3cm,levelsep=.6cm,nodesep=2pt,treefit=tight}

\makeatletter
%%\def\lab#1{~[tnpos=r]{#1}}
\def\lab#1{{\footnotesize #1}}

\newif\ifTAG@xname\TAG@xnamefalse
\def\psset@xname#1{\def\psk@xname{#1}\TAG@xnametrue}

\newif\ifTAG@xopt\TAG@xoptfalse
\def\psset@xopt#1{\def\psk@xopt{#1}\TAG@xoptfalse}

\newpsstyle{dom}{linecolor=blue,linestyle=dashed}
\newpsstyle{father}{linecolor=black,linestyle=solid}

\def\namewrap#1{
\ifTAG@xopt
  (#1)
\else
  #1
\fi
}

\def\stdN{\def\pst@par{}\pst@object{stdN}}
\def\stdN@i{\stdN@ii}
\def\stdN@ii#1#2{
\begin@OpenObj%
\pstree{%
\def\name{\namewrap{#1}}
\ifTAG@xname
\TR[name=N\psk@xname]{\name}~[tnpos=r,tnyref=.8]{\lab{\psk@xname}}
\else
\TR{\name}
\fi
}{#2}%
\end@OpenObj%
%%\pstree{\TR{#1}~[tnpos=r,tnyref=.8]{\lab{\psk@xname}}}{#2}
}

\def\leafN{\def\pst@par{}\pst@object{leafN}}
\def\leafN@i{\leafN@ii}
\def\leafN@ii#1{
\begin@OpenObj%
\ifTAG@xopt
  \def\name{(#1)}
\else
  \def\name{#1}
\fi
\ifTAG@xname
\TR[name=N\psk@xname]{\name}~[tnpos=r,tnyref=.8]{\lab{\psk@xname}}
\else
\TR{\name}
\fi
\end@OpenObj%
}

\def\anchor#1{\textcolor{cyan}{\textbf{#1}}$\Diamond$}
\def\coanchor#1{\textcolor{cyan}{\textbf{#1}}$&lt;=&gt;$}
\def\foot#1{#1$\star$}
\def\subst#1{#1$\downarrow$}
\def\lex#1{\textcolor{magenta}{#1}$\diamond$}

\makeatother

\def\NodeError#1{\psframebox[fillstyle=vlines,linecolor=red,hatchcolor=red]{#1}}

\newenvironment{NodeArg}[2]{
  #1.#2=\begin{avm}%
}{
  \end{avm}
}

\begin{document}
\tableofcontents
	</xsl:text>
<xsl:apply-templates select="tree | family/tree"/>
	<xsl:text>
\end{document}</xsl:text>
</xsl:template>

<!-- .................................................. -->
<!-- .................................................. -->
<!-- .................................................. -->
<xsl:template match="tree">
  <xsl:text>\newpage

\section{Tree \protect{\{
</xsl:text>
<xsl:value-of select="@name"/>
<xsl:text> 
\}}}
%%\large
</xsl:text>

<xsl:apply-templates select="ht"/>

<xsl:text>
\subsection{Tree}
\begin{center}
</xsl:text>
<xsl:apply-templates select="node|optional"/>

<xsl:text>
\end{center}
  
\subsection{Node arguments}
\small

</xsl:text>
<xsl:apply-templates select="." mode="recurseavm"/>

<xsl:text>

\subsection{Node guards}
\small
</xsl:text>

<!-- <xsl:apply-templates select="." mode="recurseguard"/> -->

</xsl:template>


<xsl:template match="ht">
  <xsl:text>
\paragraph{Hypertag}
\begin{center}
  \begin{avm}{\[
  </xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\]}
  \end{avm}
\end{center}
   </xsl:text>
</xsl:template >


<!-- Feature -->

<xsl:template match="fs">
  <xsl:text>\[{</xsl:text>
  <xsl:value-of select="@type"/>
  <xsl:text>}</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\]</xsl:text>
</xsl:template>


<xsl:template match="f">
  <xsl:text>\verb@</xsl:text>
  <xsl:value-of select="@name"/>
  <xsl:text>@ &amp;</xsl:text>
  <xsl:apply-templates/>
  <xsl:if test="position()!=last()">
    <xsl:text>\\</xsl:text>
  </xsl:if>
</xsl:template >

<xsl:template match="symbol">
  <xsl:value-of select="@value"/>
</xsl:template >

<xsl:template match="plus">
  <xsl:text>+</xsl:text>
</xsl:template>

<xsl:template match="minus">
  <xsl:text>-</xsl:text>
</xsl:template>

<xsl:template match="fs">
  <xsl:text>{\[</xsl:text>
  <xsl:apply-templates select="f"/>
  <xsl:text>\]}</xsl:text>
</xsl:template >

<xsl:template match="var">
  <xsl:text>\@{</xsl:text><xsl:value-of select="@name"/>
  <xsl:text>} </xsl:text><xsl:apply-templates/>
</xsl:template>

<xsl:template match="vAlt">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
    <xsl:if test="not(position()=last())">\|</xsl:if>
  </xsl:for-each>
</xsl:template>

<xsl:template match="vNot">
  <xsl:text>$\neg$ \(</xsl:text>
  <xsl:apply-templates/>
  <xsl:text>\)</xsl:text>
</xsl:template>


<!-- Nodes -->

<xsl:template match="optional">
  <xsl:apply-templates/>
</xsl:template>

<xsl:template match="node[@type != 'std' and node|alternative|optional|sequence|interleave]">
  <xsl:text>\stdN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:apply-templates select="parent::optional" mode="opt"/>
  <xsl:text>]{\NodeError{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}{</xsl:text>
  <xsl:apply-templates select="node|optional"/>
  <xsl:text>}</xsl:text>  
</xsl:template>

<xsl:template match="node[@type='subst' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:apply-templates select="parent::optional" mode="opt"/>
  <xsl:text>]{\subst{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node[@type='foot' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text>]{\foot{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node[@type='lex' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:apply-templates select="parent::optional" mode="opt"/>
  <xsl:text>]{\lex{</xsl:text>
  <xsl:value-of select="@lex"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node[@type='anchor' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:apply-templates select="parent::optional" mode="opt"/>
  <xsl:text>]{\anchor{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>

<xsl:template match="node[@type='coanchor' and not(node)]">
  <xsl:text>\leafN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:apply-templates select="parent::optional" mode="opt"/>
  <xsl:text>]{\coanchor{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}}</xsl:text>
</xsl:template>


<xsl:template match="node">
  <xsl:text>\stdN[xname=</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:apply-templates select="." mode="dom"/>
  <xsl:apply-templates select="parent::optional" mode="opt"/>
  <xsl:text>]{</xsl:text>
  <xsl:value-of select="@cat"/>
  <xsl:text>}{</xsl:text>
  <xsl:apply-templates select="node|optional|sequence|interleave|alternative"/>
  <xsl:text>}</xsl:text>
</xsl:template>

<xsl:template match="optional" mode="opt">
  <xsl:text>,xopt</xsl:text>
</xsl:template>

<xsl:template match="node" mode="dom">
  <xsl:choose>
    <xsl:when test="@dom='+'">
      <xsl:text>,style=dom</xsl:text>
    </xsl:when>
    <xsl:otherwise>
      <xsl:text>,style=father</xsl:text>
    </xsl:otherwise>
  </xsl:choose>
</xsl:template>

<xsl:template match="node[@id]" mode="avm">
  <xsl:apply-templates select="narg"/>
</xsl:template>

<xsl:template match="tree|node|sequence|interleave|alternative|optional" mode="recurseavm">
  <xsl:apply-templates select="node[@id]" mode="avm"/>
  <xsl:apply-templates select="node|optional|interleave|alternative|sequence" mode="recurseavm"/>
</xsl:template>

<xsl:template match="narg">
  <xsl:text>\begin{NodeArg}{</xsl:text>
  <xsl:value-of select="../@id"/>
  <xsl:text>}{</xsl:text>
  <xsl:value-of select="@type"/>
  <xsl:text>}
</xsl:text>
  <xsl:apply-templates/>
  <xsl:text> 
\end{NodeArg}

</xsl:text>
</xsl:template>


<xsl:template match="tree|node|sequence|alternative|interleave|optional" mode="recurseguard">
  <xsl:apply-templates select="*[@id and guards]" mode="guard"/>
  <xsl:apply-templates select="node|optional|interleave|alternative|sequence" mode="recurseguard"/>
</xsl:template>

<xsl:template match="*[@id and guards]" mode="guard">
  <xsl:text>\begin{center}</xsl:text>
  <xsl:value-of select="@id"/>
  <xsl:text>:</xsl:text>
  <xsl:apply-templates select="guards" mode="guard"/>
  <xsl:text>\end{center}</xsl:text>
</xsl:template>

<xsl:template match="guards[@rel='+']" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="guards[@rel='-']" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="guard" mode="guard">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>    
</xsl:template>

<xsl:template match="xguard" mode="guard">
  <xsl:for-each select="*">
    <xsl:apply-templates select="."/>
  </xsl:for-each>    
</xsl:template>


<xsl:template match="and" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="or" mode="guard">
  <xsl:apply-templates mode="guard"/>
</xsl:template>

<xsl:template match="or[count(*) > 1]" mode="guard">
  <xsl:for-each select="*">
    <xsl:apply-templates select="." mode="guard"/>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

