## Process this file with automake to produce Makefile.in
AUTOMAKE_OPTIONS = foreign

# configure define the following variables
#       DYACC            DyALog compiler name
#       DYALOG_CFLAGS    Dyalog specific CFLAGS to compile C files with CC
#       DYALOG_DFLAGS    DYACC flags to compile DyALog files
#       DYALOG_LIBS      Link flags for both CC and DYACC
#       DYALOG_CONFIG    DyALog configuration script (dyalog-config)

# additional  variables
DINCLUDES = -I $(srcdir)
DFLAGS    = -autoload -verbose_tag -parse
CFLAGS   += $(DYALOG_CFLAGS)
LIBS     += $(DYALOG_LIBS)
DISTCHECK_CONFIGURE_FLAGS = $(dyalog_configure)

TESTS = t/tree_nb.t

EXTRA_DIST = config.rpath m4/ChangeLog  autogen.sh \
	     spgram.smg \
	     spgram.tag.xml \
	     preheader.tag \
	     spgram.map \
	     tagml2latex.xsl\
	     easy2html.xsl \
	     extract.xsl \
	     tagml2html.xsl \
	     style.css \
	     index.html \
	     tests.txt \
	     sentences.txt \
	     sentences2.txt \
	     sample_udag \
	     NOTES.txt \
	     error_analyzer.pl \
	     spmg_lexer.in \
	     tree.pl.in \
	     spmg.conf.in \
	     register_parserd.conf.in \
	     MyRegExp.pm \
	     spgram.map \
	     complete.lex \
	     tree.js \
	     prototype.js \
	     missing.lex \
	     restrictions.txt \
	     LefffDecoder.yp\
	     spmg.yml \
	     ${TESTS}

.PHONY: html
SUFFIXES = .conf.xml .conf.pl .smg .xml .mg.pl .tag.xml .tag .tex .ps .pdf .pl .o

MAP      = $(srcdir)/spgram.map
MGCOMP   = mgcomp
SMG2XML  = smg2xml
XSLT     = xsltproc
LATEX    = latex
REGISTER = register_parsers

bin_PROGRAMS    = spmg_parser easyforest
dist_bin_SCRIPTS= spmg_lexer
pkgdata_SCRIPTS = error_analyzer.pl
pkgdata_DATA    = LefffDecoder.pm \
		  MyRegExp.pm \
		  header.tag \
		  tests.txt \
		  complete.lex \
		  spgram.map \
		  register_parserd.conf \
		  missing.lex \
		  spmg.yml \
                  restrictions.txt

sysconf_DATA    = spmg.conf

if WANT_MODPERL
    pkgexecmodperl_DATA = extract.xsl \
		          style.css \
			  tagml2html.xsl \
			  index.html \
			  spgram.tag.xml \
			  classes.html \
			  resources.html \
			  nodes.html \
			  tree.js\
			  prototype.js
    pkgexecmodperl_SCRIPTS = tree.pl
endif
pkgexecmodperldir = $(execmodperldir)/$(PACKAGE)


spmg_parser_SOURCES = main.tag tag_generic.pl addons.tag
nodist_spmg_parser_SOURCES = spgram.tag
spmg_parser_DFLAGS = -res header.tag -res tig_header.tag -res features_header.tag

BUILT_SOURCES = spgram.tag.xml

#lexer_SOURCES = lexer.tag tag_generic.pl

easyforest_SOURCES = easyforest.pl rx_c.c rx.pl
easyforest_DFLAGS = $(DYALOG_XML_DFLAGS) ${DYALOG_SQLITE_DFLAGS}
easyforest_LDADD = $(DYALOG_XML_LIBS) $(DYALOG_SQLITE_LIBS)


pkgconfigdir = $(libdir)/pkgconfig
dist_pkgconfig_DATA = spmg.pc

######################################################################

header.tag: spgram.tag.xml $(MAP) $(srcdir)/preheader.tag
	-rm header.tag
	cp $(srcdir)/preheader.tag header.tag
	tag_converter --map=$(MAP) -t header spgram.tag.xml >> $@

main.o: spgram.tag header.tag

spgram.tag: header.tag

tig_header.tag: spgram.tag addons.tag header.tag features_header.tag
	$(DYACC) $(DINCLUDES) -analyze tag2tig $^ -o $@
	cp $@ tmp_header.tag
	$(DYACC) $(DINCLUDES) -analyze lctag $^ tmp_header.tag >> $@

features_header.tag: spgram.tag addons.tag header.tag
	$(DYACC) $(DINCLUDES) -analyze tag_features $^ tig_header.tag.tmp -o $@

spmg_parser-spgram.o spmg_parser-main.o spmg_parser-tag_generic.o: tig_header.tag features_header.tag

######################################################################
# Conversions

## MG and grammar files

# Conversions SMG to XMLMG
%.xml: %.smg
	$(SMG2XML) $< > $@

# Converting from XML mg file to LP mg file
%.mg.pl: %.xml
	$(XSLT) -o $@ $(MGTOOLS_XSLT)/mg2dyalog.xsl $<

# Converting from LP mg file to XML TAG file
%.tag.xml: %.mg.pl
	-cp $*.dump $*.dump.bak
	$(MGCOMP) -noforest $< -trytig -xml -family -ht \
				-restore $*.dump -dump $*.dump -o $@

# Converting from XML TAG file to DyALog TAG file
%.tag: %.tag.xml
	tag_converter --map=$(MAP) -t lp $< |\
	sed -e '/^%% Family/d' > $@

######################################################################
# Get addition version and information

%.tex: %.tag.xml
	$(XSLT) $(srcdir)/tagml2latex.xsl $< | $(PERL) $(srcdir)/lfg2latex.pl > $@

%.warnings: %.mg.pl
	$(MGCOMP) -noforest $< -warning 2> $@

%.simple: %.mg.pl
	$(MGCOMP) -noforest $< -simple | sort > $@

%.dot: %.xml
	$(XSLT) -o $@ $(MGTOOLS_XSLT)/mg2dot.xsl $<

%.gif: %.dot
	dot -Tgif -o $@ $< 

%.cmap: %.dot
	dot -Tcmap -o $@ $<

classes.html: spgram.xml
	$(XSLT) -o $@ $(MGTOOLS_XSLT)/mgclass2html.xsl $<

classes.html: $(MGTOOLS_XSLT)/mgclass2html.xsl

resources.html: spgram.xml
	$(XSLT) -o $@ $(MGTOOLS_XSLT)/mgres2html.xsl $<

resources.html: $(MGTOOLS_XSLT)/mgres2html.xsl

nodes.html: spgram.xml
	$(XSLT) -o $@ $(MGTOOLS_XSLT)/mgnode2html.xsl $<

nodes.html: $(MGTOOLS_XSLT)/mgnode2html.xsl

html: classes.html resources.html nodes.html


# if necessary, use LATEX = hugelatex make spgram.ps
# or fiddle texmf.cnf file to increase memory size


%.ps: %.tex
	$(LATEX) $*
	$(LATEX) $*
	$(LATEX) $*
	dvips -o $@ $*

%.pdf: %.ps
	ps2pdf $< $@

clean-local:
	-rm spgram.tag.xml spgram.tag header.tag tig_header.tag
	-rm spgram.mg.pl

LefffDecoder.pm: LefffDecoder.yp
	eyapp -o $@ -m LefffDecoder $<

EDIT = sed \
       -e 's|@bindir\@|$(bindir)|' \
       -e 's|@libdir\@|$(libdir)|' \
       -e 's|@pkgdatadir\@|$(pkgdatadir)|' \
       -e 's|@sysconfdir\@|$(sysconfdir)|' \
       -e 's|@pkgexecmodperldir\@|$(pkgexecmodperldir)|' \
       -e 's|@PACKAGE\@|$(PACKAGE)|' \
       -e 's|@VERSION\@|$(VERSION)|'

tree.pl: tree.pl.in Makefile
	$(EDIT) < $< > $@

register_parserd.conf: register_parserd.conf.in Makefile
	$(EDIT) < $< > $@

spmg.conf: spmg.conf.in Makefile
	$(EDIT) < $< > $@

spmg_lexer: spmg_lexer.in Makefile
	$(EDIT) < $< > $@


## Local stuff: an example of using restrictions for desambiguation
restrictions.db: fr_dv_N_sim4sub4.txt  fr_dv_V3_73_simil8_removed.txt
	-@rm $@
	${PERL} ./load_restrictions.pl --db $@ $<

## To install spmg.conf without overwriting existing one
install-sysconfDATA: $(sysconf_DATA)
	@$(NORMAL_INSTALL)
	test -z "$(sysconfdir)" || $(mkdir_p) "$(DESTDIR)$(sysconfdir)"
	@list='$(sysconf_DATA)'; for p in $$list; do \
		if test -f "$$p"; then d=; else d="$(srcdir)/"; fi; \
		f=$(am__strip_dir) \
		if [ ! -f $(DESTDIR)$(sysconfdir)/$$f ]; then \
			echo " $(sysconfDATA_INSTALL) '$$d$$p' '$(DESTDIR)$(sysconfdir)/$$f'"; \
			$(sysconfDATA_INSTALL) "$$d$$p" "$(DESTDIR)$(sysconfdir)/$$f"; \
		else \
			echo " *** File exists: '$(DESTDIR)$(sysconfdir)/$$f'"; \
			echo " $(sysconfDATA_INSTALL) '$$d$$p' '$(DESTDIR)$(sysconfdir)/$$f.distrib'"; \
			$(sysconfDATA_INSTALL) "$$d$$p" "$(DESTDIR)$(sysconfdir)/$$f.distrib"; \
		fi; \
	done

# Registering parsers
install-exec-hook:
	$(REGISTER) -a $(srcdir)/register_parserd.conf $(sysconfdir)/parserd.conf

postsvn:
	${MAKE} -t spgram.dump
	${MAKE} -t spgram.tag.xml

SUBDIRS =

ACLOCAL_AMFLAGS = -I m4
